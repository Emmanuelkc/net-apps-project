import express, {Express, Request, Response} from "express";

// const express = require("express");
import fileUpload from 'express-fileupload';
const app:Express  = express()
import cors from 'cors';

app.use(fileUpload());

app.post("/upload", (req: Request, res: Response) =>{
    if(req.files === null){
        return res.status(400).json({msg: "No file uploaded"});
    }

    const file = req.files.file;

    file.mv(`${__dirname}/client/public/uploads/${file.name}`, err => {
        if(err){
            console.log(err);
            return res.status(500).send(err);
        }

        res.json({fileName: file.name, filePath:`/uploads/${file.name}`})
    })
});

app.delete("/upload", (req, res) => {
    console.log('File deleted')
    return res.status(200).json({result: true, msg:'file deleted'})
});

const port = process.env.PORT || 8080
app.listen(8080, () =>{
    console.log("server running on port 8080")
});



// import express, {Express, Request, Response} from "express";

// // const express = require("express");
// import fileUpload from 'express-fileupload';
// const app = express()
// import cors from 'cors';

// app.use(cors());

// app.post("/upload", (req: Request, res: Response) =>{
//     setTimeout(() =>{
//         console.log('File deleted')
//         return res.status(200).json({result: true, msg:'file uploaded'})
//     }, 3000);
// });

// app.delete("/upload", (req, res) => {
//     console.log('File deleted')
//     return res.status(200).json({result: true, msg:'file deleted'})
// });

// app.listen(8080, () =>{
//     console.log("server running on port 8080")
// });