declare module 'react-step-progress-bar' {
    interface ProgressBar {
        percent?: number;
        filledBackground?: any;
        height?: string | number;
        stepPositions?: number;
    }
    interface Step {
        transition?: any;
        position?: any;
    }
    class ProgressBar extends React.Component<ProgressBarProps, any> {}
    class Step extends React.Component<StepProps, any> {}
}