import React from 'react'
import {ProductDescWrap} from "../../../styles/BorrowerDashboard/BorrowersTransactionDescription/";
import { productDescription, productDescriptionTitle } from '../Utility/DataFetch';

const ProductDesc = () => {
    return (
    <ProductDescWrap>

      <div className='container'>
      <table className='table'>
        <thead className='table-head'>
          {productDescriptionTitle.map((row)=>{
            return(
              <tr>
                <th>{row.col1}</th>
                <th className='mid-row'>{row.col2}</th>
                <th>{row.col3}</th>
              </tr>
            )
          })}
        </thead>
        
        <tbody className='table-body'>
          {productDescription.map((row) =>{
            return(
              <tr>
                <td className='right-row'>{row.loanPrdtDesc}</td>
                <td className='mid-row'>{row.details}</td>
                <td>{row.status}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
      </div>
    </ProductDescWrap>
  )
}

export default ProductDesc