import React, { useState, useEffect, Fragment } from "react";
import { InvestorTransactionTable } from "../../../../../styles/InvestorStyles/dashboard";
import { NumericFormat } from "react-number-format";
import ReactPaginate from "react-paginate";
import { TransactionProps } from "../../../Utility/types";
import { BsFillPatchCheckFill } from "react-icons/bs";
import { approveLoanAmount, ModalType } from "../../../Utility/modalData";
import LoanApprovalModal from "../../../Utility/Investor/LoanApprovalModal";
import { Button } from "primereact/button";

const ApprovedTransactionData = ({
  transactionTableData,
  transactionTableHeaders,
}: any) => {
  const [approvalModal, setApprovalModal] = useState({
    open: false,
    modalType: ModalType.NONE,
  });

  // const ViewTransactionDetails = (e: any) =>{
  //     e.preventDefault()
  //     router.push(`/borrowers/${transactionDetailsId}`)
  // }

  const [currentItems, setCurrentItems] = useState<TransactionProps[]>([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const itemsPerPage = 6;

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(transactionTableData.slice(itemOffset, endOffset));
    return setPageCount(Math.ceil(transactionTableData.length / itemsPerPage));
  }, [itemOffset, itemsPerPage, transactionTableData]);

  const handlePageClick = (event: any) => {
    const newOffset =
      (event.selected * itemsPerPage) % transactionTableData.length;
    setItemOffset(newOffset);
  };

  return (
    <InvestorTransactionTable>
      <div className="card container">
        <div className="table-head-text">
          <h5 className="table-head"> Approved Loan Transactions</h5>
        </div>
        <table className="table">
          <thead>
            <tr>
              {transactionTableHeaders.map(
                (rows: { id: string; label: string }) => {
                  return <th key={rows.id}>{rows.label}</th>;
                }
              )}
            </tr>
          </thead>
          <tbody>
            {currentItems.map((row: any) => {
              return (
                <tr key={row.id}>
                  {row.status === "approved" && (
                    <Fragment>
                      <td data-label="id">{row.id}</td>
                      <td data-label="Full Name">{row.name}</td>
                      <td data-label="Product">{row.product}</td>
                      <td data-label="Loan Amount">
                        <NumericFormat
                          value={row.loanAmount}
                          thousandsGroupStyle="lakh"
                          thousandSeparator=","
                          displayType="text"
                          renderText={(value) => <b>{value}</b>}
                        />
                      </td>
                      <td data-label="Date Applied">{row.applied}</td>
                      <td data-label="Loan Status">
                        {row.status === "approved" ? (
                          <div className="paid">
                            {row.status}
                            <BsFillPatchCheckFill className="payment-icon" />
                          </div>
                        ) : (
                          <Button
                            className="repay"
                            onClick={() =>
                              setApprovalModal({
                                open: true,
                                modalType: ModalType.APPROVE_LOAN_AMOUNT,
                              })
                            }
                          >
                            {row.status}
                            <BsFillPatchCheckFill className="payment-icon" />
                          </Button>
                        )}
                      </td>
                      <td data-label="Approved Amount">
                        {row.status === "approved" ? (
                          <NumericFormat
                            value={row.approvedAmount}
                            thousandsGroupStyle="lakh"
                            thousandSeparator=","
                            displayType="text"
                            renderText={(value) => <b>{value}</b>}
                          />
                        ) : (
                          <div className="">
                            {row.status}
                            <BsFillPatchCheckFill className="payment-icon" />
                          </div>
                        )}
                      </td>
                    </Fragment>
                  )}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      {approvalModal.open ? (
        approvalModal.modalType === ModalType.APPROVE_LOAN_AMOUNT ? (
          <LoanApprovalModal
            setApprovalModal={setApprovalModal}
            modalData={approveLoanAmount}
          />
        ) : null
      ) : null}

      <div className="custom flex justify-content-end align-items-end">
        <ReactPaginate
          previousLabel={"previous"}
          nextLabel={" next "}
          breakLabel={"..."}
          pageCount={pageCount}
          pageRangeDisplayed={4}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          pageLinkClassName={"page-number"}
          breakLinkClassName={"page-item"}
          previousLinkClassName={"previous-btn"}
          nextLinkClassName={"next-btn"}
          activeLinkClassName={"active"}
        />
      </div>
    </InvestorTransactionTable>
  );
};

export default ApprovedTransactionData;
