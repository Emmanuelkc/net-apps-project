import React, {FC, useState} from 'react';
import 'primeflex/primeflex.css';
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css";  //theme
import {TbCurrencyNaira} from 'react-icons/tb'
import {NumericFormat} from "react-number-format";
import {Button} from "primereact/button";
import {InvestorsStats,} from "../../../../styles/InvestorStyles/dashboard";
import { Chart } from 'primereact/chart';



const WalletStats:FC = () => {
    const [basicData] = useState({
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
            {
                label: 'Total Disbursed Loan',
                backgroundColor: '#42A5F5',
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            {
                label: 'Total UnPaid Loans',
                backgroundColor: '#FFA726',
                data: [28, 48, 40, 19, 86, 27, 90]
            }
        ]
    });



    const getLightTheme:any = () => {
        let basicOptions = {
            maintainAspectRatio: false,
            aspectRatio: .8,
            plugins: {
                legend: {
                    labels: {
                        color: '#495057'
                    }
                }
            },
            scales: {
                x: {
                    ticks: {
                        color: '#495057'
                    },
                    grid: {
                        color: '#ebedef'
                    }
                },
                y: {
                    ticks: {
                        color: '#495057'
                    },
                    grid: {
                        color: '#ebedef'
                    }
                }
            }
        };

        return {
            basicOptions,
        }
    }

    const { basicOptions, } = getLightTheme();

    return (
        <>
            <InvestorsStats>
                <div className="grid m-2">
                    <div className=" col-12 md:col-6 lg:col-6"  >
                        <div className="wallet-card shadow-2 flex justify-content-between" >
                            <div className="p-4">
                                <h2 className="block ml-2 font-bold">Total Balance</h2>
                                <div className=" font-medium ml-2">
                                    <TbCurrencyNaira className='naira-icon' style={{marginBottom:"-0.6rem"}}/>
                                    <NumericFormat
                                        value={4340000}
                                        thousandsGroupStyle="lakh"
                                        thousandSeparator=","
                                        displayType="text"
                                        renderText={(value) => <span className="value">{value}</span>}
                                    />
                                </div>
                                <Button className="ml-3 wallet-card-btn">
                                    <i className="pi pi-wallet pr-2"></i>Fund Wallet</Button>
                                <Button className="ml-3 wallet-card-btn">
                                    <i className="pi pi-wallet pr-2"></i>Withdraw</Button>
                            </div>
                            <div className="bg-blue-400 clipPath p-4 " >
                                <i className="pi pi-wallet text-white text-2xl"></i>
                                <h5 className="lg:text-1xl line-height-1 text-white">Net-apps</h5>
                                <h5 className="lg:text-1xl line-height-1 text-white">Loan Wallet </h5>
                                <span className="text-yellow-500 font-medium font-bold">24%
                                    <i className="pi pi-chart-line ml-2"></i></span>
                                <span className="text-500 text-white"> credit score </span>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 md:col-6 lg:col-6 ">
                        <div className=" wallet-card  shadow-2 p-4">
                            <Chart type="bar" data={basicData} options={basicOptions} style={{ position: 'relative',display:"flex", width:"100%",height:"23vh" }}/>
                        </div>
                    </div>
                </div>
            </InvestorsStats>
        </>
    );
}

export default WalletStats;