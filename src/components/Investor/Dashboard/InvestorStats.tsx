import React, { FC } from 'react';
import 'primeflex/primeflex.css';
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css";  //theme
import {TbCurrencyNaira} from 'react-icons/tb'
import {LoanStatWrap} from "../../../../styles/BorrowerDashboard"
import {NumericFormat} from "react-number-format";

const Investor_Stats:FC = () => {

    return (
            <LoanStatWrap>
                <div className="grid m-2">
                    <div className="col-12 md:col-6 lg:col-3 ">
                        <div className="surface-0 shadow-2 p-3 wallet-card">
                            <div className="flex justify-content-between mb-3 p-3">

                                <div>
                                    <p className="block text-800  mb-3 ">Total Approved Loan</p>
                                    <div className="text-700 font-medium">
                                        <TbCurrencyNaira className='naira-icon text-700' style={{marginBottom:"-0.6rem"}}/>
                                        <NumericFormat
                                            value={400000}
                                            thousandsGroupStyle="lakh"
                                            thousandSeparator=","
                                            displayType="text"
                                            renderText={(value) => <b className='text-700'>{value}</b>}
                                        />
                                    </div>
                                </div>
                                <div className="flex align-items-center justify-content-center bg-blue-100 border-round" style={{ width: '3rem', height: '3rem' }}>
                                    <i className="pi pi-paypal text-blue-800 text-xl"></i>
                                </div>
                            </div>
                            <span className="text-green-500">View all approved loan <i className="pi pi-chart-line"></i></span>
                        </div>
                    </div>
                    <div className="col-12 md:col-6 lg:col-3 ">
                        <div className="surface-0 shadow-2 p-3 wallet-card">
                            <div className="flex justify-content-between mb-3 p-3">

                                <div>
                                    <p className="block text-800  mb-3">Total Retrieved Loan</p>
                                    <div className="text-700 font-medium">
                                        <TbCurrencyNaira className='naira-icon text-700' style={{marginBottom:"-0.6rem"}}/>
                                        <NumericFormat
                                            value={200000}
                                            thousandsGroupStyle="lakh"
                                            thousandSeparator=","
                                            displayType="text"
                                            renderText={(value) => <b className='text-700'>{value}</b>}
                                        />
                                    </div>
                                </div>
                                <div className="flex align-items-center justify-content-center bg-blue-100 border-round" style={{ width: '3rem', height: '3rem' }}>
                                    <i className="pi pi-paypal text-blue-800 text-xl"></i>
                                </div>
                            </div>
                            <span className="text-green-500">View all retrieved Loan</span>
                        </div>
                    </div>
                    <div className="col-12 md:col-6 lg:col-3 ">
                        <div className="surface-0 shadow-2 p-3 wallet-card">
                            <div className="flex justify-content-between mb-3 p-3">

                                <div>
                                    <p className="block text-800  mb-3">Incoming Loan Request</p>
                                    <div className="text-700 font-medium">
                                        <TbCurrencyNaira className='naira-icon text-700' style={{marginBottom:"-0.6rem"}}/>
                                        <NumericFormat
                                            value={300000}
                                            thousandsGroupStyle="lakh"
                                            thousandSeparator=","
                                            displayType="text"
                                            renderText={(value) => <b className='text-700'>{value}</b>}
                                        />
                                    </div>
                                </div>
                                <div className="flex align-items-center justify-content-center bg-blue-100 border-round" style={{ width: '3rem', height: '3rem' }}>
                                    <i className="pi pi-paypal text-blue-800 text-xl"></i>
                                </div>
                            </div>
                            <span className="text-green-500">View all Incoming loan</span>
                        </div>
                    </div>
                    <div className="col-12 md:col-6 lg:col-3 ">
                        <div className="surface-0 shadow-2 p-3 wallet-card">
                            <div className="flex justify-content-between mb-3 p-3">

                                <div>
                                    <p className="block text-800  mb-3">Total Disbursed Loan</p>
                                    <div className="text-700 font-medium">
                                        <TbCurrencyNaira className='naira-icon text-700' style={{marginBottom:"-0.6rem"}}/>
                                        <NumericFormat
                                            value={448000}
                                            thousandsGroupStyle="lakh"
                                            thousandSeparator=","
                                            displayType="text"
                                            renderText={(value) => <b className='text-700'>{value}</b>}
                                        />
                                    </div>
                                </div>
                                <div className="flex align-items-center justify-content-center bg-blue-100 border-round" style={{ width: '3rem', height: '3rem' }}>
                                    <i className="pi pi-paypal text-blue-800 text-xl"></i>
                                </div>
                            </div>
                            <span className="text-green-500">view All disbursed loan</span>
                        </div>
                    </div>
                </div>

            </LoanStatWrap>
    );
}

export default Investor_Stats;