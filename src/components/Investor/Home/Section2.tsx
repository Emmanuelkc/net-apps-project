import React from "react";
import "primeicons/primeicons.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import router from "next/router";
import {
  ButtonsContainer,
  HeroSection22,
} from "../../../../styles/InvestorStyles/Home";
import { Button } from "primereact/button";

const Section2 = () => {
  const Borrowers = (e: any) => {
    e.preventDefault();
    router.push("/investors/dashboard");
  };
  return (
    <HeroSection22 className="grid grid-nogutter surface-0  banner">
      <div className="col-12 md:col-6 lg:p-7 sm:p-6 text-center md:text-left  align-items-center ">
        <section className="lg:p-6 sm:p-6">
          <h2 className="text-white ">Net-apps</h2>
          <span className="block text-3xl font-bold mb-3">
            The Next Phase of Marketplace Lending
          </span>
          <p className="mt-0 mb-4  line-height-">
            You dont need to have perfect credit to get a personal loan. So
            instead of going from lender to lender to find something you qualify
            for, let us help you explore all your options at once and find the
            best offers for you.
          </p>

          <ButtonsContainer>
            <Button className="Button" onClick={Borrowers}>
              Get Started
            </Button>
          </ButtonsContainer>
        </section>
      </div>
      {/*<div className="col-12 md:col-6 overflow-hidden text-bluegray-900 justify-content-center flex">*/}

      {/*</div>*/}
    </HeroSection22>
  );
};

export default Section2;
