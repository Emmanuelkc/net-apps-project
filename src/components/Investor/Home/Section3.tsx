import React from "react";
import "primeicons/primeicons.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import router from "next/router";
import {
  ButtonsContainer,
  HeroSection22,
} from "../../../../styles/BorrowerDashboard/LoanCategory/loanCategory";
import { Button } from "primereact/button";

const Section3 = () => {
  const Borrowers = (e: any) => {
    e.preventDefault();
    router.push("");
  };
  return (
    <HeroSection22 className="grid grid-nogutter text-800 ">
      <div className=" side col-12 md:col-6 p-6  overflow-hidden flex flex-wrap ">
        <h2 className="block text-3xl font-bold align-items-end  md:text-left">
          Purchase Loans with Price Transparency
        </h2>
      </div>
      <div className="col-12 md:col-6 p-6 text-center md:text-left flex align-items-center ">
        <section>
          <h2 className="block text-1xl font-bold mb-3">
            Electronic Marketplace for Unsecured Personal Loans
          </h2>
          <p className="mt-0 mb-4 text-900 line-height-">
            Execute your investment strategy by filtering and purchasing loans
            through specific credit attributes. Individually purchase loans pre-
            or post-issuance at prices that can be at, above, or below par.
          </p>

          <h2 className=" mt-6 block text-1xl font-bold mb-3">$1.5B+ Market</h2>
          <p className="mt-0 mb-4 text-900 line-height-">
            The Net-apps marketplace launched in 2019, with over $1.5B
            transacted to date in both primary and secondary markets
          </p>
          <h2 className=" mt-6 block text-1xl font-bold mb-3">
            Financial well-being for everyone.
          </h2>
          <p className="mt-0 mb-4 text-900 line-height-">
            Everything we do is centered around empowering you to meet your
            personal financial goals by enabling access to a broad range of
            financial products, services, and educational resources , all
            designed to help you pay less when borrowing and earn more when
            saving.
          </p>
          <ButtonsContainer className="justify-content-center">
            <Button className="Button" onClick={Borrowers}>
              Learn More
            </Button>
          </ButtonsContainer>
        </section>
      </div>
    </HeroSection22>
  );
};

export default Section3;
