import React from "react";
import "primeicons/primeicons.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import bg from "../../../../public/Assets/withdraw.png";
import Image from "next/image";
import router from "next/router";
import {
  ButtonsContainer,
  HeroSection1,
} from "../../../../styles/BorrowerDashboard/LoanCategory/loanCategory";
import { Button } from "primereact/button";

const Section1 = () => {
  const Borrowers = (e: any) => {
    e.preventDefault();
    router.push("/borrower");
  };
  return (
    <HeroSection1 className="grid grid-nogutter text-800  ">
      <div className="col-12 md:col-6 md:text-left p-4  align-items-center  justify-content-center   text-center md:text-left  ">
        <section className="lg:pl-6">
          <h4>The highest interest rates</h4>
          <h5> Need your money sooner?</h5>
          <p className="mt-0 mb-4 text-700 line-height-4">
            Cash out to get your investment back any time under normal market
            conditions – even if the investments haven’t reached maturity.
          </p>
          <h5>High-yield</h5>
          <p className="mt-0 mb-4 text-700 line-height-4">
            Maximize your potential returns with the highest interest rate
            investments With Net-apps Loan..
          </p>
          <p className="mt-0 mb-4 text-700 line-height-3">
            Max 15% in Notes for one lending company
          </p>
          <p className="mt-0 mb-4 text-700 line-height-3">
            Max 15% in Notes for one lending company
          </p>
          <p className="mt-0 mb-4 text-700 line-height-3">
            Notes backed by loans with a buyback obligation
          </p>
          <p className="mt-0 mb-4 text-700 line-height-3">
            Notes backed by loans with a buyback obligation
          </p>

          <ButtonsContainer>
            <Button className="Button" onClick={Borrowers}>
              Get Started
            </Button>
          </ButtonsContainer>
        </section>
      </div>
      <div className="col-12 md:col-6 p-4 overflow-hidden  flex flex-wrap align-items-center ">
        <Image src={bg} alt="hero-1" width={600} height={430} />
      </div>
    </HeroSection1>
  );
};

export default Section1;
