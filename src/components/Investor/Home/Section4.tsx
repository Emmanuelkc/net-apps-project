import React from "react";
import "primeicons/primeicons.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import bg from "../../../../public/Assets/mobile.png";
import Image from "next/image";
import router from "next/router";
import {
  ButtonsContainer,
  HeroSection1,
} from "../../../../styles/BorrowerDashboard/LoanCategory/loanCategory";
import { Button } from "primereact/button";

const Section4 = () => {
  const Borrowers = (e: any) => {
    e.preventDefault();
    router.push("");
  };
  return (
    <HeroSection1 className="grid grid-nogutter  text-800   ">
      <div className="col-12 md:col-6 overflow-hidden p-4 flex flex-wrap align-items-center Container ">
        <Image src={bg} alt="hero-1" width={400} height={300} />
      </div>
      <div className="col-12 md:col-6 mt-4   text-center md:text-left  align-items-center  justify-content-center ">
        <section>
          <h2>3 strategies for all risk tolerances </h2>
          <p className="mt-0 mb-4 text-700 line-height-4">
            Investing should be easy and effective. Our predefined Net-apps{" "}
            <br /> strategies work for portfolios of all sizes. They are fully
            automated,
            <br /> and can get you started in no time.{" "}
          </p>
          <span className="block text-1xl font-bold mb-3">Save Time</span>
          <p className="mt-0 mb-4 text-700 line-height-4">
            Get started in seconds, save time spent on investing
          </p>
          <span className="block text-1xl font-bold mb-3">
            {" "}
            Earn attractive Profits
          </span>
          <p className="mt-0 mb-4 text-700 line-height-4">
            Earn attractive returns, cash out anytime (subject to market demand)
          </p>
          <span className="block text-1xl font-bold mb-3">Risk Management</span>
          <p className="mt-0 mb-4 text-700 line-height-4">
            Diversify your investments to manage risk
          </p>

          <ButtonsContainer>
            <Button className="Button" onClick={Borrowers}>
              Get Started
            </Button>
          </ButtonsContainer>
        </section>
      </div>
    </HeroSection1>
  );
};

export default Section4;
