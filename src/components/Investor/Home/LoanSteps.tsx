import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import Bg1 from "../../../../public/Assets/I1.svg"
import Bg2 from "../../../../public/Assets/I2.svg"
import Bg3 from "../../../../public/Assets/I3.svg"
import {LoanStepsWrapper} from "../../../../styles/BorrowerDashboard/LoanCategory/loanCategory";
import Image from "next/image";

const LoanSteps =()=>{
    return(
        <LoanStepsWrapper>
            <div className=" text-center p-6">
                <div className="mb-8 font-bold text-2xl">
                    <h2>Ways to Participate</h2>
                </div>
                <div className="grid">
                    <div className="col-12 md:col-4 mb-4 px-5 ">
                        <Image src={Bg1} alt="bg1" layout="intrinsic" width={120} height={120}/>
                        <div className="mb-3 font-medium font-bold">
                            Self Service
                        </div>
                        <div className="text-900 mb-3 font-medium">Investor builds credit model and customizes API</div>

                    </div>
                    <div className="col-12 md:col-4 mb-4 px-5">
                        <Image src={Bg2} alt="bg1" layout="intrinsic" width={120} height={120}/>
                        <div className="mb-3 font-medium font-bold">
                            Partner
                        </div>
                        <div className="text-900 mb-3 font-medium">Investor leverages Net-apps’s vendor partners to build API</div>
                    </div>

                    <div className="col-12 md:col-4 mb-4 px-5">
                        <Image src={Bg3} alt="bg1" layout="intrinsic" width={120} height={120}/>
                        <div className="mb-3 font-medium font-bold">
                            Net-apps</div>
                        <div className="text-900 mb-3 font-medium">Investor gives credit filters to Net-apps, and Net-apps executes on behalf of the investor</div>


                    </div>
                </div>
            </div>

        </LoanStepsWrapper>
    )
}

export default LoanSteps