import React from "react";
import { KYCFormWrap } from "../../../../../../styles/Vendor/kycFormStyle";
import { KycBusinessFormData } from "../../../Utility/InvestorData";

const BusinessInfo = () => {
  return (
    <KYCFormWrap>
      <div className="grid text-800 mt-3">
        <div className=" surface-10 text-800  col-12 md:col-12 overflow-hidden  text-center  align-items-center">
          <div className="terms-section text-800 text-2xl font-bold mb-3 text-primary ">
            <h2 className="block text-1xl font-bold mb-3">
              Business Information
            </h2>
          </div>
          <div className="right-flex ">
            {KycBusinessFormData.map((inputData) => {
              return (
                <div key={inputData.id}>
                  {
                    <>
                      <label className="block text-600 font-medium mb-2 font-bold">
                        {inputData.label}
                      </label>
                      <input
                        className="input mb-6  pl-2"
                        name={inputData.name}
                        required={inputData.required}
                        datatype={inputData.dataType}
                        placeholder={inputData.placeholder}
                      />
                    </>
                  }
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </KYCFormWrap>
  );
};

export default BusinessInfo;
