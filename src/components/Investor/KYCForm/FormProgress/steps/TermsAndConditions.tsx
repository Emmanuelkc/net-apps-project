import React from "react";
import { KYCFormWrap } from "../../../../../../styles/Vendor/kycFormStyle";
import { KycTermsFormData } from "../../../Utility/InvestorData";

const TermsAndConditions = () => {
  return (
    <KYCFormWrap>
      <div className="grid grid-nogutter mt2">
        <div className="surface-10 col-12 md:col-12    overflow-hidden  ">
          <div className="terms-section">
            <div className="ml-4">
              <h2 className="block text-1xl font-bold  mt-4 text-center">
                Investor Application Instructions
              </h2>
              <p className="text-red-500  text-center">
                {" "}
                Applicants must Click on each box to acknowledge instructions
              </p>
            </div>
            <>
              {KycTermsFormData.map((rules: { name: string }, index: any) => {
                return (
                  <div key={index} className="checkbox-text">
                    <input
                      type="checkbox"
                      id={`custom-checkbox-${index}`}
                      name={rules.name}
                    />
                    <label
                      className="checkbox-label"
                      htmlFor={`custom-checkbox-${index}`}
                    >
                      {rules.name}
                    </label>
                  </div>
                );
              })}
            </>
          </div>
        </div>
      </div>
    </KYCFormWrap>
  );
};

export default TermsAndConditions;
