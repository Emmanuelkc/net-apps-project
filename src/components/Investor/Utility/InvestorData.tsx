export const KycBusinessFormData = [
  {
    id: 1000,
    name: "businessInfo",
    label: "Business Name",
    required: true,
    dataType: "text",
    placeholder: "Investor's Business Name",
  },
  {
    id: 1001,
    name: "businessAddress",
    label: "Business Location",
    required: true,
    dataType: "text",
    placeholder: "No. 20, Lungi Str. Wuse II, Abuja-Nigeria",
  },
  {
    id: 1002,
    name: "phoneNumber",
    label: "Phone Contact",
    required: true,
    dataType: "number",
    placeholder: "08100010200",
  },
];

export const KycVerificationFormData = [
  {
    id: 1000,
    name: "nin",
    label: "NIN",
    required: true,
    dataType: "number",
    placeholder: "Enter NIN",
  },
  {
    id: 1001,
    name: "bvn",
    label: "BVN",
    required: true,
    dataType: "number",
    placeholder: "Enter BVN",
  },
];

export const KycTermsFormData = [
  {
    name: "Yes, I have read through and I agree with the terms and condition and i accept all ",
  },
  {
    name: "Yes, I have read through and I agree with the terms and condition  and i accept all ",
  },
  {
    name: "Yes, I have read through and I agree with the terms and condition and i accept all ",
  },
  {
    name: "Yes, I have read through and I agree with the terms and condition and i accept all ",
  },
  {
    name: "Yes, I have read through and I agree with the terms and condition  and i accept all ",
  },
  {
    name: "Yes, I have read through and I agree with the terms and condition  and i accept all ",
  },
  {
    name: "Yes, I have read through and I agree with the terms and condition  and i accept all ",
  },
  {
    name: "Yes, I have read through and I agree with the terms and condition  and i accept all ",
  },
  {
    name: "Yes, I have read through and I agree with the terms and condition  and i accept all ",
  },
];
