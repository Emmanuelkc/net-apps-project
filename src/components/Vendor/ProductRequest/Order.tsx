import React, { useState } from "react";
import { ProductRequestWrap } from "../../../../styles/Vendor/productRequestStyle";
import brownHandbag from "../../../../public/Assets/productRequest/brownHandbag.png";
import Image from "next/image";
import { TbCurrencyNaira } from "react-icons/tb";
import { customerData, productRequest } from "../Utility/productDescData";
import { request } from "https";
import CustomerHistory from "../CustomerHistory/CustomerHistory";

// type requestData = {
//   productDescription: string,
//   designerInfo: string,
//   detailsCare: string
// }

const Order = () => {
  const [description, setDescription] = useState(false);
  const [designer, setDesigner] = useState(false);
  const [detailsCare, setDetailsCare] = useState(false);
  const [personalInfo, setPersonalInfo] = useState(false);

  const handleDescriptionInfo = (e: any) => {
    e.preventDefault();
    setDescription(true);
  };

  const handleDesignerInfo = (e: any) => {
    e.preventDefault();
    setDesigner(true);
  };

  const handleDetailsCare = (e: any) => {
    e.preventDefault();
    setDetailsCare(true);
  };

  const handlePersonalInfo = (e: any) => {
    e.preventDefault();
    setPersonalInfo(true);
  };

  return (
    <>
      <CustomerHistory />
      <ProductRequestWrap>
        <div className="customer-request-flex">
          <div>
            <Image
              layout="intrinsic"
              width={400}
              height={450}
              src={brownHandbag}
            />
          </div>
          <div className="flex-right">
            <p className="name-tag">Brown Leather Handbag</p>
            <p className="price-tag">
              <TbCurrencyNaira className="naira-icon" />
              {productRequest.map((info: any) => {
                return (
                  <p key={info.id} className="price">
                    {info.price}
                  </p>
                );
              })}
            </p>
            <div className="customer-info">
              <p className="description" onClick={handleDescriptionInfo}>
                Description
              </p>
              {description && (
                <>
                  <hr></hr>
                  {productRequest.map((info: any) => {
                    return <p key={info.id}>{info.productDescription}</p>;
                  })}
                </>
              )}
              <p className="sub-title" onClick={handleDesignerInfo}>
                Designer Information
              </p>
              {designer && (
                <>
                  <hr></hr>
                  {productRequest.map((info: any) => {
                    return <p key={info.id}>{info.designerInfo}</p>;
                  })}
                </>
              )}
              <p className="sub-title" onClick={handleDetailsCare}>
                Details and Care
              </p>
              {detailsCare && (
                <>
                  <hr></hr>
                  {productRequest.map((info: any) => {
                    return <p key={info.id}>{info.detailsCare}</p>;
                  })}
                </>
              )}

              <div className="personal-info">
                <p className="personal" onClick={handlePersonalInfo}>
                  Personal Info
                </p>
                <hr></hr>
                <div className="personal-info-flex">
                  <div className="customer-name">
                    {personalInfo && (
                      <>
                        <p className="name-title">Name:</p>
                        {customerData.map((info: any) => {
                          return <p key={info.id}>{info.customerName}</p>;
                        })}
                      </>
                    )}
                  </div>
                  <div className="customer-email">
                    {personalInfo && (
                      <>
                        <p className="email-title">Quantity:</p>
                        {customerData.map((info: any) => {
                          return <p key={info.id}>{info.quantity}</p>;
                        })}
                      </>
                    )}
                  </div>
                  <div className="customer-email">
                    {personalInfo && (
                      <>
                        <p className="email-title">Email:</p>
                        {customerData.map((info: any) => {
                          return <p key={info.id}>{info.customerEmail}</p>;
                        })}
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </ProductRequestWrap>
    </>
  );
};

export default Order;
