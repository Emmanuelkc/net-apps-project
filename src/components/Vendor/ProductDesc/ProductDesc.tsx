import React from 'react'
import {ProductDescWrap} from "../../../../styles/Vendor/productDescriptionStyle"
import { productDescription, productDescriptionTitle } from '../Utility/productDescData'

const ProductDesc = () => {
  return (
    <ProductDescWrap>
      <div className='page-title'>
        <h3>Product Description</h3>
      </div>
      <div className='container'>
      <table className='table'>
        <thead className='table-head'>
          {productDescriptionTitle.map((row)=>{
            return(
              <tr>
                <th>{row.col1}</th>
                <th className='mid-row'>{row.col2}</th>
                <th>{row.col3}</th>
              </tr>
            )
          })}
        </thead>
        
        <tbody className='table-body'>
          {productDescription.map((row) =>{
            return(
              <tr key={row.loanPrdtDesc}>
                <td className='right-row'>{row.loanPrdtDesc}</td>
                <td className='mid-row'>{row.details}</td>
                <td>{row.status}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
      </div>
      {/* <div className='product-top-flex'>
        <div className='square-left'>
          <h3>Square Left</h3>
        </div>
        <div className='square-right'>
          <h3>Square Right</h3>
        </div>
      </div> */}
      <div className="chart">

      </div>
      <div className='end-note'>

      </div>
    </ProductDescWrap>
  )
}

export default ProductDesc