import FormWrapper  from "./FormWrapper"

import * as React from 'react';

type UserData = {
  firstName: string
  lastName: string
  email: string
}

type UserFormProps = UserData & {
  updateFields: (fields: Partial<UserData>) => void
}

    export function UserForm({firstName, lastName, email, updateFields,}: UserFormProps) {
  return (
             <FormWrapper title="Personal Information" >
              <label  className=" mt-4 mb-4 block text-1xl font-bold text-gray-700">First Name</label>
              <input
                  className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
                autoFocus
                required
                type="text"
                value={firstName}
                onChange={e => updateFields({ firstName: e.target.value })}
              />
              <label  className=" mt-4 mb-4  block text-1xl font-bold text-gray-700">Last Name</label>
              <input
                  className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
                required
                type="text"
                value={lastName}
                onChange={e => updateFields({ lastName: e.target.value })}
              />
              <label  className=" mt-4 mb-4 block text-1xl font-bold text-gray-700">Email Address</label>
              <input
                  className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
                required
                min={1}
                type="email"
                value={email}
                onChange={e => updateFields({ email: e.target.value })}
              />
    </FormWrapper>
  )
}
