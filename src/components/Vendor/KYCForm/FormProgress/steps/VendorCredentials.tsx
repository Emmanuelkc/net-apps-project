import React, { useState } from "react";
import { KYCFileUploadWrap } from "../../../../../../styles/Vendor/kycFormStyle";
import { ImUpload } from "react-icons/im";

const ProductUpload = ({ form }: any) => {
  const [uploaded, setUploaded] = useState();
  const [image, setImage] = useState("");
  const [formInputData, setFormInputData] = useState<Record<string, any>>({});

  const uploadHandler = (e: any) => {
    e.preventDefault();
    const file = e.target.files[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      const read = reader.result?.toString() as string;
      setImage(read);
    };

    reader.readAsDataURL(file);
    console.log(e.target.files);
  };

  const handleSubmitted = (e: any) => {
    e.preventDefault();
    console.log("Product properties:", formInputData);
  };

  return (
    <KYCFileUploadWrap>
      <div className="grid text-800 mt-3">
        <div className=" surface-10 text-800  col-12 md:col-12 overflow-hidden  text-center  align-items-center">
          <div className="terms-section text-800 text-2xl font-bold mb-3 text-primary ">
            <h2 className="block text-1xl font-bold mb-3">
              Business Registration Document
            </h2>
          </div>
          <form className="product-data-container" onSubmit={handleSubmitted}>
            <div className="avatar">
              <div className="image-uploaded">
                {/* <img src={image} /> */}
                <p>{image}</p>
              </div>

              <button className="upload-btn">
                <label onChange={(e) => uploadHandler(e)} htmlFor="fileupload">
                  <input id="fileupload" hidden type="file" name="imagefile" />
                  Click to select file
                  {/* <ImUpload className="upload-btn-icon" /> */}
                </label>
              </button>
            </div>
          </form>
        </div>
      </div>
    </KYCFileUploadWrap>
  );
};

export default ProductUpload;
