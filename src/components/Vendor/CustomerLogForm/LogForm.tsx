import React from "react";
import { LogFormWrap } from "../../../../styles/Vendor/customerLogStyle";
import { customerLog } from "../Utility/VendorData";
import Image from "next/image";
import customerHero from "../../../../public/Assets/customerLog/customerHero.png";

const LogForm = () => {
  return (
    <LogFormWrap>
      <div className="outter-wrap">
        <div className="about">
          <div className="form-title">Customer Details</div>
          <button>Active</button>
        </div>
        {customerLog.map((info: any) => {
          return (
            <>
              <div className="info-section">
                <div className="customer-name">
                  <p className="title">{info.nameLabel}</p>
                  <p key={info.id} className="data-info">
                    {info.name}
                  </p>
                </div>
                <div className="customer-email">
                  <p className="title">{info.emailLabel}</p>
                  <p key={info.id} className="data-info">
                    {info.email}
                  </p>
                </div>
                <div className="customer-phone">
                  <p className="title">{info.phoneLabel}</p>
                  <p key={info.id} className="data-info">
                    {info.phone}
                  </p>
                </div>
              </div>

              <div className="payment-section">
                <div className="">
                  <p className="title">{info.paymentLabel}</p>
                  <p key={info.id} className="data-info">
                    {info.paymentOption}
                  </p>
                </div>
                <div className="customer-email">
                  <p className="title">{info.priceLabel}</p>
                  <p key={info.id} className="data-info">
                    {info.price}
                  </p>
                </div>
              </div>

              <div className="location-section">
                <div className="">
                  <p className="title">{info.addressLabel}</p>
                  <p key={info.id} className="data-info">
                    {info.address}
                  </p>
                </div>
                <div className="customer-email">
                  <p className="title">{info.countryLabel}</p>
                  <p key={info.id} className="data-info">
                    {info.country}
                  </p>
                </div>
              </div>
            </>
          );
        })}

        <div className="customer-info">
          <div className=""></div>
          {/* <div className="customer-email">
            {customerLog.map((info: any) => {
              return (
                <>
                </>
              );
            })}
          </div> */}
          {/* <div className="customer-email">
            {customerLog.map((info: any) => {
              return (
                <>
                </>
              );
            })}
          </div> */}
        </div>
      </div>
      <div className="image">
        <Image layout="intrinsic" width={400} height={350} src={customerHero} />
      </div>
    </LogFormWrap>
  );
};

export default LogForm;
