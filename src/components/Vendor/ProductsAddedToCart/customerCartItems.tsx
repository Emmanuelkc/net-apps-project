import router from "next/router";
import React, { useEffect, useState } from "react";
import { NumericFormat } from "react-number-format";
import { CartItemsWrap } from "../../../../styles/Vendor/customerCartItems";
import { cartItemsHeader } from "../Utility/VendorData";
import ReactPaginate from "react-paginate";
import { MdReadMore } from "react-icons/md";
import { BsFillPatchCheckFill } from "react-icons/bs";
import { TbCurrencyNaira } from "react-icons/tb";

const CustomerCartItems = ({ cartItems }: any) => {
  const [currentItems, setCurrentItems] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const itemsPerPage = 6;

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(cartItems.slice(itemOffset, endOffset));
    return setPageCount(Math.ceil(cartItems.length / itemsPerPage));
  }, [itemOffset, itemsPerPage, cartItems]);

  const handlePageClick = (event: any) => {
    const newOffset = (event.selected * itemsPerPage) % cartItems.length;
    setItemOffset(newOffset);
  };

  const productRequest = (e: any) => {
    e.preventDefault();
    router.push("vendor/productRequest");
  };

  return (
    <CartItemsWrap>
      <div className="table-head-text">
        <h3 className="table-head">Customer Request List</h3>
      </div>
      <table className="table">
        <thead>
          <tr>
            {cartItemsHeader.map((item: any) => {
              return <th key={item.id}>{item.title}</th>;
            })}
          </tr>
        </thead>

        <tbody>
          {currentItems.map((item: any) => {
            return (
              <>
                <tr data-heading={cartItems} key={item.id}>
                  <td data-label="S/NO" key={item.id}>
                    {item.id}
                  </td>
                  <td data-label="Customer Name" key={item.id}>
                    {item.customerName}
                  </td>
                  <td data-label="Phone Contact" key={item.id}>
                    {item.phone}
                  </td>
                  <td data-label="Quantity" key={item.id}>
                    {item.quantity}
                  </td>
                  <td data-label="Total Amount" key={item.id}>
                    {/* <TbCurrencyNaira className="naira-icon" /> */}
                    <NumericFormat
                      value={item.totalAmount}
                      thousandsGroupStyle="lakh"
                      // thousandSeparator=","
                      displayType="text"
                      renderText={(value) => <b>{value}</b>}
                    />
                  </td>
                  <td data-label="Status" key={item.id}>
                    {item.status === "approved" ? (
                      <div className="approved">
                        {item.status}
                        <BsFillPatchCheckFill className="status-icon" />
                      </div>
                    ) : (
                      <button className="pending">
                        {item.status}
                        <BsFillPatchCheckFill className="status-icon" />
                      </button>
                    )}
                  </td>
                  <td data-label="Read more" key={item.id}>
                    <button onClick={productRequest}>
                      {item.moreDetails}
                      <MdReadMore className="read-icon" />
                    </button>
                  </td>
                </tr>
              </>
            );
          })}
        </tbody>

        <tbody>
          <tr></tr>
        </tbody>
        {/* <button onClick={productRequest}>View Details</button> */}
      </table>
      <div className="custom">
        <ReactPaginate
          previousLabel={"< Previous"}
          nextLabel={"Next >"}
          breakLabel={"..."}
          pageCount={pageCount}
          pageRangeDisplayed={4}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          pageLinkClassName={"page-number"}
          breakLinkClassName={"page-item"}
          previousLinkClassName={"previous-btn"}
          nextLinkClassName={"next-btn"}
          activeLinkClassName={"active"}
        />
      </div>
    </CartItemsWrap>
  );
};

export default CustomerCartItems;
