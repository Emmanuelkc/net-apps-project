import React, { useState } from "react";
import { ProductUploadWrap } from "../../../../styles/Vendor/productUploadStyle";
import Image from "next/image";
import {
  descriptionData,
  productCategory,
  productFormDataA,
  productFormDataB,
  productOptions,
} from "../Utility/VendorData";
import { ImUpload } from "react-icons/im";
import { AiOutlineFileDone } from "react-icons/ai";

type items = [category: string, industrial: string];

const ProductUpload = ({ form }: any) => {
  const [product, setProduct] = useState("");
  const [uploaded, setUploaded] = useState();
  const [image, setImage] = useState("");
  const [keyword, setKeyword] = useState("");
  const [productName, setProductName] = useState("");
  const [unitPrice, setUnitPrice] = useState("");
  const [formInputData, setFormInputData] = useState<Record<string, any>>({});

  const uploadHandler = (e: any) => {
    e.preventDefault();
    const file = e.target.files[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      const read = reader.result?.toString() as string;
      setImage(read);
    };

    reader.readAsDataURL(file);
    console.log(e.target.files);
  };

  const optionChangeHandler = (event: any) => {
    console.log("Product Category - ", event.target.value);
  };

  const handleSubmitted = (e: any) => {
    e.preventDefault();
    console.log("Product properties:", formInputData);
    console.log("Uploaded product name:", formInputData.productName);
    console.log("Unit price:", formInputData.unitPrice);
    console.log("Product keywords:", formInputData.keyword);

    setProductName("");
    setUnitPrice("");
    setKeyword("");
  };

  return (
    <ProductUploadWrap>
      <div className="title">
        <h3>+ Add New Product</h3>
      </div>

      <form className="product-data-container" onSubmit={handleSubmitted}>
        <div className="product-data">
          <div className="avatar sm:col-12 md:col-6">
            <picture className="image-preview">
              {image ? (
                <div className="image-uploaded">
                  <img
                    src={image}
                    // layout={"intrinsic"}
                    // width={150}
                    // height={150}
                  />
                </div>
              ) : (
                <div className="image-uploaded">
                  <img
                    src={
                      uploaded
                        ? URL.createObjectURL(uploaded)
                        : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
                    }
                    alt=""
                  />
                </div>
              )}
            </picture>
            <button className="upload-btn">
              <label onChange={(e) => uploadHandler(e)} htmlFor="fileupload">
                <input id="fileupload" hidden type="file" name="imagefile" />
                Upload
                <ImUpload className="upload-btn-icon" />
              </label>
            </button>
          </div>
          <div className="sm:col-12 md:col-6">
            {productCategory.map((form: any) => {
              return (
                <div className="form-data-input-top">
                  <label htmlFor="categories" className="select-category">
                    {form.label}
                  </label>
                  <select
                    className="category-options"
                    // onChange={optionChangeHandler}
                    onChange={(e) =>
                      setFormInputData({
                        ...formInputData,
                        [form.name]: e.target.value,
                      })
                    }
                    value={formInputData[form.name]}
                    name="categories"
                    required={form.required}
                    placeholder={form.placeholder}
                  >
                    <option>Select Product Category</option>
                    div
                    {productOptions.map((categories, item) => {
                      return (
                        <>
                          <option key={item}>{categories}</option>
                        </>
                      );
                    })}
                  </select>
                </div>
              );
            })}
            {productFormDataA.map((form: any) => {
              return (
                <div className="form-data-input-top" key={form.id}>
                  <label className="form-label">
                    {form.label}
                    <input
                      className="input-field"
                      name={form.name}
                      type={form.dataType}
                      onChange={(e) =>
                        setFormInputData({
                          ...formInputData,
                          [form.name]: e.target.value,
                        })
                      }
                      value={formInputData[form.name]}
                      required={form.required}
                      placeholder={form.placeholder}
                    />
                  </label>
                </div>
              );
            })}
          </div>
          <div className="sm:col-12 md:col-6">
            <div className="form-data col-6">
              {productFormDataB.map((form: any) => {
                return (
                  <div className="form-data-input-bottom" key={form.id}>
                    <label className="form-label">{form.label}</label>
                    <input
                      className="input-field"
                      type={form.dataType}
                      name={form.name}
                      onChange={(e) =>
                        setFormInputData({
                          ...formInputData,
                          [form.name]: e.target.value,
                        })
                      }
                      value={formInputData[form.name]}
                      required={form.required}
                      placeholder={form.placeholder}
                    />
                  </div>
                );
              })}
              {descriptionData.map((form: any) => {
                return (
                  <div className="form-data-input-bottom" key={form.id}>
                    <label className="form-label">{form.label}</label>
                    <textarea
                      className="description-input"
                      name={form.name}
                      onChange={(e) =>
                        setFormInputData({
                          ...formInputData,
                          [form.name]: e.target.value,
                        })
                      }
                      value={formInputData[form.name]}
                      placeholder="Type product description here..."
                    ></textarea>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="submit-btn">
          <button type="submit" className="btn">
            Submit
            <AiOutlineFileDone className="upload-btn-icon" />
          </button>
        </div>
      </form>
    </ProductUploadWrap>
  );
};

export default ProductUpload;
