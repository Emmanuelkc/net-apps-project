import React from "react";
import { CustomerLogWrap } from "../../../../styles/Vendor/customerLogStyle";
import Image from "next/image";
import customerHistory from "../../../../public/Assets/customerLog/customerHistory.png";
import wishTo from "../../../../public/Assets/customerLog/wishTo.svg";
import bankLoan from "../../../../public/Assets/customerLog/bankLoan.svg";
import loanCategory from "../../../../public/Assets/customerLog/loanCategory.svg";
import LogForm from "../CustomerLogForm/LogForm";

const CustomerHistory = () => {
  return (
    <CustomerLogWrap>
      <div className="log-hero">
        <div className="flex-left">
          <p className="text-large">
            The best and instant loan service provides what you want.
          </p>
          <p className="text-small">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam
            fuga molestias voluptatum placeat quaerat tempore ab ea et obcaecati
            enim. Reiciendis, quo amet. Lorem ipsum dolor sit amet consectetur
            adipisicing elit. Numquam fuga molestias voluptatum placeat quaerat
            tempore ab ea et obcaecati enim. Reiciendis, quo amet.
          </p>
        </div>

        <div className="flex-right">
          <Image
            layout="intrinsic"
            width={500}
            height={500}
            src={customerHistory}
          />
        </div>
      </div>
      <div className="categories">
        <div className="category-title">
          <p>How to stimulate loans with three types of states</p>
        </div>
        <div className="category-flex">
          <div>
            <Image layout="intrinsic" width={100} height={100} src={wishTo} />
            <p className="sub-title">Make your own wish</p>
            <p className="description">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam,
              ratione illo minima fugit expedita ad perspiciatis quis aliquam
              quasi pariatur?
            </p>
          </div>
          <div>
            <Image
              layout="intrinsic"
              width={100}
              height={100}
              src={loanCategory}
            />
            <p className="sub-title">Choose your loan type</p>
            <p className="description">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam,
              ratione illo minima fugit expedita ad perspiciatis quis aliquam
              quasi pariatur?
            </p>
          </div>
          <div>
            <Image layout="intrinsic" width={100} height={100} src={bankLoan} />
            <p className="sub-title">Get money in your account</p>
            <p className="description">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam,
              ratione illo minima fugit expedita ad perspiciatis quis aliquam
              quasi pariatur?
            </p>
          </div>
        </div>
      </div>
      <LogForm />
    </CustomerLogWrap>
  );
};

export default CustomerHistory;
