import React from "react";
import { CustomerLogBtn } from "../../../../styles/Vendor/dashboard";
import { MdOutlineHistoryEdu } from "react-icons/md";
import { Router } from "express";
import router from "next/router";

const CustomerLog = () => {
  function handleCustomerLog(e: any) {
    e.preventDefault();
    router.push(`vendor/customerHistory`);
  }

  return (
    <CustomerLogBtn>
      <button onClick={handleCustomerLog}>
        <MdOutlineHistoryEdu className="log" />
        Customers Log
      </button>
    </CustomerLogBtn>
  );
};

export default CustomerLog;
