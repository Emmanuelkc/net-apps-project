import React, { useEffect, useState } from "react";
import { ProductTableWrap } from "../../../../styles/Vendor/dashboard";
import { NumericFormat } from "react-number-format";
import ReactPaginate from "react-paginate";
import router from "next/router";
import { productTableDataHeader } from "../Utility/VendorData";
import Image from "next/image";

type items = {
  id: string;
  label: string;
  img: string;
  productCategory: string;
  productName: string;
  brand: string;
  unitPrice: string;
  quantity: string;
  quantitySold: string;
  quantityAvailable: string;
  dateUploaded: string;
};

const ProductTable = (
  { productTableData }: any,
  { productDescriptionId }: any
) => {
  const [currentItems, setCurrentItems] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const itemsPerPage = 6;

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(productTableData.slice(itemOffset, endOffset));
    return setPageCount(Math.ceil(productTableData.length / itemsPerPage));
  }, [itemOffset, itemsPerPage, productTableData]);

  const handlePageClick = (event: any) => {
    const newOffset = (event.selected * itemsPerPage) % productTableData.length;
    setItemOffset(newOffset);
  };

  const productDescription = (e: any) => {
    e.preventDefault();
    router.push(`vendor/${productDescriptionId}`);
  };

  return (
    <ProductTableWrap>
      <div className="card container">
        <div className="table-head-text">
          <h3 className="table-head">Product List</h3>
        </div>
        <table className="table">
          <thead>
            <tr>
              {productTableDataHeader.map((items) => {
                return <th key={items.id}>{items.label}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            {currentItems.map((items: any) => {
              return (
                <tr
                  key={items.id}
                  data-heading={productTableData}
                  onClick={productDescription}
                >
                  <td data-label="Avatar">
                    {items.img}
                    <Image
                      className="col-6"
                      src={items.bag}
                      alt={items.bag}
                      width={43}
                      height={40}
                      objectFit="contain"
                    />
                  </td>
                  <td data-label="Product Category">{items.productCategory}</td>
                  <td data-label="Product Name">{items.productName}</td>
                  <td data-label="Unit Price">
                    <NumericFormat
                      value={items.unitPrice}
                      thousandsGroupStyle="lakh"
                      // thousandSeparator=","
                      displayType="text"
                      renderText={(value) => <b>{value}</b>}
                    />
                  </td>
                  <td data-label="Quantity" className="font-bold">
                    {items.totalQuantity}
                  </td>
                  <td data-label="Quantity Sold" className="font-bold">
                    {items.quantitySold}
                  </td>
                  <td data-label="Quantity Available" className="font-bold">
                    {items.quantityAvailable}
                  </td>
                  <td data-label="Date Uploaded">{items.dateUploaded}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="custom">
        <ReactPaginate
          previousLabel={"< Previous"}
          nextLabel={"Next >"}
          breakLabel={"..."}
          pageCount={pageCount}
          pageRangeDisplayed={4}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          pageLinkClassName={"page-number"}
          breakLinkClassName={"page-item"}
          previousLinkClassName={"previous-btn"}
          nextLinkClassName={"next-btn"}
          activeLinkClassName={"active"}
        />
      </div>
    </ProductTableWrap>
  );
};

export default ProductTable;
