import React, { useEffect, useState, Fragment } from "react";
import { ProductTableWrap } from "../../../../styles/Vendor/dashboard";
import { NumericFormat } from "react-number-format";
import ReactPaginate from "react-paginate";
import router from "next/router";
import { cartItemsHeader } from "../Utility/VendorData";
import Image from "next/image";
import { BsFillPatchCheckFill } from "react-icons/bs";
import { MdReadMore } from "react-icons/md";

const ApprovedRequest = ({ cartItems }: any) => {
  const [currentItems, setCurrentItems] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const itemsPerPage = 6;

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(cartItems.slice(itemOffset, endOffset));
    return setPageCount(Math.ceil(cartItems.length / itemsPerPage));
  }, [itemOffset, itemsPerPage, cartItems]);

  const handlePageClick = (event: any) => {
    const newOffset = (event.selected * itemsPerPage) % cartItems.length;
    setItemOffset(newOffset);
  };

  const productRequest = (e: any) => {
    e.preventDefault();
    router.push("vendor/productRequest");
  };

  //   const productDescription = (e: any) => {
  //     e.preventDefault();
  //     router.push(`vendor/${productDescriptionId}`);
  //   };

  return (
    <ProductTableWrap>
      <div className="card container">
        <div className="table-head-text">
          <h3 className="table-head">Approved Requests</h3>
        </div>
        <table className="table">
          <thead>
            <tr>
              {cartItemsHeader.map((items: any) => {
                return <th key={items.id}>{items.title}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            {currentItems.map((items: any) => {
              return (
                <tr
                  key={items.id}
                  data-heading={cartItems}
                  //   onClick={productDescription}
                >
                  {items.status === "approved" && (
                    <Fragment>
                      <td data-label="S/N">{items.id}</td>
                      {/* <td data-label="Avatar">
                    {items.img}
                    <Image
                      className="col-6"
                      src={items.bag}
                      alt={items.bag}
                      width={43}
                      height={40}
                      objectFit="contain"
                    />
                  </td> */}
                      <td data-label="Customer Name">{items.customerName}</td>
                      <td data-label="Phone Contact">{items.phone}</td>
                      <td data-label="Quantity" className="font-bold">
                        {items.quantity}
                      </td>
                      <td data-label="Total Amount">
                        <NumericFormat
                          value={items.totalAmount}
                          thousandsGroupStyle="lakh"
                          // thousandSeparator=","
                          displayType="text"
                          renderText={(value) => <b>{value}</b>}
                        />
                      </td>
                      <td data-label="Status" key={items.id}>
                        {items?.status ? (
                          <div className="approved">
                            {items.status}
                            <BsFillPatchCheckFill className="status-icon" />
                          </div>
                        ) : (
                          <button className="pending">
                            {items.status}
                            <BsFillPatchCheckFill className="status-icon" />
                          </button>
                        )}
                      </td>
                      <td data-label="Read more" key={items.id}>
                        <button onClick={productRequest}>
                          {items.moreDetails}
                          <MdReadMore className="read-icon" />
                        </button>
                      </td>
                    </Fragment>
                  )}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="custom">
        <ReactPaginate
          previousLabel={"< Previous"}
          nextLabel={"Next >"}
          breakLabel={"..."}
          pageCount={pageCount}
          pageRangeDisplayed={4}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          pageLinkClassName={"page-number"}
          breakLinkClassName={"page-item"}
          previousLinkClassName={"previous-btn"}
          nextLinkClassName={"next-btn"}
          activeLinkClassName={"active"}
        />
      </div>
    </ProductTableWrap>
  );
};

export default ApprovedRequest;
