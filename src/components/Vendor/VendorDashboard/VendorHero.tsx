import React, { useState, FC } from "react";
import { useRouter } from "next/router";
import "primeflex/primeflex.css";
import "primereact/resources/themes/saga-blue/theme.css";
import "primeicons/primeicons.css";
import { BiCloudUpload } from "react-icons/bi";
import { VendorHeroWrap } from "../../../../styles/Vendor/dashboard";
import { Button } from "primereact/button";

const VendorHero = ({ uploadPrdt }: any, { productOrder }: any) => {
  const router = useRouter();

  const itemUploadPage = (e: any) => {
    e.preventDefault();
    router.push("uploadProduct");
  };

  return (
    <VendorHeroWrap>
      <div className="outter">
        <div className="align-items-start lg:justify-content-between lg:flex-row">
          <div className="vendor-items">
            <div className="font-medium text-800  flex">
              <p className="upload-prdt">Product Upload and Management</p>
            </div>
            <Button className="product-order" onClick={itemUploadPage}>
              Upload product <BiCloudUpload className="upload-icon" />
            </Button>
          </div>
        </div>
      </div>
    </VendorHeroWrap>
  );
};

export default VendorHero;
