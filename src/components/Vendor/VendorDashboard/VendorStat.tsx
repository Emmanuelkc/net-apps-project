import React, { FC } from "react";
import "primeflex/primeflex.css";
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css";
import { TbCurrencyNaira } from "react-icons/tb";
import { BsCheckLg } from "react-icons/bs";
import { GrCompliance } from "react-icons/gr";
import { FcDebt } from "react-icons/fc";
import { VendorStatWrap } from "../../../../styles/Vendor/dashboard";

const VendorStat = () => {
  return (
    <>
      <VendorStatWrap>
        <div className="grid m-2 card-container">
          <div className="col-12 md:col-6 lg:col-3">
            <div className="surface-0 shadow-2 card">
              <div className="flex justify-content-between p-2">
                <div>
                  <span className="text-1xl card-title">Total Product</span>
                  <h2>0</h2>
                </div>
                <div className="naira-circle">
                  <GrCompliance className="total-icon" />
                </div>
              </div>
              <span className="text-green-500 link-text">
                View all products <i className="pi pi-chart-line"></i>
              </span>
            </div>
          </div>
          <div className="col-12 md:col-6 lg:col-3">
            <div className="surface-0 shadow-2 card">
              <div className="flex justify-content-between p-2">
                <div>
                  <span className="text-1xl card-title">Approved Products</span>
                  <h2>0</h2>
                </div>
                <div className="naira-circle">
                  <BsCheckLg className="check-icon" />
                </div>
              </div>
              <span className="text-green-500 link-text">
                View all approved products <i className="pi pi-chart-line"></i>
              </span>
            </div>
          </div>
          <div className="col-12 md:col-6 lg:col-3">
            <div className="surface-0 shadow-2 card">
              <div className="flex justify-content-between p-2">
                <div>
                  <span className="card-title text-black-600 text-1xl">
                    Sold Products
                  </span>
                  <h2>0</h2>
                </div>
                <div className="naira-circle">
                  <TbCurrencyNaira className="naira-icon" />
                </div>
              </div>
              <span className="text-green-500 link-text">
                View sold products <i className="pi pi-chart-line"></i>
              </span>
            </div>
          </div>
          <div className="col-12 md:col-6 lg:col-3">
            <div className="surface-0 shadow-2 card-last">
              <div className="flex justify-content-between p-2">
                <div>
                  <span className="text-red-600 text-1xl card-title">
                    Total Debts
                  </span>
                  <h2>0</h2>
                </div>
                <div className="naira-circle">
                  <FcDebt className="debt-icon" />
                </div>
              </div>
              <span className="text-green-500 link-text">
                View all debts <i className="pi pi-chart-line"></i>
              </span>
            </div>
          </div>
        </div>
      </VendorStatWrap>
    </>
  );
};

export default VendorStat;
