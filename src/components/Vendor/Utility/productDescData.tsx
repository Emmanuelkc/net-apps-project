export const productDescriptionTitle = [
  {
    col1: "Loan Details",
    col2: "Description",
    col3: "Status",
  },
];

export const productDescription = [
  {
    loanPrdtDesc: "Date Uploaded",
    details: "24/09/2022",
    status: "Available",
  },
  {
    loanPrdtDesc: "Product Category",
    details:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos veritatis odio dolor. Tenetur perferendis aut corporis fuga non temporibus reiciendis voluptatem, illo dicta harum ratione libero. Suscipit sequi officiis earum.",
    status: "Sold",
  },
  {
    loanPrdtDesc: "Product Name",
    details:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos veritatis odio dolor. Tenetur perferendis aut corporis fuga non temporibus reiciendis voluptatem, illo dicta harum ratione libero. Suscipit sequi officiis earum.",
    status: "Available",
  },
  {
    loanPrdtDesc: "Keywords",
    details: "Footwear, Fashion, Trending, Mens, Celebrity",
    status: "Sold",
  },
  {
    loanPrdtDesc: "Additional Details",
    details:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos veritatis odio dolor. Tenetur perferendis aut corporis fuga non temporibus reiciendis voluptatem, illo dicta harum ratione libero. Suscipit sequi officiis earum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos veritatis odio dolor. Tenetur perferendis aut corporis fuga non temporibus reiciendis voluptatem, illo dicta harum ratione libero.",
    status: "Sold",
  },
];

export const productRequest = [
  {
    id: 70000,
    productDescription:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore id quod inventore, accusantium, exercitationem aut nesciunt adipisci quos harum incidunt deserunt corrupti ab distinctio unde similique, earum neque perferendis!",
  },
  {
    id: 70001,
    designerInfo:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore.",
  },
  {
    id: 70002,
    detailsCare:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore id quod inventore, accusantium, exercitationem aut nesciunt adipisci quos harum!",
  },
  {
    id: 70003,
    price: "9000",
  },
];

export const customerData = [
  {
    id: 80001,
    customerName: "Emeka Adedoyin Idris",
  },
  {
    id: 80002,
    customerEmail: "emekaadeidris@gmail.com",
  },
  {
    id: 80003,
    quantity: "4",
  },
  {
    id: 80004,
    productCategory: "Fashion",
  },
  {
    id: 80004,
    productStatus: "Available",
  },
  {
    id: 80006,
    viewDetails: "View Details",
  },
];
