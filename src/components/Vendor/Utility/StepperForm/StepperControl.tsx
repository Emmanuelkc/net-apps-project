import React from "react";
import { ButtonsContainer } from "../../../../../styles/AccountCategory/loanAccountCategoryStyle";
import { useRouter } from "next/router";
import Swal from "sweetalert2";
import { Button } from "primereact/button";

const StepperControl = ({ handleClick, currentStep, steps }: any) => {
  const router = useRouter();
  const successAlert = () => {
    Swal.fire({
      titleText: "Your Information was has been Verified Successfully!",
      color: "#0c2154",
      html: "<p>Your verification was success. Proceed to your dashboard.<p/>",
      icon: "success",
      confirmButtonText: "Return to Dashboard",
      confirmButtonColor: "#3085d6",
      customClass: {
        title: "info",
      },
      showConfirmButton: false,
      timer: 1000,
      // timer: 86400,
      background: "#e7e9ed",
    }).then(function () {
      router.push("/vendor/dashboard");
    });
  };

  return (
    <ButtonsContainer>
      <Button
        onClick={() => handleClick()}
        className={`${
          currentStep === 1 ? "Button opacity-50 cursor-not-allowed" : "Button"
        }`}
      >
        prev
      </Button>

      {currentStep === 4 ? (
        <Button className="Button" type="submit" onClick={successAlert}>
          Confirm
        </Button>
      ) : (
        <Button className="Button" onClick={() => handleClick("next")}>
          {currentStep === steps.length - 1 ? "next" : "next"}
        </Button>
      )}
      {/*<Button onClick={()=> handleClick("next")} >*/}
      {/*    /!*{currentStep === steps.length -1 ? "next" : "next"}*!/*/}
      {/*    /!*{currentStep === steps.length -1 ? "next" : "next" }*!/*/}
      {/*    {currentStep === 3 ? <Button type="submit" onClick={successAlert}>Confirm</Button> :"next"}*/}
      {/*</Button>*/}
    </ButtonsContainer>
  );
};

export default StepperControl;
