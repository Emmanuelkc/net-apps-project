import  FormWrapper  from "./FormWrapper"

type AccountData = {
  email: string
  password: string
}

type AccountFormProps = AccountData & {
  updateFields: (fields: Partial<AccountData>) => void
}

export function AccountForm({email, password, updateFields,}: AccountFormProps) {

  return (
    <FormWrapper title="Account Creation">
      <label className=" mt-4 block text-1xl font-bold text-gray-700">Email</label>
      <input
          className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
          autoFocus
        required
        type="email"
        value={email}
        onChange={e => updateFields({ email: e.target.value })}
      />
      <label className=" mt-4 block text-1xl font-bold text-gray-700">Password</label>
      <input
          className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
          required
        type="password"
        value={password}
        onChange={e => updateFields({ password: e.target.value })}
      />
    </FormWrapper>
  )
}
