import { ReactNode } from "react"
import styled from "styled-components";
import React from "react"

type FormWrapperProps = {
  title: string
  children: ReactNode
}

const FormWrapper=({ title, children }: FormWrapperProps)=> {


  return (
    <div className=' m-4 p-2  ' style={{width:"70%",}}>
      <h2  className="text-3xl font-bold  text-teal-900 uppercase text-gray-900 "
           style={{ textAlign: "center", margin: 0, marginBottom: "2rem" }}>
        {title}
      </h2>
      <Wrapper>
        {children}
      </Wrapper>
    </div>
  )
}

const Wrapper = styled.div`
  display: grid;
  gap: 0.2rem 0.2rem;
  padding: 1rem;
  //justify-content: flex-start;
  //grid-template-columns: auto minmax(auto, 400px);

`

export default FormWrapper;