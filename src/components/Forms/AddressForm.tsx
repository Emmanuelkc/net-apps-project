import  FormWrapper  from "./FormWrapper"
import * as React from 'react';


type AddressData = {
  nin: string
  bvn: string
  state: string
    country: string
}

type AddressFormProps = AddressData & {
  updateFields: (fields: Partial<AddressData>) => void
}

export function AddressForm({nin, bvn, state, country, updateFields,}: AddressFormProps) {

  return (
    <FormWrapper title="Personal Identification & Validation">
      <label className=" mt-4 block text-1xl font-bold text-gray-700">NIN</label>
      <input
          className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
          autoFocus
        required
        type="text"
        value={nin}
        onChange={e => updateFields({ nin: e.target.value })}
      />
      <label className=" mt-4 block text-1xl font-bold text-gray-700">BVN</label>
      <input
          className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
          required
        type="text"
        value={bvn}
        onChange={e => updateFields({ bvn: e.target.value })}
      />
      <label className="mt-4 block text-1xl font-bold text-gray-700">State</label>
      <input
          className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
          required
        type="text"
        value={state}
        onChange={e => updateFields({ state: e.target.value })}
      />
      <label className="mt-4 block text-1xl font-bold text-gray-700">Country</label>
      <input
          className="shadow-md shadow-gray-500 p-3 block  rounded  sm:text-sm"
          required
        type="text"
        value={country}
        onChange={e => updateFields({ country: e.target.value })}
      />
    </FormWrapper>
  )
}
