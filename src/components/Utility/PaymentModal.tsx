import React,{useState, useEffect} from "react";
import {ModalType, loanProductModalDetail} from "./modalData";
import {ModalWrap} from "../../../styles/BorrowerDashboard/ModalStyle/PaymentModalStyle"

type InputData = {
  name: string,
  required: boolean,
  label: string,
   placeholder:string,
  dataType: string,
  htmlElement?: string,
  defaultValue: string
}

const PaymentModal = ({setPaymodal, modalData}:{setPaymodal :any, modalData: InputData[]}) => {

  const onSubmit = (e: any) => {
    e.preventDefault();
  } 
  
  return (
    <ModalWrap>
      <div>
      <div className="modal" onSubmit={onSubmit}>
        {modalData.map(inputData => {
            return(
              <div key={inputData.label}>
              {
                ( <div>
                  {loanProductModalDetail.map(inputData =>{
                    return(
                      <div key={inputData.loanType} className="loan-modal-description">
                        <p className="loan-type">{inputData.loanType}</p>
                        <p className="loan-product">{inputData.loanProduct}</p>
                      </div>
                    )
                  })}
                  <label>{inputData.label}</label>
                  <input type={inputData.htmlElement} 
                    name={inputData.name} 
                    required={inputData.required} 
                    datatype={inputData.dataType} 
                    placeholder={inputData.placeholder}
                  />
                </div>
                )
              }
              </div>
            )
          })
        }
          <div className='button'>
            <button className="cancel" onClick={()=>setPaymodal({open: false, modalType: ModalType.NONE})}>Cancel</button>
            <button className="submit" onClick={()=>setPaymodal({open: false, modalType: ModalType.NONE})}>Submit</button>
          </div>
        </div>
      </div>
    </ModalWrap>
  )
}

export default PaymentModal