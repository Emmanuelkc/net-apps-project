// /Borrowers
export type LoanTransactionProps = {
    TransactionDetailsId?:string;
    id: string,
    product: string,
    applied: string,
    loanAmount: string,
    approvedAmount: string,
    approved: string,
    placeholder:string,
    due: string,
    status: string,
    repaymentStatusPaid: string
}

//investors Data type
export type TransactionProps = {
    id: string,
    name:string,
    product: string,
    applied: string,
    loanAmount: string,
    status: string,
    approvedAmount:string
    paidBackLoan:string
}


export type stepper ={
    step:string
}

//Ecommerce Data Type
export type CartProducts ={
    id: number
    category:string;
    cat:string;
    description: string;
    image:string;
    price:number;
    title:string;
    amount:number;
}
export type ProductItem ={
    item:CartProducts;
    viewProduct?:(productDetailsId:any)=>void
    handleAddToCart:(clickedItem:CartProducts)=>void
    handleRemoveFromCart:(id: number)=>void
};
export type CartHome = {
    cartItems: CartProducts[];
    addToCart: (clickedItem: CartProducts) => void;
    removeFromCart: (id: number) => void;
}
export type CartItems ={
    item: CartProducts;
    addToCart: (clickedItem: CartProducts)=>void;
    removeFromCart: (id: number) => void;
}





// Transaction InputDataProps
export type InputDataProps = {
    id:number,
    name: string,
    required: boolean,
    label: string,
    placeholder:string,
    dataType: string,
    htmlElement?: string,
    defaultValue: string
}

type items = {
    id: string,
    label: string,
    productCategory: string,
    productName: string,
    brand: string,
    unitPrice: string,
    quantity: string,
    quantitySold: string,
    dateUploaded: string
}

interface InputData{
    name: string,
    required: boolean,
    label: string,
    dataType: string,
    htmlElement?: string,
    defaultValue: string
    placeholder:string
}