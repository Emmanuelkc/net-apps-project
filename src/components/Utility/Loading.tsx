import Skeleton from 'react-loading-skeleton'

export const Loading =()=>{
    return(
        <div className="col-12 md:col-6 lg:col-3 container">
            <div className='product-grid-item'>
                <Skeleton/>
            </div>
        </div>
    )
}