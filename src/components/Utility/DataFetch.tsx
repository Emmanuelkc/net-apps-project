import union from "../../../public/Assets/bank logo/union.jpeg"
import access from "../../../public/Assets/bank logo/access.jpeg"
import bowen from "../../../public/Assets/bank logo/bowen.jpeg"
import aso from "../../../public/Assets/bank logo/aso.png"
import cf from "../../../public/Assets/bank logo/cf.jpeg"
import chevron from "../../../public/Assets/bank logo/chevron.jpeg"
import citi from "../../../public/Assets/bank logo/citi.jpeg"
import diamond from "../../../public/Assets/bank logo/diamond.jpeg"
import eco from "../../../public/Assets/bank logo/eco.png"
import ekondo from "../../../public/Assets/bank logo/ekondo.jpeg"
import fcmb from "../../../public/Assets/bank logo/fcmb.png"
import fidelity from "../../../public/Assets/bank logo/fidelity.png"
import firstbank from "../../../public/Assets/bank logo/firstbank.png"
import globus from "../../../public/Assets/bank logo/globus.png"
import gtb from "../../../public/Assets/bank logo/gt.png"
import hasal from "../../../public/Assets/bank logo/hasal.jpeg"
import heritage from "../../../public/Assets/bank logo/heritage.png"
import jaiz from "../../../public/Assets/bank logo/jaiz.png"
import keystone from "../../../public/Assets/bank logo/keystone.jpeg"
import kuda from "../../../public/Assets/bank logo/kuda.jpeg"
import one from "../../../public/Assets/bank logo/one.jpeg"
import paga from "../../../public/Assets/bank logo/paga.png"
import parallex from "../../../public/Assets/bank logo/parallex.jpeg"
import paycom from "../../../public/Assets/bank logo/paycom.png"
import polaris from "../../../public/Assets/bank logo/polaris.jpeg"
import providus from "../../../public/Assets/bank logo/providus.png"
import rubies from "../../../public/Assets/bank logo/rubies.png"
import scb from "../../../public/Assets/bank logo/scb.png"
import standard from "../../../public/Assets/bank logo/standard.jpeg"
import starling from "../../../public/Assets/bank logo/starling.png"
import suntrust from "../../../public/Assets/bank logo/suntrust.jpeg"
import taj from "../../../public/Assets/bank logo/taj.png"
import trust from "../../../public/Assets/bank logo/trust.png"
import uba from "../../../public/Assets/bank logo/uba.png"
import unity from "../../../public/Assets/bank logo/unity.png"
import vfd from "../../../public/Assets/bank logo/vfd.jpeg"
import wema from "../../../public/Assets/bank logo/wema.png"
import zenith from "../../../public/Assets/bank logo/zenith.jpeg"


export const loanApplication = [
    {
        name: "Yes, I have read through and I agree with the terms and condition and i accept all "
    },
    {
        name: "Yes, I have read through and I agree with the terms and condition  and i accept all "
    },
    {
        name: "Yes, I have read through and I agree with the terms and condition and i accept all "
    },
    {
        name: "Yes, I have read through and I agree with the terms and condition and i accept all "
    },
    {
        name: "Yes, I have read through and I agree with the terms and condition  and i accept all "
    },
    {
        name: "Yes, I have read through and I agree with the terms and condition  and i accept all "
    },
    {
        name: "Yes, I have read through and I agree with the terms and condition  and i accept all "
    },
    {
        name: "Yes, I have read through and I agree with the terms and condition  and i accept all "
    },
    {
        name: "Yes, I have read through and I agree with the terms and condition  and i accept all "
    },

];

export const BorrowersTransactionHeaders = [
    { id: 10001, label: "Loan" },
    { id: 10002, label: "Date Applied" },
    { id: 10009, label: "Loan Amount" },
    { id: 10005, label: "Loan Status" },
    { id: 10003, label: "Approved Amount" },
    { id: 10004, label: "Due Date" },
    { id: 10006, label: "Re-payment Status" },
    { id: 10007, label: "View Transactions" },
];

export const loanTableData = [
    {
        id: "5000",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: "50000",
        approvedAmount: "30000",
        approved: "27/09/2023",
        due: "Dec. 23, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },

    {
        id: "5001",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: "980000",
        approvedAmount: "580000",
        approved: "27/09/2022",
        due: "Jan. 12, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },

    {
        id: "5002",
        product: "E-commerce Loan",
        applied: "27/09/2022",
        loanAmount: "45000",
        approvedAmount: "25000",
        approved: "27/09/2022",
        due: "May 15, 2021",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    },

    {
        id: "5003",
        product: "Housing Loan",
        applied: "27/09/2022",
        loanAmount: "230000",
        approvedAmount: "150000",
        approved: "27/09/2022",
        due: "Nov. 30, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },

    {
        id: "5004",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: "530000",
        approvedAmount: "300000",
        approved: "27/09/2022",
        due: "Jul. 4, 2022",
        status: "approved",
        repaymentStatusRepay: "repay-loan",
    },

    {
        id: "5005",
        product: "Agriculture Loan",
        applied: "27/09/2022",
        loanAmount: "450000",
        approvedAmount: "320000",
        approved: "27/09/2022",
        due: "Dec. 23, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },


    {
        id: "5006",
        product: "Housing Loan",
        applied: "20/09/2022",
        loanAmount: "100000",
        approvedAmount: "80000",
        approved: "19/03/2022",
        due: "Oct. 11, 2022",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    },

    {
        id: "5007",
        product: "E-commerce Loan",
        applied: "27/09/2022",
        loanAmount: "45000",
        approvedAmount: "25000",
        approved: "27/09/2022",
        due: "May 15, 2021",
        status: "pending",
        repaymentStatusRepay: "pending"

    },

    {
        id: "5008",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: "980000",
        approvedAmount: "580000",
        approved: "27/09/2022",
        due: "Jan. 12, 2022",
        status: "approved",
        repaymentStatusRepay: "repay-Loan"
    },

    {
        id: "5009",
        product: "Academic Loan",
        applied: "04/09/2022",
        loanAmount: "600000",
        approvedAmount: "450000",
        approved: "14/09/2022",
        due: "Apr. 08, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },

    {
        id: "5010",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: "530000",
        approvedAmount: "300000",
        approved: "27/09/2022",
        due: "Jul. 4, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },

    {
        id: "5011",
        product: "Agriculture Loan",
        applied: "27/09/2022",
        loanAmount: "450000",
        approvedAmount: "320000",
        approved: "27/09/2022",
        due: "Dec. 23, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },
    {
        id: "5012",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: "980000",
        approvedAmount: "580000",
        approved: "27/09/2022",
        due: "Jan. 12, 2022",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    },

    {
        id: "5013",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: "50000",
        approvedAmount: "30000",
        approved: "27/09/2022",
        due: "Dec. 23, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },

    {
        id: "5014",
        product: "Agriculture Loan",
        applied: "27/09/2022",
        loanAmount: "450000",
        approvedAmount: "320000",
        approved: "27/09/2022",
        due: "Dec. 23, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },

    {
        id: "5015",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: "230000",
        approvedAmount: "150000",
        approved: "27/09/2022",
        due: "Nov. 30, 2022",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    },

    {
        id: "5016",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: "530000",
        approvedAmount: "300000",
        approved: "27/09/2022",
        due: "Jul. 4, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },

    {
        id: "5017",
        product: "Health Loan",
        applied: "10/09/2022",
        loanAmount: "390000",
        approvedAmount: "200000",
        approved: "22/09/2022",
        due: "Dec. 23, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },
    {
        id: "5018",
        product: "E-commerce Loan",
        applied: "27/09/2022",
        loanAmount: "45000",
        approvedAmount: "25000",
        approved: "27/09/2022",
        due: "May 15, 2021",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    },

    {
        id: "5019",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: "980000",
        approvedAmount: "580000",
        approved: "27/09/2022",
        due: "Jan. 12, 2022",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    },

    {
        id: "5020",
        product: "Academic Loan",
        applied: "27/09/2022",
        loanAmount: "45000",
        approvedAmount: "25000",
        approved: "27/09/2022",
        due: "May 15, 2021",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    },

    {
        id: "5021",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: "230000",
        approvedAmount: "150000",
        approved: "27/09/2022",
        due: "Nov. 30, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },

    {
        id: "5022",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: "530000",
        approvedAmount: "300000",
        approved: "27/09/2022",
        due: "Jul. 4, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },

    {
        id: "5023",
        product: "Agriculture Loan",
        applied: "27/09/2022",
        loanAmount: "450000",
        approvedAmount: "320000",
        approved: "27/09/2022",
        due: "Dec. 23, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },
    {
        id: "5024",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: "50000",
        approvedAmount: "30000",
        approved: "27/09/2022",
        due: "Dec. 23, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },

    {
        id: "5025",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: "980000",
        approvedAmount: "580000",
        approved: "27/09/2022",
        due: "Jan. 12, 2022",
        status: "approved",
        repaymentStatusRepay: "paid-loan"
    },

    {
        id: "5026",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: "50000",
        approvedAmount: "30000",
        approved: "27/09/2022",
        due: "Dec. 23, 2022",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    },

    {
        id: "5027",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: "230000",
        approvedAmount: "150000",
        approved: "27/09/2022",
        due: "Nov. 30, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },

    {
        id: "5028",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: "530000",
        approvedAmount: "300000",
        approved: "27/09/2022",
        due: "Jul. 4, 2022",
        status: "pending",
        repaymentStatusRepay: "pending"
    },

    {
        id: "5029",
        product: "Health Loan",
        applied: "07/11/2022",
        loanAmount: "900000",
        approvedAmount: "320000",
        approved: "19/11/2022",
        due: "Feb. 20, 2023",
        status: "approved",
        repaymentStatusRepay: "repay-loan"
    }
]

export const BankQueryList = [
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Access Bank",
        slug: "access-bank",
        code: "044",
        ussd: "*901#",
        image: access,
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0452038344",
        BankName: "Access Bank (Diamond)",
        slug: "access-bank-diamond",
        code: "063",
        ussd: "*426#",
        image: diamond
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "ALAT by WEMA",
        slug: "alat-by-wema",
        code: "035A",
        ussd: "*945*100#",
        image: wema
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "ASO Savings and Loans",
        slug: "asosavings",
        code: "401",
        ussd: "",
        image: aso
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Bowen Microfinance Bank",
        slug: "bowen-microfinance-bank",
        code: "50931",
        ussd: "",
        image: bowen
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "CEMCS Microfinance Bank",
        slug: "cemcs-microfinance-bank",
        code: "50823",
        ussd: "",
        image: chevron
    },
    {
        AccountName: "Andrew Sampson",
        AccountNumber: "0192038394",
        BankName: "Citibank Nigeria",
        slug: "citibank-nigeria",
        code: "023",
        ussd: "",
        image: citi
    },
    {
        AccountName: "Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Ecobank Nigeria",
        slug: "ecobank-nigeria",
        code: "050",
        ussd: "*326#",
        image: eco
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Ekondo Microfinance Bank",
        slug: "ekondo-microfinance-bank",
        code: "562",
        ussd: "*540*178#",
        image: ekondo
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "Fidelity Bank",
        slug: "fidelity-bank",
        code: "070",
        ussd: "*770#",
        image: fidelity
    },
    {
        AccountName: "Andrew Sampson",
        AccountNumber: "0192038394",
        BankName: "First Bank of Nigeria",
        slug: "first-bank-of-nigeria",
        code: "011",
        ussd: "*894#",
        image: firstbank
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "First City Monument Bank",
        slug: "first-city-monument-bank",
        code: "214",
        ussd: "*329#",
        image: fcmb
    },
    {
        AccountName: "Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Globus Bank",
        slug: "globus-bank",
        code: "00103",
        ussd: "*989#",
        image: globus
    },
    {
        AccountName: "Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Guaranty Trust Bank",
        slug: "guaranty-trust-bank",
        code: "058",
        ussd: "*737#",
        image: gtb
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Hasal Microfinance Bank",
        slug: "hasal-microfinance-bank",
        code: "50383",
        ussd: "*322*127#",
        image: hasal
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Heritage Bank",
        slug: "heritage-bank",
        code: "030",
        ussd: "*322#",
        image: heritage
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Jaiz Bank",
        slug: "jaiz-bank",
        code: "301",
        ussd: "*389*301#",
        image: jaiz
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Keystone Bank",
        slug: "keystone-bank",
        code: "082",
        ussd: "*7111#",
        image: keystone
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "Kuda Bank",
        slug: "kuda-bank",
        code: "50211",
        ussd: "",
        image: kuda
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "One Finance",
        slug: "one-finance",
        code: "565",
        ussd: "*1303#",
        image: one
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "Paga",
        slug: "paga",
        code: "327",
        ussd: "",
        image: paga
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "Parallex Bank",
        slug: "parallex-bank",
        code: "526",
        ussd: "*322*318*0#",
        image: parallex
    },
    {
        AccountName: "Andrew Sampson",
        AccountNumber: "0192038394",
        BankName: "PayCom",
        slug: "paycom",
        code: "100004",
        ussd: "*955#",
        image: paycom
    },
    {
        AccountName: "Andrew Sampson",
        AccountNumber: "0192038394",
        BankName: "Polaris Bank",
        slug: "polaris-bank",
        code: "076",
        ussd: "*833#",
        image: polaris
    },
    {
        AccountName: "Andrew Sampson",
        AccountNumber: "0192038394",
        BankName: "Providus Bank",
        slug: "providus-bank",
        code: "101",
        ussd: "",
        image: providus
    },
    {
        AccountName: "Andrew Sampson",
        AccountNumber: "0192038394",
        BankName: "Rubies MFB",
        slug: "rubies-mfb",
        code: "125",
        ussd: "*7797#",
        image: rubies
    },
    {
        AccountName:" Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Sparkle Microfinance Bank",
        slug: "sparkle-microfinance-bank",
        code: "51310",
        ussd: "",
        image: cf
    },
    {
        AccountName:"Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Stanbic IBTC Bank",
        slug: "stanbic-ibtc-bank",
        code: "221",
        ussd: "*909#",
        image: standard
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "Standard Chartered Bank",
        slug: "standard-chartered-bank",
        code: "068",
        ussd: "",
        image: scb
    },
    {
        AccountName: "Andrew Amadi",
        AccountNumber: "0192038394",
        BankName: "Sterling Bank",
        slug: "sterling-bank",
        code: "232",
        ussd: "*822#",
        image: starling
    },
    {
        AccountName: "Kelly Blaq",
        AccountNumber: "0192038394",
        BankName: "Suntrust Bank",
        slug: "suntrust-bank",
        code: "100",
        ussd: "*5230#",
        image: suntrust
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "TAJ Bank",
        slug: "taj-bank",
        code: "302",
        ussd: "*898#",
        image: taj
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "TCF MFB",
        slug: "tcf-mfb",
        code: "51211",
        ussd: "*908#",
        image: trust
    },
    {
        AccountName: "Kelly Blaq",
        AccountNumber: "0192038394",
        BankName: "Titan Trust Bank",
        slug: "titan-trust-bank",
        code: "102",
        ussd: "*922#",
        image: trust
    },
    {     AccountName: "Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Union Bank of Nigeria",
        slug: "union-bank-of-nigeria",
        code: "032",
        ussd: "*826#",
        image: union
    },
    {     AccountName: "Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "United Bank For Africa",
        slug: "united-bank-for-africa",
        code: "033",
        ussd: "*919#",
        image: uba
    },
    {
          AccountName: "Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Unity Bank",
        slug: "unity-bank",
        code: "215",
        ussd: "*7799#",
        image: unity
    },
    {
          AccountName: "Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "VFD",
        slug: "vfd",
        code: "566",
        ussd: "",
        image: vfd
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Wema Bank",
        slug: "wema-bank",
        code: "035",
        ussd: "*945#",
        image: wema
    },
    {
        AccountName: "Andrew Amadi Sampson",
        AccountNumber: "0192038394",
        BankName: "Zenith Bank",
        slug: "zenith-bank",
        code: "057",
        ussd: "*966#",
        image: zenith
    }
]

export const productDescriptionTitle = [
    {
        col1: "Loan Details",
        col2: "Date",
        col3: "Amount",
        col4: "Status"
    }
]

export const productDescription = [
    {
        loanPrdtDesc: "Date Applied",
        date: "23/03/2022",
        amount: 425000,

    },
    {
        loanPrdtDesc: "Date Approved",
        date: "25/03/2022",
        status: "Approved",
        amount: 45000,
    },

    {
        loanPrdtDesc: "Repayment Due Date",
        date: "29/03/2022",
        amount: 45000,
    },

    {
        loanPrdtDesc: "Paid Amount",
        date: "27/03/2022",
        status: "paid",
        amount: 25000,
    },
    {
        loanPrdtDesc: "un-Paid Amount",
        date: "27/03/2022",
        status: "repay loan",
        amount: 20000,
    },



]

// Investors Data
export const transactionTableHeaders = [
    { id: 10001, label: "S/N" },
    { id: 10002, label: "Full Name" },
    { id: 10009, label: "Loan Type" },
    { id: 10003, label: "Loan Amount" },
    { id: 10004, label: "Date Applied" },
    { id: 10005, label: "Loan Status" },
    { id: 10008, label: "Approved Amount" },
];


export const transactionTableData = [
    {
        id: 5001,
        name:"Sandra Igwe johnson",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: 560000,
        status: "pending",
    },

    {
        id: 5002,
        name:"Sandra Igwe johnson",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: 980000,
        status: "approved",
        approvedAmount:49000
    },

    {
        id: 5003,
        name:"Sandra Igwe johnson",
        product: "E-commerce Loan",
        applied: "27/09/2022",
        loanAmount: 459000,
        status: "pending",
    },

    {
        id: 5004,
        name:"Sandra Igwe johnson",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: 230000,
        status: "approved",
        approvedAmount:494000
    },

    {id:5005,
        name:"Sandra Igwe johnson",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: 530000,
        status: "pending",
    },

    {id:5006,
        name:"peter Igwe johnson",
        product: "Agriculture Loan",
        applied: "27/09/2022",
        loanAmount: 450000,
        status: "approved",
        approvedAmount: 450000
    },


    {
        id: 5007,
        name:" johnson duruchi",
        product: "Housing Loan",
        applied: "20/09/2022",
        loanAmount: 100000,
        status: "approved",
        approvedAmount:349000
    },

    {
        id: 5008,
        name:"Sandra Igwe johnson",
        product: "E-commerce Loan",
        applied: "27/09/2022",
        loanAmount: 453000,
        status: "pending",
    },

    {id:5009,
        name:"favour Igwe johnson",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: 980000,
        status: "approved",
        approvedAmount:569000
    },

    {id:5010,
        name:"amstrong Igwe johnson",
        product: "Academic Loan",
        applied: "04/09/2022",
        loanAmount: 600000,
        status: "Rejected",
    },

    {
        id: 5011,
        name:"jayson peter johnson",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: 530000,
        status: "pending",
    },

    {
        id: 5012,
        name:"Sandra Igwe johnson",
        product: "Agriculture Loan",
        applied: "27/09/2022",
        loanAmount: 450000,
        status: "pending",
    },
    {
        id: 5013,
        name:"Sandra Igwe benard",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: 980000,
        status: "approved",
        approvedAmount:719000
    },

    {
        id: 5014,
        name:"Sandra Igwe johnson",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: 50000,
        status: "pending",
    },

    {
        id: 5015,
        name:"Sandra Okafor johnson",
        product: "Agriculture Loan",
        applied: "27/09/2022",
        loanAmount: 450000,
        status: "approved",
        approvedAmount: 420000
    },

    {
        id: 5016,
        name:"bethel Igwe johnson",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: 230000,
        status: "pending",
    },

    {
        id: 5017,
        name:"Andrew Igwe johnson",
        product: "Morgage Loan",
        applied: "27/09/2022",
        loanAmount: 530000,
        status: "pending",
    },

    {
        id: 5018,
        name:"John Igwe johnson",
        product: "Health Loan",
        applied: "10/09/2022",
        loanAmount: 390000,
        status: "approved",
        approvedAmount: 22000
    },
    {
        id: 5019,
        name:"Steven Igwe johnson",
        product: "E-commerce Loan",
        applied: "27/09/2022",
        loanAmount: 45000,
        status: "pending",
    },

    {
        id: 5020,
        name:"Sandra Igwe johnson",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: 980000,
        status: "approved",
        approvedAmount: 420000
    },

    {
        id: 5021,
        name:"Sandra Igwe johnson",
        product: "Academic Loan",
        applied: "27/09/2022",
        loanAmount: 45000,
        status: "approved",
    },

    {
        id: 5022,
        name:"Sandra Igwe johnson",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: 230000,
        approvedAmount: "150000",
        status: "approved",
    },

    {
        id: 5023,
        name:"Sandra Igwe johnson",
        product: "Morgage Loan",
        applied: "27/09/2022",
        loanAmount: 530000,
        status: "pending",
    },

    {
        id: 5024,
        name:"Sandra Igwe johnson",
        product: "Agriculture Loan",
        applied: "27/09/2022",
        loanAmount: 450000,
        status: "approved",
        approvedAmount: 40000

    },
    {
        id: 5025,
        name:"Sandra Igwe johnson",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: 50000,
        status: "pending",
    },

    {
        id: 5026,
        name:"Sandra Igwe johnson",
        product: "Auto Loan",
        applied: "27/09/2022",
        loanAmount: 980000,
        status: "approved",
        approvedAmount: 420000
    },

    {
        id: 5027,
        name:"kelvin amadi john",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: 520000,
        status: "approved",
        approvedAmount: 420000
    },

    {
        id: 5028,
        name:"Sandra Amadi okoro",
        product: "Business Loan",
        applied: "27/09/2022",
        loanAmount: 230000,
        status: "approved",
        approvedAmount: 120000
    },

    {
        id: 5029,
        name:"Igwe johnson",
        product: "Mortgage Loan",
        applied: "27/09/2022",
        loanAmount: 530000,
        status: "pending",
    },

    {
        id: 5030,
        name:"Sandra Igwe johnson",
        product: "Health Loan",
        applied: "07/11/2022",
        loanAmount: 900000,
        status: "pending",
    }
]