import React from 'react'

type CircleProps ={
  classname?: string,
  children?: React.ReactNode;
}


export const Circle = ({classname, children}:CircleProps) => {
  return (
    <div className={classname}>
      {children}
    </div>
  )
}

// import router, { useRouter } from 'next/router';
// import React, { FormEvent, useState } from 'react'
// import { TermsForm } from '../Forms/KycForm/TermsForm';
// import { useMultistepForm } from '../Forms/KycForm/useMultistepForm';
// import { UserForm } from '../Forms/KycForm/UserForm';
// import { VerifyForm } from '../Forms/KycForm/VerifyForm';

// type CircleProps ={
//   classname?: string,
//   children?: React.ReactNode;
// }

// type FormData = {
//   phone: string
//   nin: string
//   bvn: string
//   email: string
//   password: string
// }

// const INITIAL_DATA: FormData = {
//   phone: "",
//   bvn: "",
//   nin: "",
//   email: "",
//   password: "",
// }

// export const Circle = ({classname, children}:CircleProps) => {
//   const router = useRouter()
//   const [data, setData] = useState(INITIAL_DATA)
  
//   function updateFields(fields: Partial<FormData>) {
//       setData(prev => {
//           return { ...prev, ...fields }
//       })
//   }
  
//   const {Circlebar, steps, currentStepIndex, step, isFirstStep, isLastStep, back, next } =
//       useMultistepForm([
//           <UserForm {...data} updateFields={updateFields} />,
//           <VerifyForm  {...data} updateFields={updateFields} />,
//           <TermsForm {...data} updateFields={updateFields} />,
//       ])


//   const onSubmit=(e: FormEvent)=> {
//       e.preventDefault()
//       if (!isLastStep) return next()
      
            
//       router.push('/borrower/banksQuery')
//   }

//   function setActive(arg0: number) {
//     throw new Error('Function not implemented.');
//   }

//   return (
//     <div className={classname}>
//             <div className="style-form">
//                         <form onSubmit={onSubmit}>
//                             {/* <div className="progress-bar">
//                              <Circle >
//                              {currentStepIndex + 1} / {steps.length} 
//                              </Circle>
                    
//                             </div> */}
//                             {step}
                           
//                     </form>
//             </div>
//     </div>
//   )

// }