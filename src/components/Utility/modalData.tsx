export enum ModalType{
    REPAY_LOAN_MODAL = "REPAY_LOAN_MODAL",
    UPDATE_ACCT_MODAL = "UPDATE_ACCT_MODAL",
    NONE = "NONE",
    APPROVE_LOAN_AMOUNT = "APPROVE_LOAN_AMOUNT"
}



export const repayLoanModalData = [
    {
        name: "amount",
        label: "Amount",
        required: true,
        dataType: "number",
        htmlElement: "input",
        placeholder:"amount",
        defaultValue: "30,000"
    }
];

export const updateAcctModalData = [
    {
        name: "loanProduct",
        label: "Loan Product",
        required: true,
        dataType: "text",
        placeholder:"amount",
        htmlElement: "number",
        defaultValue: "Business Loan"
    },
    {
        name: "loanAmount",
        label: "Loan Amount",
        required: true,
        dataType: "number",
        placeholder:"amount",
        htmlElement: "number",
        defaultValue: "00.0"
    }    
];

export const loanTermsFormData = [
    {
        name: "loanProduct",
        label: "Loan Type",
        required: true,
        dataType: "text",
        placeholder: "Business Loan"
    },
    {
        name: "loanAmount",
        label: "Loan Amount",
        required: true,
        dataType: "number",
        placeholder: "00.0"
    }    
];

export const shippingFormData = [
    {
        name: "receiversName",
        label: "Full Name",
        required: true,
        dataType: "text",
        placeHolder: "Bisi Chuks Idris",
    },
    {
        name: "email",
        label: "Email Address",
        required: true,
        dataType: "number",
        placeHolder: "example@email.com",
    },
    {
        name: "phoneNumber",
        label: "Phone Number",
        required: true,
        dataType: "number",
    },
    {
        name: "billingAddress",
        label: "Billing Address",
        required: true,
        dataType: "text",
        placeHolder: "No. 4 Nonso Oloye Str., Gwarimpa - Abuja",
        multiple: "50"
    },
    {
        name: "postalCode",
        label: "Postal Code",
        required: true,
        dataType: "number",
        placeHolder: "123456",
    }

];

export const loanProductModalDetail = [
    {
        loanType: "Loan Type:",
        loanProduct: "Business Loan",
    }
]

export const approveLoanAmount =[
    {
        id:5000,
        FullName: "Sandra Igwe johnson",
        product:"Business Loan",
        loanAmount: 459000,
        label: "Loan Approval Amount",
        name:"amount",
        required: true,
        dataType: "number",
        htmlElement: "input",
        placeholder:"approval Amount",
        defaultValue: "500,000"
    }
]