import React, {useEffect, useRef, useState} from "react"
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';


const Stepper =({ steps, currentStep}:any)=>{
    const [newStep, setNewStep] = useState<any[]>([])
    const stepRef = useRef();

    const updateStep = (stepNumber: any, steps: any) => {
    const newSteps = [...steps]
    let count = 0;

    while (count < newSteps.length) {
        //current step
        if (count === stepNumber) {
            newSteps[count] = {
                ...newSteps[count],
                highlighted: true,
                selected: true,
                completed: true,
            }
            count ++
        }
        //    step completed
        else if (count < stepNumber) {
            newSteps[count] = {
                ...newSteps[count],
                highlighted: false,
                selected: true,
                completed: true,
            }
            count ++
        }
        //    Step pending
        else {
            newSteps[count] = {
                ...newSteps[count],
                highlighted: false,
                selected: false,
                completed: false,
            }
            count ++
        }
    }
    return newSteps

}
    useEffect(()=>{
    //    create Object
    //     stepRef.current = steps.map((step:any, index: any) =>
        stepRef.current = steps.map((step: any, index: any) =>
            Object.assign({}, {
                description: step,
                completed: false,
                highlighted: index === 0 ? [true] : [false],
                selected: index === 0 ? [true] : [false],
            })
        )
        const current = updateStep(currentStep -1, stepRef.current);
        setNewStep(current)
    },[steps, currentStep]);


    const displaySteps = newStep.map((step, index) => {
     return (
    <>
        <div key={index} className={index !== newStep.length -1
            ? "w-full flex  align-items-center"
            : "flex  align-items-center"}>

            <div className="relative flex flex-column  align-items-center text-blue-800">
                <div className={`border-circle border-1 border-gray-700 w-4rem h-4rem flex  align-items-center justify-content-center py-3
                ${step.selected ? "bg-blue-500 text-white font-bold border-green-700":""}`}>
                    {/* Display Number*/}
                    {step.completed ? (<span className="text-white font-bold text-xl">&#10003;</span>) : (index + 1)}
                </div>

                <div className={`absolute top-0 text-center mt-8 w-32 text-xs font-medium ${step.highlighted ? "text-blue-500 font-bold" : "text-gray-500"}`}>
                    {/* Display Description*/}
                    {step.description}
                </div>
            </div>
            <div className={`flex-auto border-top-2 transition-all animation-duration-500 transition-ease-in-out
              ${step.completed ? "border-blue-500" : "border-gray-400"}`}
            >
                {/* Display Line*/}
            </div>

        </div>

    </>
    )
    })

    return(
        <div className="mx-4 p-4 flex justify-content-between align-items-center text-purple-800">
            {displaySteps}
        </div>
    )
}


export default Stepper