export const sliderData = [
    {
        image: "https://i.ibb.co/b7YQNmx/banner5.jpg",
        heading: "Shoes Villa",
        desc: "Up to 30% off on all on sale products.",
    },
    {
        image: "https://i.ibb.co/LvDhrhk/banner1.jpg",
        heading: "Shoes Villa",
        desc: "Up to 30% off on all on sale products.",
    },
    {
        image: "https://i.ibb.co/GHwPRPn/banner4.jpg",
        heading: "Women Fashion",
        desc: "Up to 30% off on all on sale products.",
    },
    {
        image: "https://i.ibb.co/dpQYqFx/banner6.jpg",
        heading: "Men Fashion",
        desc: "Up to 30% off on all on sale products.",
    },
    {
        image: "https://i.ibb.co/GTxv4CB/banner7.jpg",
        heading: "Awesome Gadgets",
        desc: "Up to 30% off on all on sale products.",
    },
    {
        image: "https://i.ibb.co/w7RBDtF/banner3.jpg",
        heading: "Awesome Gadgets",
        desc: "Up to 30% off on all on sale products.",
    },
];


