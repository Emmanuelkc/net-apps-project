const serviceData = [
    {
        icon: "pi pi-truck ",
        title: "Free Shipping",
        subtitle: "Lorem ipsum dolor sit amet.",
        bg: "#fdefe6",
    },
    {
        icon: "pi pi-shopping-bag",
        title: "Easy Returns",
        subtitle: "Lorem ipsum dolor sit amet.",
        bg: "#ceebe9",
    },
    {
        icon: "pi pi-credit-card",
        title: "Secure Payment",
        subtitle: "Lorem ipsum dolor sit amet.",
        bg: "#e2f2b2",
    },
    {
        icon: "pi pi-bitcoin",
        title: " Back Guarantee",
        subtitle: "Lorem ipsum dolor sit amet.",
        bg: "#d6e5fb",
    },
];

export default serviceData;