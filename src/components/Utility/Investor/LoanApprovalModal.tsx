import React from "react";
import {approveLoanAmount, ModalType} from "../modalData";
import {InputDataProps} from "../types"
import {TransactionModalWrap} from "../../../../styles/InvestorStyles/dashboard"
import {NumericFormat} from "react-number-format";


const LoanApprovalModal = ({setApprovalModal, modalData}:{setApprovalModal :any, modalData: InputDataProps[]}) => {

    const onSubmit = (e: any) => {
        e.preventDefault();
    }

    return (
        <TransactionModalWrap>
            <div>
                <div className="modal" onSubmit={onSubmit}>
                    {modalData.map(InputData => {
                        return(
                            <div key={InputData.label}>
                                {
                                    ( <div>
                                            {approveLoanAmount.map((InputData:any) =>{
                                                return(
                                                    <div key={InputData.id} className="loan-modal-description">
                                                        <p className="loan-type">
                                                            <span className="text-blue-700">Full-Name: </span>
                                                            {InputData.FullName}
                                                        </p>

                                                        <p className="loan-type">
                                                            <span className="text-blue-700">Loan-Amount: </span>
                                                            <NumericFormat
                                                                value={InputData.loanAmount}
                                                                thousandsGroupStyle="lakh"
                                                                thousandSeparator=","
                                                                displayType="text"
                                                                renderText={(value) => <b>{value}</b>}
                                                            />
                                                            </p>
                                                    </div>
                                                )
                                            })}
                                         <div className="mt-6">
                                             <label>{InputData.label}</label>
                                             <input type={InputData.htmlElement}
                                                    name={InputData.name}
                                                    required={InputData.required}
                                                    datatype={InputData.dataType}
                                                    placeholder={InputData.placeholder}
                                             />
                                         </div>
                                        </div>
                                    )
                                }
                            </div>
                        )
                    })
                    }
                    <div className='button'>
                        <button className="cancel" onClick={()=>setApprovalModal({open: false, modalType: ModalType.NONE})}>Cancel</button>
                        <button className="submit" onClick={()=>setApprovalModal({open: false, modalType: ModalType.NONE})}>Approve</button>
                    </div>
                </div>
            </div>
        </TransactionModalWrap>
    )
}

export default LoanApprovalModal