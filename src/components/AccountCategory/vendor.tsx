import React from "react";
import "primeicons/primeicons.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import bg from "../../../public/Assets/L3.png";

import {
  ButtonsContainer,
  Container,
} from "../../../styles/AccountCategory/loanAccountCategoryStyle";
import Image from "next/image";
import router from "next/router";
import { Button } from "primereact/button";

const VendorsSection = () => {
  const vendorsKYC = (e: any) => {
    e.preventDefault();
    router.push("/vendor");
  };

  return (
    <Container className="grid grid-nogutter  text-800 p-7">
      <div className="col-12 md:col-6 p-4  text-center md:text-left flex align-items-center  ">
        <section>
          <h2 className="block text-4xl font-bold mb-3">Become a vendor</h2>
          <h2 className="text-2xl  font-bold mb-3">
            Sell product on credit using Netapps Market Place
          </h2>
          <p className="mt-0 mb-4 text-700 line-height-4">
            Customers buy more when you make it easy for them to get the things
            they need the most when you provide them with instalment payment
            plans. Take advantage of the Netapps E-commerce service and leverage
            on increased sales and repeat customers.
          </p>

          <ButtonsContainer>
            <a className="Button" onClick={vendorsKYC}>
              Sell Product On Credit
            </a>
            {/*<Button>Contact Us</Button>*/}
          </ButtonsContainer>
        </section>
      </div>

      <div className="col-12 md:col-6  p-4  overflow-hidden  flex flex-wrap align-items-center Container ">
        <Image
          src={bg}
          alt="hero-1"
          width={470}
          height={650}
          className="md:ml-auto block md:h-full"
        />
      </div>
    </Container>
  );
};

export default VendorsSection;