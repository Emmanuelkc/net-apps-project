import React from 'react'
import CountUp from 'react-countup';
import {LoanProductAccountWrap} from "../../../styles/AccountCategory/loanAccountCategoryStyle"

export const LoanProductCount = () => {
    return (
    <LoanProductAccountWrap>
        <div className='pg-title m-4'>
            <h1>Extend Boundaries with our Amazing Financial Loans</h1>
            <p className="text-blue-800">We are everything you need to achieve your financial breakthrough  💨</p>
        </div>
        <div className='grid'>
            <div className='loan-percentage col-12 md:col-3 mt-4'>
            <CountUp start={0} end={8} delay={1}>
                {({ countUpRef }) => (
                    <div>
                    <span className="count" ref={countUpRef}/>
                        <span className="count">+</span>
                        <p className="mt-4 text-blue-800">Loan Product</p>
                    </div>
                )}
            </CountUp>

        </div>

        <div className='loan-percentage col-12 md:col-3 mt-4' >
            <CountUp start={0} end={50} delay={1}>
                {({ countUpRef }) => (
                    <div>
                        <span className="count" ref={countUpRef}/>
                        <span className="count">+</span>
                        <p className="mt-4 text-blue-800">Loan Partners & Investors</p>
                    </div>
                )}
            </CountUp>
        </div>
        <div className='loan-percentage col-12 md:col-3 mt-4'>
            <CountUp start={0} end={10} delay={1}>
                {({ countUpRef }) => (
                    <div>
                        <span className="count" ref={countUpRef}/>
                        <span className="count">M+</span>
                        <p className="mt-4 text-blue-800">Credit Limit for Business</p>

                    </div>
                )}
            </CountUp>
        </div>
        <div className='loan-percentage col-12 md:col-3 mt-4'>
            <CountUp start={0} end={5} delay={1}>
                {({ countUpRef }) => (
                    <div>
                        <span className="count" ref={countUpRef}/>
                        <span className="count">M+</span>
                        <p className="mt-4 text-blue-800">Credit Limit for Individuals</p>

                    </div>
                )}
            </CountUp>
        </div>
        </div>
    </LoanProductAccountWrap>
  )
}
