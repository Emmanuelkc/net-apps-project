import React,{FC} from "react"
import 'primeflex/primeflex.css';
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css";
import {FcLibrary, FcShop, FcVip} from "react-icons/fc";
import router from "next/router";

const LoanAccountCategory:FC =()=>{
    const handleClick =(e:any)=>{
        e.preventDefault()
        router.push('/borrowers')
    }

    return(
        <>
            <div className=" m-4 p-3 surface-0 text-center border-round-md " style={{boxShadow: "rgba(0, 0, 0, 0.16) 0px 10px 36px 0px, rgba(0, 0, 0, 0.06) 0px 0px 0px 1px"}}>
                <div className="mb-5 font-bold text-2xl">
                    <h2 className='max-w-lg p-4  mt-2 font-sans text-3xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl md:mx-auto'>
                        Net Apps Quick Loans For Business
                    </h2>
                </div>

                <div className="text-700 text-sm mb-6">Ac turpis egestas maecenas pharetra convallis posuere morbi leo urna.</div>

                <div className="grid">
                    <div className="col-12 md:col-4 mb-4 px-5 cursor-pointer" onClick={handleClick}>
                    <span className="p-5 border-circle shadow-2 mb-3 inline-block bg-blue-400 ">
                            <FcLibrary className="w-12 h-12 text-6xl text-gray-900 sm:w-15 sm:h-15" />
                    </span>
                        <div className="mb-3 font-bold leading-5 text-black text-2xl">Get a Loan</div>
                        <p className="max-w-md text-md text-gray-900 sm:mx-auto">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <a href="/" aria-label=""
                            className="inline-flex items-center font-semibold transition-colors duration-200 text-blue-800 hover:text-purple-800">
                            Learn more</a>
                    </div>
                    <div className="col-12 md:col-4 mb-4 px-5 cursor-pointer" onClick={handleClick}>
                    <span  className="p-5 border-circle shadow-2 mb-3 inline-block bg-blue-400">
                          <FcShop className="w-12 h-12 text-6xl text-gray-900 sm:w-15 sm:h-15" />
                    </span>
                        <div className="mb-3 font-bold leading-5 text-black text-2xl">Vendor's</div>
                        <p className="max-w-md text-md text-gray-900 sm:mx-auto">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <a href="/" aria-label=""
                           className="inline-flex items-center font-semibold transition-colors duration-200 text-blue-800 hover:text-purple-800">
                            Learn more</a>
                    </div>
                    <div className="col-12 md:col-4 mb-4 px-5 cursor-pointer" onClick={handleClick}>
                    <span className="p-5 border-circle shadow-2 mb-3 inline-block bg-blue-400">
                        <FcVip className="w-12 h-12 text-6xl text-gray-900 sm:w-15 sm:h-15" />
                    </span>
                        <div className="mb-3 font-bold leading-5 text-black text-2xl">Investor's</div>
                        <p className="max-w-md text-md text-gray-900 sm:mx-auto">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <a href="/" aria-label=""
                           className="inline-flex items-center font-semibold transition-colors duration-200 text-blue-800 hover:text-purple-800">
                            Learn more</a>
                    </div>
                </div>
            </div>

        </>
    )
}

export default LoanAccountCategory