import React from "react";
import "primeicons/primeicons.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import bg from "../../../public/Assets/L4.png";

import {
  ButtonsContainer,
  Container,
} from "../../../styles/AccountCategory/loanAccountCategoryStyle";
import Image from "next/image";
import router from "next/router";
import { Button } from "primereact/button";

const InvestorsSection = () => {
  // const Investor =(e:any)=>{
  //     e.preventDefault()
  //     router.push('/investors')
  // }

  const InvestorKYC = (e: any) => {
    e.preventDefault();
    router.push("/investors");
  };

  return (
    <Container className="grid grid-nogutter text-800 p-7">
      <div className="col-12 md:col-6 overflow-hidden  flex flex-wrap align-items-center Container ">
        <Image
          src={bg}
          alt="hero-1"
          width={480}
          height={650}
          className="md:ml-auto block md:h-full"
        />
      </div>
      <div className="col-12 md:col-6 p-4  text-center md:text-left flex align-items-center  ">
        <section>
          <h2 className="block  text-4xl font-bold mb-3">Become an investor</h2>
          <h2 className="text-3xl  font-bold mb-3">
            We track your daily expense with better algorithm & data
          </h2>
          <p className="mt-0 mb-4 text-700 line-height-4">
            We have created a tech alternative platform that links corporate
            lenders and registered individuals looking to earn returns with
            verified business and personal borrowers. Our loan platforms
            register investors as loan providers while providing safe and secure
            loans. Now, investors can reach a larger audience and earn more on
            the go.
          </p>

          <ButtonsContainer>
            <Button className="Button" onClick={InvestorKYC}>
              Become an Investor
            </Button>
            {/*<Button>Contact Us</Button>*/}
          </ButtonsContainer>
        </section>
      </div>
    </Container>
  );
};

export default InvestorsSection;
