import React from "react";
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import {Container,} from "../../../styles/AccountCategory/loanAccountCategoryStyle";
import router from "next/router";

const Disclosure = () => {

    const Investor =(e:any)=>{
        e.preventDefault()
        router.push('/termsOfUse')
    }
    return (
        <Container className="grid  text-800 mt-4  ">
            <div className="col-12 lg:pl-8 lg:pr-8 text-center  lg:align-items-center">
                <section className="">
                    <p className=" text-800  text-1xl  m-3 font-italic align-items-center">KEY DISCLOSURES REGARDING YOUR POSSIBLE LOAN OPTIONS</p>
                    <h2 className="text-1xl  mb-3">We track your daily expense  with better algorithm & data</h2>

                    <ul>
                        <li className="TC text-700 line-height-2">The lender options presented range from a minimum repayment term of 12 months and a maximum repayment term of 84 months.</li>
                        <li className=" TC text-700 line-height-4">The lender options presented range from a minimum annual percentage rate (APR) of 3.99% and a maximum annual percentage rate (APR) of 35.99%</li>
                    </ul>

                    <p className="mt-0 mb-3 text-700 line-height-4">
                       we seek to match you to partners that may
                        extend a loan or other services to you. All loan approval decisions and terms are
                        determined by the loan providers at the time of your application with them.
                        There is no guarantee that you will be approved for a loan or that you will qualify for the rates displayed.
                        The offers and rates presented on this website are estimates based on information you submit to us.
                        Your actual rates depend on your credit history, income, loan terms and other factors.
                        Reasonable efforts are made to compile and maintain accurate information.
                        However all loan rates and terms, including APRs, are
                        presented without warranty and are subject to change by the loan providers without notice.
                    </p>
                    <div className="m-6 cursor-pointer TC">
                        <span className=" mb-3 p-2 text-700 line-height-4"  onClick={Investor}>Terms Of Use |</span>
                        <span className=" mb-3  text-700 line-height-4"  onClick={Investor}>Disclosures and Licenses |</span>
                        <span className=" mb-3 p-2 text-700 line-height-4"  onClick={Investor}>Privacy Notice |</span>
                        <span className=" mb-3  text-700 line-height-4"  onClick={Investor}>Contact Us </span>
                    </div>
                </section>

            </div>

        </Container>


    );
};

export default Disclosure