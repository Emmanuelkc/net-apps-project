import React from "react";
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';


import {
    Button,
    ButtonsContainer,
    Container,
} from "../../../styles/AccountCategory/loanAccountCategoryStyle";
import router from "next/router";


const TermsOfUse = () => {

    const Investor =(e:any)=>{
        e.preventDefault()
        router.push('/investors')
    }
    return (
        <Container className="grid grid-nogutter text-800 ">
            <div className="col-12 lg:pl-8 lg:pr-8 text-center text-left  lg:align-items-center  ">
                <div>
                    SUMMARY: This notice describes your rights and responsibilities
                    when using LendingClub Web site, including alternatives and obligations for resolving disputes or violations of the Terms of Use. You should read this agreement in its entirety.
                </div>
                <section className="">
                    <p className=" text-800  text-2xl  m-3 border-bottom-1">Terms of Use</p>
                    <h2 className="text-1xl  mb-3">We track your daily expense  with better algorithm & data</h2>
                    <p className="mt-0 mb-3 text-700 line-height-4">
                        We have created a tech alternative platform that links corporate lenders
                        and registered individuals
                        looking to earn returns with verified business and personal borrowers.
                        Our loan platforms register
                        investors as loan providers while providing safe and secure loans.
                        Now, investors can reach a larger
                        audience and earn more on the go.
                    </p>

                    <ButtonsContainer>
                        <Button  onClick={Investor}>Become an Investor</Button>
                        {/*<Button>Contact Us</Button>*/}
                    </ButtonsContainer>
                </section>

            </div>

        </Container>


    );
};

export default TermsOfUse