import React from "react"
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import {LoanProductAccountWrap, } from "../../../styles/AccountCategory/loanAccountCategoryStyle";
import {BsShieldLockFill} from "react-icons/bs";

const Security_Home = ()=>{

    return(
        <LoanProductAccountWrap>
            <div className=" text-700 text-center">
                <div className="text-blue-600 font-bold mb-3 mt-6"> <BsShieldLockFill className="text-7xl"/> </div>
                <h2 className="text-900 font-bold text-2xl mb-3">Security that doesn’t cut corners</h2>
                <p className="text-900 text-1xl mb-5">Your security is our highest priority. <br/>
                    Netapps uses 256-bit encryption to keep your information safe.</p>
                 <p className="text-900 text-1xl line-height-1">We’re rewriting the rules of traditional banking,
                and we only win when our customers succeed.</p> <br/>
                <p className="text-900 text-1xl line-height-1 "> We’ve helped over 1 million members reach their goals,
                    and we’re just getting started!</p>
            </div>

        </LoanProductAccountWrap>
    )
}

export default Security_Home