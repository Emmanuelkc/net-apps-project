import React from "react";
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import bg from "../../../public/Assets/L1.png"

import {
    ButtonsContainer,
    Container,
} from "../../../styles/AccountCategory/loanAccountCategoryStyle";
import Image from "next/image";
import router from "next/router";
import {Button} from "primereact/button";

const LoanHero = () => {

    const Borrowers =(e:any)=>{
        e.preventDefault()
        router.push('/borrower')
    }
    return (
        <Container className="grid grid-nogutter  text-800 p-7">
            <div className="col-12 md:col-6 p-4  text-center md:text-left flex align-items-center  ">
                <section>
                    <span className="block text-4xl font-bold mb-3">Personal banking Loan</span>
                    <h4 className="text-4xl  font-bold mb-3">From up to $112,5000</h4>
                    <p className="mt-0 mb-4 text-700 line-height-4">
                        Net-apps offers an array of safe, convenient, fast and transparent lending choices for businesses
                        and Individuals. We have designed and implemented a system with over 50 loan providers and lenders.
                        With us, you can identify the best loans and save money by comparing interest rates, among others,
                        available in your chosen loan product category. How does this work?
                    </p>

                    <ButtonsContainer>
                        <a className="Button"  onClick={Borrowers}>Get Loan Now</a>
                    </ButtonsContainer>
                </section>

            </div>
            <div className="col-12 md:col-6 overflow-hidden  flex flex-wrap align-items-center Container ">
                <Image src={bg}
                     alt="hero-1"
                     width={700} height={600}
                     className="md:ml-auto block md:h-full"/>
            </div>
        </Container>


    );
};

export default LoanHero;