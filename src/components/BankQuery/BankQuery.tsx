import React, {FC} from 'react';
import 'primeflex/primeflex.css';
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css";
import {useRouter} from "next/router";
import {BankQueryList} from "../Utility/DataFetch"
import Image from "next/image";
import {GetAllBanks} from "../../../styles/Home/GetAllBanks/getAllBanks";
import netapp from "../../../public/Assets/bank logo/img.png"

const BankQuery:FC = ( ) => {
    const router = useRouter()

    const viewDashboard = (e:any) => {
        e.preventDefault()
        router.push(`/borrowers`)
    }



    return (
        <GetAllBanks>
            <div className="grid m-4 ">
                {BankQueryList.map((bank:any)=>{
                    return(
                        <div key={bank.bankName} className="col-12 md:col-6 lg:col-4 relative">
                            <div className="card" onClick={viewDashboard}>
                                <div className="grid  flex justify-content-between">
                                    <Image className="col-6" src={bank.image} alt={bank.image} width={53} height={50}  objectFit="contain"/>
                                    <Image src={netapp} alt="netapp" className="text-blue-800 text-5xl"  width={20} height={30} objectFit="contain"/>
                                </div>
                                <h5 className="text-xs">Account-Name:<span className="text-blue-800 text-sm ml-4">{bank.AccountName}</span></h5>
                                <h5 className="text-xs">Bank-Name:<span className="text-blue-800 text-sm ml-6 sm:text-xs">{bank.BankName}</span></h5>
                                <h5 className="text-xs ">Account-Number:<span className="text-blue-800 text-sm ml-3">{bank.AccountNumber}</span></h5>

                            </div>
                        </div>
                    )
                })}

            </div>
        </GetAllBanks>
    );
}

export default BankQuery;