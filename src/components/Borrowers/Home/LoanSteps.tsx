import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import {LoanStepsWrapper} from "../../../../styles/BorrowerDashboard/LoanCategory/loanCategory";
import {
    FcGenealogy,
    FcOnlineSupport,
    FcSalesPerformance,
    FcSurvey,
} from "react-icons/fc";

const LoanSteps =()=>{
    return(
        <LoanStepsWrapper>
            <div className=" text-center p-6">
                <div className="mb-8 font-bold text-2xl">
                    <h2>Find a Personal Loan in 4 Easy Steps</h2>
                </div>
                <div className="grid">
                    <div className="col-12 md:col-3 mb-4 px-5">
                    <span className="p-3 shadow-2 mb-3 inline-block border-circle" >
                         <FcSurvey className=" text-6xl"/>
                    </span>
                        <div className="mb-3 font-medium"><span className="step">1.</span> Fill out our simple form</div>
                    </div>
                    <div className="col-12 md:col-3 mb-4 px-5">
                    <span className="p-3 shadow-2 mb-3 inline-block border-circle">
                      <FcGenealogy className=" text-6xl"/>
                    </span>
                        <div className="mb-3 font-medium"><span className="step">2.</span> Get matched to lending partners</div>
                    </div>
                    <div className="col-12 md:col-3 mb-4 px-5">
                    <span className="p-3 shadow-2 mb-3 inline-block border-circle" >
                         <FcSalesPerformance className=" text-6xl"/>
                    </span>
                    <div className="mb-3 font-medium"><span className="step">3.</span> Compare & choose the best loan for you</div>
                </div>
                    <div className="col-12 md:col-3 mb-4 px-5">
                    <span className="p-3 shadow-2 mb-3 inline-block border-circle" >
                        <FcOnlineSupport className=" text-6xl"/>
                    </span>
                        <div className="mb-3 font-medium"><span className="step">4.</span>  Loan consultants will help you understand your options</div>
                    </div>
                </div>
            </div>

        </LoanStepsWrapper>
    )
}

export default LoanSteps