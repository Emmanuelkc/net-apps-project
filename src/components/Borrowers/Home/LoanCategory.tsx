import React, { FC } from "react";
import "primeflex/primeflex.css";
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css"; //theme
import { FcDebt } from "react-icons/fc";
import { TbBusinessplan } from "react-icons/tb";
import { BsHouseDoorFill } from "react-icons/bs";
import { FcAutomotive } from "react-icons/fc";
import { GiPlantsAndAnimals, GiShoppingCart } from "react-icons/gi";
import { useRouter } from "next/router";
import { LoanCategoriesWrap } from "../../../../styles/BorrowerDashboard/LoanCategory/loanCategory";
import { Button } from "primereact/button";

const BorrowersLoanCategory: FC = () => {
  const router = useRouter();
  const loanApplicationId = router.query.loanApplicationId;

  const LoanDetails = (e: any) => {
    e.preventDefault();
    router.push(`borrower/${loanApplicationId}/loan`);
  };

  const EcommerceProducts = (e: any) => {
    e.preventDefault();
    router.push(`borrower/${loanApplicationId}/ecommerce/home`);
  };



    return (
        <LoanCategoriesWrap>
            <h2 className="text-700 font-bold text-2xl pt-6">Choose a Loan that serves your purpose ?</h2>
            <p className="text-blue-900 font-italic">Compare loans in minutes</p>
            <div className="grid p-4">
                <div className="col-12 md:col-6 lg:col-3 ">
                    <div className="card" >
                        <div className="icon-wrap">
                            <FcDebt className="icon"/>
                        </div>
                        <p className="card-text-lg">Business Loan</p>
                        <p className="card-text-sm">
                            Attain that big dreams with our business loans. No matter the size of your business, we will provide
                            capital loans and asset financing to augment cash flow and expand your business.
                        </p>
                        <Button className="apply" onClick={LoanDetails}>apply for loan</Button>



                    </div>

                </div>
                <div className="col-12 md:col-6 lg:col-3 ">
                    <div className="card">
                        <div className="icon-wrap">
                            <BsHouseDoorFill   className="icon"/>
                        </div>
                        <p className="card-text-lg">Mortgage Loan</p>
                        <p className="card-text-sm">
                            Are you looking to buy your ideal home? We have you covered with our mortgage loan, designed to
                            finance completed and under-construction homes and properties.
                        </p>
                        <Button className="apply" onClick={LoanDetails}>apply for loan</Button>
                    </div>
                </div>
                <div className="col-12 md:col-6 lg:col-3 ">
                    <div className="card" >
                        <div className="icon-wrap">
                            <FcAutomotive   className="icon"/>
                        </div>
                        <p className="card-text-lg">Auto Loan</p>
                        <p className="card-text-sm">
                            Get a competitive rate and a payment plan to own that car you have always wanted.
                            The auto loan provides you with the buy now and pays later scheme.
                        </p>
                        <Button className="apply" onClick={LoanDetails}>apply for loan</Button>
                    </div>
                </div>
                <div className="col-12 md:col-6 lg:col-3 ">
                    <div className="card" >
                        <div className="icon-wrap">
                            <GiPlantsAndAnimals   className="icon"/>
                        </div>
                        <p className="card-text-lg">Agricultural Loan</p>
                        <p className="card-text-sm">
                            Netapps Agriculture Loan is a program that provides farmers and agro-allied businesses with the
                            funding they need to expand and maintain their farming business.
                        </p>
                        <Button className="apply" onClick={LoanDetails}>apply for loan</Button>
                    </div>
                </div>
                <div className="col-12 md:col-6 lg:col-3 ">
                    <div className="card" >
                        <div className="icon-wrap">
                            <GiShoppingCart   className="icon"/>
                        </div>
                        <p className="card-text-lg">Buy Product on Credit</p>
                        <p className="card-text-sm">
                            With Netapps E-commerce loan, you may shop anywhere and pay later
                            while spreading your payments at a low and affordable interest rate.
                        </p>
                        <Button className="apply" onClick={EcommerceProducts}>apply for loan</Button>
                    </div>
                </div>
                <div className="col-12 md:col-6 lg:col-3 ">
                    <div className="card">
                        <div className="icon-wrap">
                            <TbBusinessplan   className="icon"/>
                        </div>
                        <p className="card-text-lg">Investment Loan</p>
                        <p className="card-text-sm">
                            We help bridge the gap between uncertainties. With an investment loan, you can finance the acquisition
                            of shares, funds units, property, and other investment plan.
                        </p>
                        <Button className="apply" onClick={LoanDetails}>apply for loan</Button>
                    </div>
                </div>
            </div>
            <p className="card-text-lg">Investment Loan</p>
            <p className="card-text-sm">
              We help bridge the gap between uncertainties. With an investment
              loan, you can finance the acquisition of shares, funds units,
              property, and other investment plan.
            </p>
            <Button className="apply" onClick={LoanDetails}>
              apply for loan
            </Button>
    </LoanCategoriesWrap>
  );
};

export default BorrowersLoanCategory;