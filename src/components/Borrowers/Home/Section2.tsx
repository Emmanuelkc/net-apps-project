import React from "react";
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import bg from "../../../../public/Assets/H1.png"
import Image from "next/image";
import router from "next/router";
import {
    // Button,
    ButtonsContainer,
    HeroSection22
} from "../../../../styles/BorrowerDashboard/LoanCategory/loanCategory";
import {Button} from "primereact/button";

const Section2 = () => {

    const Borrowers =(e:any)=>{
        e.preventDefault()
        router.push('/borrower')
    }
    return (
        <HeroSection22 className="grid grid-nogutter text-800 ">
            <div className="col-12 md:col-6 p-6 text-center md:text-left flex align-items-center ">
                <section className="p-4">
                    <span className="block text-3xl font-bold mb-3">Our Lenders Serve Consumers Across the Credit Spectrum</span>
                    <p className="mt-0 mb-4 text-700 line-height-">
                        You dont need to have perfect credit to get a personal loan.
                        So instead of going from lender to lender to find something you qualify for,
                        let us help you explore all your options at once and find the best offers for you.
                    </p>
                    <p className="mt-0 mb-4 text-700 line-height-">It’s easy to get started in minutes with one of our predefined strategies
                        that matches your investment goals. Or create your own strategy with 15+
                        customizable investment criteria.
                        Automate your investments, or manage them actively – the choice is yours.⁴</p>

                    <ButtonsContainer>
                        <Button  className="Button" onClick={Borrowers}>Learn More</Button>
                    </ButtonsContainer>
                </section>

            </div>
            <div className=" side col-12 md:col-6 p-6  overflow-hidden flex flex-wrap align-items-center">
                <Image src={bg}
                       alt="hero-1"
                       width={450} height={400}
                       className="md:ml-auto block md:h-full"/>
            </div>
        </HeroSection22>


    );
};

export default Section2;