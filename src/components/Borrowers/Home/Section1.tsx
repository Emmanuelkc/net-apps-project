import React from "react";
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import bg from "../../../../public/Assets/L5.png"
import Image from "next/image";
import router from "next/router";
import {
    // Button,
    ButtonsContainer,
    HeroSection1
} from "../../../../styles/BorrowerDashboard/LoanCategory/loanCategory";
import {Button} from "primereact/button";

const Section1 = () => {

    const Borrowers =(e:any)=>{
        e.preventDefault()
        router.push('/borrower')
    }
    return (
        <HeroSection1 className="grid grid-nogutter text-800 p-4  ">
            <div className="col-12 md:col-6 overflow-hidden lg:pl-8  flex flex-wrap align-items-center Container ">
                <Image src={bg}
                       alt="hero-1"
                       width={400} height={450}
                       className="md:ml-auto block md:h-full "/>
            </div>
            <div className="col-12 md:col-6  text-center md:text-left flex align-items-center  ">
                <section>
                    <h4>How a Personal Loan with Netapps Works</h4>
                    <span className="block text-1xl font-bold mb-3">Apply in Minutes!</span>
                    <p className="mt-0 mb-4 text-700 line-height-4">
                        Get customized loan options based on what you tell us.
                    </p>
                    <span className="block text-1xl font-bold mb-3">Choose a Loan Offer</span>
                    <p className="mt-0 mb-4 text-700 line-height-4">
                        Select the rate, term, and payment options you like best.
                    </p>
                    <span className="block text-1xl font-bold mb-3">Get Funded</span>
                    <p className="mt-0 mb-4 text-700 line-height-4">
                        Once your loan is funded, we’ll send the money straight to your bank account or pay your creditors directly.
                    </p>

                    <ButtonsContainer>
                        <Button className="Button" onClick={Borrowers}>Learn More</Button>
                    </ButtonsContainer>
                </section>

            </div>

        </HeroSection1>


    );
};

export default Section1;