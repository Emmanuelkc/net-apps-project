import React from "react"

const CommonBanner = ({title}:any)=>{
    return(
        <div className="text-gray-100 justify-content-center align-items-center  flex-wrap common__Banner ">
            <div className="align-items-center">
                <h2 className="line-height-7 text-3xl text-white ">{title} 🔥</h2>
            </div>
        </div>

    )
}

export default CommonBanner