import React from "react"
import {useSelector} from "react-redux";
import {Headers} from "../../../../../../styles/BorrowerDashboard/BuyProduct/Header";
import Link from "next/link";
import {useRouter} from "next/router";

const MenuIcons =()=>{
    const router = useRouter()
    const {loanApplicationId} = router.query
    // @ts-ignore
    const totalQuantity = useSelector(state => state.cart.totalQuantity)
    const explore  =()=>{
        router.push(`/borrower/${loanApplicationId}/ecommerce/shop`)
    }
    const shoppingCart  =()=>{
        router.push(`/borrower/${loanApplicationId}/ecommerce/cart`)
    }
    return(


        <Headers className="text-center">
            <div className="grid pt-3">
                <div className="col-12 md:col-3">

                     <span className="cart__icon" onClick={explore}>
                         <i className="pi pi-heart-fill"></i>
                        <p className="font-bold text-teal-600">explore</p>
                     </span>

                </div>
                 <div className="col-12 md:col-3">
                <span className="cart__icon ">
                         <i className="pi pi-heart-fill"></i>
                         <span className="badge">6</span>
                          <p className="font-bold text-teal-600">Wishlist</p>
                     </span>

                </div>
                 <div className="col-12 md:col-3 " onClick={shoppingCart}>
                     <span className="cart__icon ">
                         <i className="pi pi-shopping-cart"></i>
                         <span className="badge">{totalQuantity}</span>
                          <p className="font-bold text-teal-600">Shopping Cart</p>
                     </span>
                </div>
                 <div className="col-12 md:col-3 ">
                    <span className="cart__icon ">
                         <i className="pi pi-heart-fill"></i>
                         <span className="badge">7</span>
                          <p className="font-bold text-teal-600">order</p>
                     </span>
                </div>


            </div>
        </Headers>

    )
}

export default MenuIcons