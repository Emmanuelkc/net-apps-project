import React from "react"
import {motion} from "framer-motion"
import serviceData from "../../../../Utility/Ecommerce/data/serviceData";
import {Service} from "../../../../../../styles/BorrowerDashboard/BuyProduct/Header";


const Services =()=>{

    return(
        <Service className="services m-4">
            <div className="grid m-4">
                {
                    serviceData.map((item,index)=>(
                        <div key={index} className="col-12 md:col-3">
                           <motion.div whileHover={{scale:1.2}}>
                               <div className="service__item flex"
                                    style={{background: `${item.bg}`}}
                               >
                                <span>
                                    <i className={item.icon}></i>
                                </span>
                                   <div>
                                       <h5>{item.title}</h5>
                                       <p>{item.subtitle}</p>
                                   </div>
                               </div>
                           </motion.div>
                        </div>
                    ))
                }

            </div>

        </Service>
    )
}

export default Services