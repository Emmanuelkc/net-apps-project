import React, {useState} from "react"
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import "primereact/resources/themes/saga-blue/theme.css";
import CommonBanner from "../Header/CommonBanner";
import {ButtonsContainer, CartWrapper} from "../../../../../../styles/BorrowerDashboard/BuyProduct/Header";
import Image from "next/image";
import {motion} from "framer-motion";
import {cartActions} from "../../../../Utility/redux/slice/cartSlice";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import {NumericFormat} from "react-number-format";
import {TbCurrencyNaira} from "react-icons/tb";


const ProductCart =()=>{
    // @ts-ignore
    const cartItems = useSelector((state) => state.cart.cartItems)
    // @ts-ignore
    const totalAmount = useSelector((state) => state.cart.totalAmount)
    const router = useRouter()
    const {loanApplicationId} = router.query
    const explore  =()=>{
        router.push(`/borrower/${loanApplicationId}/ecommerce/shop`)
    }

    return(
        <CartWrapper>
            <CommonBanner title="Shopping Cart"/>

            <div className="grid p-5">
                {
                    cartItems.length === 0 ?

                        (<div className="col-12 md:col-9">
                            <div className="table-head-text">
                                <h2 className="table-head">Cart</h2>
                            </div>
                                <h2 className="text-center flex justify-content-center align-items-center"> No Item added to cart</h2>
                        </div>

                      ):

                        (
                            <div className="col-12 md:col-9">
                                <div className="table-head-text">
                                    <h2 className="table-head">Cart</h2>
                                </div>
                                <table className="table">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        cartItems.map((item:any,index:any)=>(
                                            <CartList item={item} key={index}/>
                                            ))
                                    }

                                    </tbody>
                                </table>
                            </div>
                        )
                }


                <div className="col-12 md:col-3">
                    <div>
                        <h2>SubTotal</h2>
                        <div>
                            <TbCurrencyNaira className='naira-icon' />
                             <NumericFormat
                                 value={totalAmount}
                                 thousandsGroupStyle="lakh"
                                 thousandSeparator=","
                                 displayType="text"
                                 renderText={(value) => <b className="amount">{value}</b>}
                             />
                        </div>


                    </div>
                    <p className="text-gray-700">tax and Shipping fee</p>
                    <ButtonsContainer>
                        <motion.button whileTap={{scale:1.2}} className="Button w-full" onClick={explore}>
                            Continue Shopping
                        </motion.button>
                    </ButtonsContainer>
                    <ButtonsContainer className="mt-5">
                        <motion.button whileTap={{scale:1.2}} className="Button w-full" onClick={explore}>
                            CheckOut
                        </motion.button>

                    </ButtonsContainer>
                </div>
            </div>


        </CartWrapper>
    )
}

const CartList =({item}:any) =>{

    const dispatch = useDispatch()
    const deleteProduct =()=>{
        dispatch(cartActions.deleteItem(item.id))
    }

    return(
        <>
            <tr>
                <td data-label="Image">
                    <Image src={item.imgUrl} alt="watch" width={100} height={100}/>
                </td>
                <td data-label="Title">{item.productName}</td>
                <td data-label="Price">
                    <TbCurrencyNaira className='naira-icon-sm' />

                    <NumericFormat
                        value={item.price}
                        thousandsGroupStyle="lakh"
                        thousandSeparator=","
                        displayType="text"
                        renderText={(value) => <b className="amount-sm">{value}</b>}
                    />
                </td>
                <td data-label="Qty">{item.quantity}</td>
                <td  data-label="Delete" className='delete' >
                    <motion.i
                        whileTap={{scale:1.2}}
                        className="pi pi-trash"
                        onClick={deleteProduct}
                    ></motion.i>
                </td>
            </tr>
        </>
    )
}

export default ProductCart