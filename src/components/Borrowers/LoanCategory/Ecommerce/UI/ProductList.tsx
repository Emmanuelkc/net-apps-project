import React from "react"
import ProductCard from "./ProductCard";
import {useRouter} from "next/router";


const ProductList =({data,loanApplicationId,productDescription}:any)=>{

    return(
        <div className="grid m-6">
            {data?.map((item:any,index:any)=> (
                <ProductCard item={item} key={index} productId={productDescription} loanId={loanApplicationId}/>
            ))}
        </div>
    )
}

export default ProductList