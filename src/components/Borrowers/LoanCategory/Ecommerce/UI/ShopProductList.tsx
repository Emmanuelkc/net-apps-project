import React from "react"
import ShopProductCard from "./ShopProductCard";


const ShopProductList =({data}:any)=>{
    return(
        <div className="grid m-6">
            {data?.map((item:any,index:any)=> (
                <ShopProductCard item={item} key={index}/>
            ))}
        </div>
    )
}

export default ShopProductList