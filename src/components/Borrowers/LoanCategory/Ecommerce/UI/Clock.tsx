import React, {useEffect, useState} from "react"

const Clock:React.FC<any> = () => {
    const [days, setDays] =useState<any>()
    const [hours, setHours] =useState<any>()
    const [minutes, setMinutes] =useState<any>()
    const [seconds, setSeconds] =useState<any>()

    let interval:any;

    const countDown = ()=> {
        const destination:any = new Date("dec 24 2022")
        interval = setInterval(()=>{
            const now = new  Date().getTime()
            const different = destination - now
            const days = Math.floor(different / (1000 * 60 * 60 * 24))
            const hours = Math.floor(different % (1000 * 60 * 60 * 24)/(1000 * 60 * 60))
            const minutes = Math.floor(different % (1000 * 60 * 60)/ (1000 * 60))
            const seconds = Math.floor(different % (1000 * 60 * 60)/ (1000))
        if(destination < 0){
            clearInterval(interval.current)
        }else{
            setDays(days)
            setHours(hours)
            setMinutes(minutes)
            setSeconds(seconds)
        }

        })
    }

    useEffect(()=>{
        countDown()
    })




  return(
      <div className="clock__wrapper flex align-items-center p-2">
          <div className="clock__data flex align-items-center gap-5">
              <div className="line-height-3">
                  <h5  className="line-height-3">{days}</h5>
                  <h5  className="line-height-3">Days</h5>
              </div>
              <span  className="font-bold text-3xl pr-3">:</span>
          </div>
          <div className="clock__data flex align-items-center gap-5">
              <div className="line-height-3">
                  <h5  className="line-height-3">{hours}</h5>
                  <h5  className="line-height-3">Hours</h5>
              </div>
              <span  className="font-bold text-3xl pr-3">:</span>
          </div>
          <div className="clock__data flex align-items-center gap-5">
              <div className="line-height-3">
                  <h5  className="line-height-3">{minutes}</h5>
                  <h5  className="line-height-3">Minutes</h5>
              </div>
              <span  className="font-bold text-3xl pr-3">:</span>
          </div>
          <div className="clock__data flex align-items-center gap-5">
              <div className="line-height-3">
                  <h5  className="line-height-3">{seconds}</h5>
                  <h5  className="line-height-3">Seconds</h5>
              </div>
          </div>

      </div>
  )
}

export default Clock