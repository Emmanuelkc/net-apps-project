import React, {useRef, useState} from "react"
import {
    ButtonsContainer,
    ProductDescriptionWrapper
} from "../../../../../../styles/BorrowerDashboard/BuyProduct/Header";
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import CommonBanner from "../Header/CommonBanner";
import Image from "next/image";
import {motion} from "framer-motion";
import {useParams} from "react-router-dom";
import products from "../../../../Utility/Ecommerce/data/product";
import {useRouter} from "next/router";
import ProductList from "./ProductList";
import {useDispatch} from "react-redux"
import {cartActions} from "../../../../Utility/redux/slice/cartSlice";
import {toast} from "react-toastify";


const ProductDescription =() =>{

    const [tab, setTabs] = useState('desc')
    const [rating, setRating] = useState<any>(null)
    const reviewUser = useRef<any>('')
    const reviewMsg = useRef<any>('')
    const dispatch = useDispatch()


    const router = useRouter()
    const {productDescription} = router.query
    const {id} =  useParams();
    console.log(id,"hello ProductId");

    const product = products.find((item:any) => item.productDescription === id)
    const  { imgUrl, productName, price, avgRating, reviews, description, shortDesc, category}:any = product;

    const relatedProducts = products.filter((item) => item.category === category);

    const addToCart =()=>{
        dispatch(cartActions.addItem({
            id,
            productName,
            price,
            image:imgUrl,
        }))
        toast.success("product added to cart Successfully")
    }


    const submitHandle =(e:any)=>{
        e.preventDefault()
        const reviewUserName = reviewUser.current.value;
        const reviewUserMsg = reviewMsg.current.value;

     const reviewObj ={
         userName: reviewUserName,
         text:reviewUserMsg,
         rating,
     }
        console.log(reviewObj)
        toast.success("Review Submitted")
    }




    return(
        <ProductDescriptionWrapper>
            <CommonBanner title={productName}/>
            <div className="grid">
                <div className="col-12 md:col-6 overflow-hidden">
                    <Image src={imgUrl} alt="productDescription" width={400} height={350} priority/>
                </div>
                <div className="col-12 md:col-6 p-6 text-center md:text-left flex ">
                    <div className="product__details">
                        <h2>{productName}</h2>
                        <div className="product__rating">
                            <div className=" product__rating flex   gap-2 mb-3">
                                <span><i className="pi pi-star-fill"></i></span>
                                <span><i className="pi pi-star-fill"></i></span>
                                <span><i className="pi pi-star-fill"></i></span>
                                <span><i className="pi pi-star-fill"></i></span>
                                <span><i className="pi pi-star-fill"></i></span>
                            </div>
                            <p className="text-gray-600">(<span>{avgRating}</span>ratings)</p>
                        </div>
                           <div className="gap-5 flex align-items-center">
                               <span className="product__price">${price}</span>
                               <span>Category: {category.toUpperCase()}</span>
                           </div>
                        <p className="mt-3 text-gray-600">{shortDesc}</p>

                        <ButtonsContainer className="mt-4 ">
                            <motion.a whileTap={{scale:1.2}} onClick={addToCart}
                                      className="Button">Add to Cart</motion.a>
                        </ButtonsContainer>
                    </div>

                </div>
            </div>

                <div className="grid">
                    <div className="col-12  md:col-8">
                        <div className=" tab__wrapper gap-5 flex align-items-center">
                            <h5 className={`${tab === 'desc' ? 'active__tab' : ''}`}
                            onClick={()=> setTabs('desc')}
                            >Description</h5>
                            <h5  className={`${tab === 'rev' ? 'active__tab' : ''}`}
                                 onClick={()=> setTabs('rev')}
                            >Review ({reviews.length})</h5>
                        </div>

                        {
                            tab === 'desc' ? (
                                <div className=" mt-4 tab__content">
                                    <p>{description}</p>
                                </div>
                            ) :(
                                <div className=" product__review mt-4">
                                    <div className="review__wrapper p-2">
                                        <ul>
                                            {reviews?.map((items:any, index:any)=>(
                                                <li key={index}>
                                                    <h2>Emmanuel Deep tech</h2>
                                                    <span>{items.rating} (rating)</span>
                                                    <p>{items.text}</p>
                                                </li>
                                            ))}
                                        </ul>

                                        <div className="review__form">
                                            <h4>Leave A Review</h4>
                                            <form action="" onSubmit={submitHandle}>
                                                <div className="form__group">
                                                    <input type="text" placeholder="Enter your name" ref={reviewUser}/>
                                                </div>
                                                <div className="cursor-pointer form__group flex justify-content-center align-items-center gap-6 m-4">
                                                    <motion.span whileTap={{scale:1.2}} onClick={()=> setRating(1)}>1 <i className="pi pi-star-fill"></i></motion.span>
                                                    <motion.span whileTap={{scale:1.2}} onClick={()=> setRating(2)}>2 <i className="pi pi-star-fill"></i></motion.span>
                                                    <motion.span whileTap={{scale:1.2}} onClick={()=> setRating(3)}>3 <i className="pi pi-star-fill"></i></motion.span>
                                                    <motion.span whileTap={{scale:1.2}} onClick={()=> setRating(4)}>4 <i className="pi pi-star-fill"></i></motion.span>
                                                    <motion.span whileTap={{scale:1.2}} onClick={()=> setRating(5)}>5 <i className="pi pi-star-fill"></i></motion.span>
                                                </div>

                                                <div className="form__group">
                                                    <textarea ref={reviewMsg} rows={6}  placeholder="Product Review Message"/>
                                                </div>
                                                <ButtonsContainer>
                                                    <button className="Button" type="submit">Submit</button>
                                                </ButtonsContainer>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            )}

                    </div>

                </div>


            <div className="grid">
                <div  className="col-12 md:col-12">
                    <p className="section__title">you might also Like</p>
                    <ProductList data={relatedProducts}/>
                </div>

            </div>

        </ProductDescriptionWrapper>
    )
}

export default ProductDescription