import React from "react"
import {motion} from "framer-motion"
import Image from "next/image";
import { ToastContainer, toast } from 'react-toastify';
import {ProductCardWrapper} from "../../../../../../styles/BorrowerDashboard/BuyProduct/Header";
import  {useRouter} from "next/router";
import {useDispatch} from "react-redux";
import {cartActions} from "../../../../Utility/redux/slice/cartSlice"



const ShopProductCard =({item}:any)=>{
    const router = useRouter()
    const loanApplicationId = router.query.loanApplicationId
    const productDescription = router.query.productDescription
    const dispatch = useDispatch()

    const addToCart =()=>{
        dispatch(cartActions.addItem({
            id:item.id,
            productName:item.productName,
            price:item.price,
            imgUrl:item.imgUrl,
        }))
        toast.success("product added to cart Successfully")
    }
    
    const showDescription =(e:any)=>{
        e.preventDefault()
        router.push(`${productDescription}`)
    }

    return(
        <ProductCardWrapper className="col-12 md:col-3 ">
            <div className="product__item p-2" >
                <motion.div whileHover={{scale:0.9}} className="product__img"  onClick={showDescription}>
                    <Image src={item.imgUrl}   alt="productImg" height={230} width={250}/>
                </motion.div>
                <div className="p-2 product__info line-height-6">
                    <h5 className="product__name line-height-2">{item.productName}</h5>
                    <span>{item.category}</span>
                </div>
                <div className="p-2 product__card-bottom flex  justify-content-between">
                    <span className="price">${item.price}</span>
                    <motion.span whileTap={{scale:1.2}} onClick={addToCart}>
                        <i className="pi pi-plus-circle"></i>
                    </motion.span>
                </div>
            </div>
        </ProductCardWrapper>
    )
}

export default ShopProductCard