import 'primeflex/primeflex.css';
import "primereact/resources/themes/saga-blue/theme.css";
import React from 'react';
import {ProductItem} from "../../../../Utility/types";
import {TbCurrencyNaira} from "react-icons/tb";
import {Button} from "primereact/button";
import {AiFillStar} from "react-icons/ai";


const ProductItemDescription: React.FC<ProductItem> = ({ product, handleAddToCart }:any) => {
    return (
        <>
            <div  className="grid surface-0 text-800 m-4 p-3">
                <div  className="col-12 md:col-5  overflow-hidden flex align-items-center justify-content-center mb-4">
                    <picture>
                    <img src={product.image}  alt={product.title}
                         className="md:ml-auto block"
                           height={200} width={200}
                         // style={{width:"50%", height:"40%"}}
                    />
                    </picture>
                </div>
                <div className="col-12 md:col-7 p-6 text-center md:text-left flex align-items-center ">

                    <section>
                        <span className="block text-1xl  mb-2">{product.category}</span>
                        <span className='grid'>
                                      <AiFillStar className='text-orange-300 m-1 text-1xl'/>
                                      <AiFillStar className='text-orange-300 m-1 text-1xl'/>
                                      <AiFillStar className='text-orange-300 m-1 text-1xl'/>
                                           {/*<p className="mt-0 mb-4 text-700 line-height-3">{item.rate}</p>*/}
                                  </span>
                        <hr className="my-3 mx-1 border-top-1 border-bottom-none border-400 bold" />
                        <div className="text-1xl text-primary font-bold mb-3">{product.title}</div>
                        <p className="mt-0 mb-4 text-700 line-height-3">{product.description}</p>
                        <hr className="my-3 mx-0 border-top-1 border-bottom-none border-300" />
                        <div className="flex align-items-center mb-5">
                            <TbCurrencyNaira className="text-bold text-2xl mb-2"/>
                            <span className="block text-1xl font-bold mb-1">{product.price}</span>
                        </div>
                        <Button
                            onClick={() => handleAddToCart(product)}
                            label="add To Cart" type="button"
                            className="mr-3 mt-1  text-1xl font-bold p-button-raised border-round-3xl cursor-pointer" >
                        </Button>
                    </section>
                </div>
                <span className=" mt-12 font-bold text-black-alpha-50">Category: {product.category}</span>
            </div>
        </>



    );
}

export default ProductItemDescription