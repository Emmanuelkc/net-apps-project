import {CartHome, CartProducts} from "../../../../Utility/types"
import CartItems from "./CartItems";
import {ShoppingCartWarapper} from "../../../../../../styles/BorrowerDashboard/BuyProduct/EcommerceProduct";

const Cart_Home:React.FC<CartHome> = ({cartItems,addToCart,removeFromCart})=>{

    const calculateTotal = (items: CartProducts[]) =>
        items.reduce((ack:number, item)=> ack + item.amount * item.price, 0)

    return(
        <ShoppingCartWarapper>
            <h2 className="text-red-500 text-1xl">Your Shopping cart</h2>
            {cartItems.length === 0 ? <span> No Items In Cart</span> : null}
            {cartItems.map(item=>(
                <CartItems key={item.id}
                item={item}
                 addToCart={addToCart}
                removeFromCart={removeFromCart}
                />
            ))}
            <h5>Total: ${calculateTotal(cartItems).toFixed(2)}</h5>
        </ShoppingCartWarapper>
    )
}

export default Cart_Home