import 'primeflex/primeflex.css';
import "primereact/resources/themes/saga-blue/theme.css";
import React from 'react';
import {ProductItem} from "../../../../Utility/types";
import {TbCurrencyNaira} from "react-icons/tb";
import {BsFillTagsFill} from "react-icons/bs";
import {Button} from "../../../../../../styles/BorrowerDashboard/BuyProduct/EcommerceProduct";


const ProductItems: React.FC<ProductItem> = ({item, handleAddToCart, viewProduct,}) => {

    return (
            <>
                {/*col-12 md:col-6 lg:col-3*/}
                <div className="product-grid-item" >
                    <div className="product-grid-item-content" onClick={() => viewProduct}>
                        <picture>
                            <img src={item.image} alt={item.title}
                                 className="md:ml-auto block"
                                 height={200} width={200}
                            />
                        </picture>

                    </div>
                    <div className="product-item-bottom">
                        <span className="product-price">
                            <TbCurrencyNaira className="currency"/>
                            {item.price}
                        </span>
                        <div className="product-price text-orange-600 cart">
                            <div> <BsFillTagsFill className="tag"/>
                                {/*{item.rating.rate}*/}Hello Rating
                            </div>
                            <Button className=' '
                                    type="button" onClick={() => handleAddToCart(item)}>
                                add to cart
                            </Button>
                        </div>
                        <div className="product-name">{item.title}</div>
                    </div>
                </div>
            </>
    );
}

export default ProductItems