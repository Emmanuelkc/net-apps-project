import {CartItems} from "../../../../Utility/types";
import {Button} from "primereact/button";
import {CartItemsWrapper} from "../../../../../../styles/BorrowerDashboard/BuyProduct/EcommerceProduct";
import React from "react";


const CartItems:React.FC<CartItems> = ({item, addToCart, removeFromCart}) => {
  return(
     <CartItemsWrapper>
         <div>
             <p className="text-bluegray-600 pt-2">{item.title}</p>
             <div className="information">
                 <p className="text-purple-800"> Price: ${(item.price).toFixed(2)}</p>
                 <p className="text-purple-800"> Total: ${(item.amount * item.price).toFixed(2)}</p>
             </div>

             <div className="buttons">
                 <Button className="btn" onClick={() => removeFromCart(item.id)}>-</Button>
                 <p>{item.amount}</p>
                 <Button className="btn" onClick={() => addToCart(item)}>+</Button>
             </div>

         </div>

         <picture>
             <img src={item.image} alt={item.title}/>
         </picture>

     </CartItemsWrapper>
  )
}

export default CartItems