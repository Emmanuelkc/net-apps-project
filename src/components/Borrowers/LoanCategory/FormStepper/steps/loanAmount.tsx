import React from "react"
import {LoanTermFormWrap} from "../../../../../../styles/BorrowerDashboard/LoanApplication";
import {loanTermsFormData} from "../../../../Utility/modalData";


const LoanAmount=()=>{
    return(
        <LoanTermFormWrap>
            <div className="grid text-800 mt-3">
                <div className=" surface-10 text-800  col-12 md:col-12 overflow-hidden  text-center  align-items-center">
                            <div className="terms-section text-800 text-2xl font-bold mb-3 text-primary ">
                                <h2 className="block text-1xl font-bold mb-3">Loan Application</h2>
                            </div>
                        <div className="right-flex ">
                            {loanTermsFormData.map(inputData => {
                                return(
                                    <div key={inputData.name}>
                                        {
                                            <>
                                                <label className="block text-600 font-medium mb-2 font-bold">{inputData.label}</label>
                                                <input className="input mb-6  pl-2"
                                                       name={inputData.name}
                                                       required={inputData.required}
                                                       datatype={inputData.dataType}
                                                       placeholder={inputData.placeholder}
                                                />
                                            </>
                                        }
                                    </div>
                                )
                            })}
                        </div>

                    {/*<Button*/}
                    {/*    // onClick={successAlert}*/}
                    {/*    className="cursor-pointer flex font-bold mt-4 p-button-raised p-button-rounded white-space-nowrap">*/}
                    {/*    <TbCurrencyNaira className='naira'/>Apply*/}
                    {/*</Button>*/}
                </div>

            </div>
        </LoanTermFormWrap>
    )
}

export default LoanAmount