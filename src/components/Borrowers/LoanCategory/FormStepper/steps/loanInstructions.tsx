import {useContext} from "react";
import {StepperContext} from "../../../../Utility/StepperForm/StepperContext/StepperContext";
import React from "react"
import 'primeflex/primeflex.css';
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css";
import {loanApplication} from "../../../../Utility/DataFetch"
import {LoanTermFormWrap} from "../../../../../../styles/BorrowerDashboard/LoanApplication";
import {useRouter} from "next/router";



const LoanInstructions =()=>{
    const {userData, setUserData}: any = useContext(StepperContext)

    // const handleChange =(e:any)=>{
    //     const {name, value} = e.target;
    //     setUserData({...userData, [name]: value});
    // }

    const router = useRouter()
    // const loanDetailsId = router.query.loanDetailsId
    return(
        <LoanTermFormWrap>
            <div className="grid grid-nogutter mt2">
                <div className="surface-10 col-12 md:col-12    overflow-hidden  ">
                    <div className="terms-section">
                        <div className="ml-4">
                            <h2 className="block text-1xl font-bold  mt-4 text-center">Loan Application Instructions</h2>
                            <p className="text-red-500  text-center"> Applicants must Click on each box to acknowledge instructions</p>
                        </div>
                        <>
                            {loanApplication.map(( rules:{ name: string}, index:any) => {
                                return (
                                    <div key={index} className="checkbox-text">
                                        <input
                                            type="checkbox"
                                            id={`custom-checkbox-${index}`}
                                            name={rules.name}
                                        />
                                        <label className="checkbox-label" htmlFor={`custom-checkbox-${index}`}>{rules.name}</label>
                                    </div>
                                );
                            })}
                        </>
                    </div>
                </div>


            </div>
        </LoanTermFormWrap>
    )
}

export default LoanInstructions