import React, { useState, useEffect } from "react";
import { BsFillPatchCheckFill } from "react-icons/bs";
import PaymentModal from "../../../Utility/PaymentModal";
import {
    ModalType,
    repayLoanModalData,
    updateAcctModalData,
} from "../../../Utility/modalData";
import { LoanTableWrap } from "../../../../../styles/BorrowerDashboard";
import { NumericFormat } from "react-number-format";
import {GiTakeMyMoney} from "react-icons/gi";
import ReactPaginate from "react-paginate";
import router from "next/router";
import {LoanTransactionProps} from "../../../Utility/types"



const PendingTransaction = ({loanTableData,BorrowersTransactionHeaders}:any, {TransactionDetailsId}: any) => {
    const [payModal, setPayModal] = useState({
        open: false,
        modalType: ModalType.NONE,
    });

    const productDescription = (e: any) =>{
        e.preventDefault()
        router.push(`dashboard/${TransactionDetailsId}`)
    }

    const [currentItems, setCurrentItems] = useState<LoanTransactionProps[]>([]);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);
    const itemsPerPage = 6;

    useEffect(() => {
        const endOffset = itemOffset + itemsPerPage;
        setCurrentItems(loanTableData.slice(itemOffset, endOffset));
        return setPageCount(Math.ceil(loanTableData.length/itemsPerPage));
    }, [itemOffset, itemsPerPage, loanTableData]);

    const handlePageClick = (event: any) =>{
        const newOffset = (event.selected * itemsPerPage) % loanTableData.length;
        setItemOffset(newOffset)
    }

    return (
        <LoanTableWrap>
            <div className="card container">
                <div className="table-head-text">
                    <h2 className="table-head"> Pending Loan Transaction</h2>
                </div>
                <table className="table">
                    <thead>
                    <tr>
                        {BorrowersTransactionHeaders.map((rows: { id: number; label: string }) => {
                            return <th key={rows.id}>{rows.label}</th>;
                        })}
                    </tr>
                    </thead>
                    <tbody>
                    {currentItems.map((row: any) => {
                        return (
                            <tr key={row.id} data-heading={loanTableData}>
                                {row.status === "pending" &&
                                    <>
                                        <td data-label="Product">{row.product}</td>
                                        <td data-label="Date Applied">{row.applied}</td>
                                        <td data-label="Loan Amount">
                                            <NumericFormat
                                                value={row.loanAmount}
                                                thousandsGroupStyle="lakh"
                                                thousandSeparator=","
                                                displayType="text"
                                                renderText={(value) => <b>{value}</b>}
                                            />
                                        </td>
                                        <td data-label="Loan Status">
                                            {row.status}
                                        </td>
                                        <td data-label="Approved Amount">
                                            {row.status === "approved" ?
                                                <NumericFormat
                                                    value={row.approvedAmount}
                                                    thousandsGroupStyle="lakh"
                                                    thousandSeparator=","
                                                    displayType="text"
                                                    renderText={(value) => <b>{value}</b>}
                                                />
                                                :
                                                <div className="">
                                                    {row.status}
                                                    <BsFillPatchCheckFill className="payment-icon" />
                                                </div>
                                            }

                                        </td>
                                        <td data-label="Date Due">
                                            {row.status === "approved" ?
                                                (<div className="">
                                                    {row.due}
                                                    <BsFillPatchCheckFill className="payment-icon" />
                                                </div>)
                                                :
                                                (<div className="">
                                                    {row.status}
                                                    <BsFillPatchCheckFill className="payment-icon" />
                                                </div>)
                                            }
                                        </td>
                                        <td data-label="Re-Payment Status">
                                            {row.repaymentStatusRepay === "paid-loan" || row.status === "pending" ? (
                                                <div className="paid" >
                                                    {row.repaymentStatusRepay}
                                                    <BsFillPatchCheckFill className="payment-icon" />
                                                </div>
                                            ) : (
                                                <div className="repay"  onClick={() =>
                                                    setPayModal({open: true, modalType: ModalType.REPAY_LOAN_MODAL,})}>
                                                    {row.repaymentStatusRepay}
                                                    <GiTakeMyMoney className="payment-icon" />
                                                </div>
                                            )
                                            }
                                        </td>
                                        <td  className="paid" onClick={productDescription}>View transaction</td>
                                    </>
                                }
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>

            {payModal.open ? (
                payModal.modalType === ModalType.UPDATE_ACCT_MODAL ? (
                    <PaymentModal
                        setPaymodal={setPayModal}
                        modalData={updateAcctModalData}
                    />
                ) : payModal.modalType === ModalType.REPAY_LOAN_MODAL ? (
                    <PaymentModal
                        setPaymodal={setPayModal}
                        modalData={repayLoanModalData}
                    />
                ) : null
            ) : null}

            <div className="custom flex justify-content-end align-items-end">
                <ReactPaginate
                    previousLabel = {'prev'}
                    nextLabel = {'next'}
                    breakLabel = {'...'}
                    pageCount = {pageCount}
                    pageRangeDisplayed = {4}
                    onPageChange = {handlePageClick}
                    containerClassName = {"pagination"}
                    pageLinkClassName = {"page-number"}
                    breakLinkClassName = {"page-item"}
                    previousLinkClassName = {"previous-btn"}
                    nextLinkClassName = {"next-btn"}
                    activeLinkClassName = {"active"}
                />
            </div>
        </LoanTableWrap>
    );
};


export default PendingTransaction;
