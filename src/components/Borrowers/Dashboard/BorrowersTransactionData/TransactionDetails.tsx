import {ProductDescWrap} from "../../../../../styles/BorrowerDashboard/BorrowersTransactionDescription/BorrowersTransactionDetails";
import {productDescription, productDescriptionTitle} from "../../../Utility/DataFetch";
import React from "react";
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';


const TransactionDetails =()=>{
    return(
        <>
            <ProductDescWrap>
                <div className='container'>
                 <div className="m-4">
                     <div className="font-medium text-3xl text-900 mb-3">Loan Information</div>
                     <div className="text-500 mb-5">Business Loan History.</div>

                 </div>
                    <table className='table'>
                        <thead className='table-head'>
                        {productDescriptionTitle.map((row)=>{
                            return(
                                <tr key={row.col1}>
                                    <th>{row.col1}</th>
                                    <th >{row.col2}</th>
                                    <th>{row.col3}</th>
                                    <th>{row.col4}</th>
                                </tr>
                            )
                        })}
                        </thead>

                        <tbody className='table-body'>
                        {productDescription.map((row) =>{
                            return(
                                <tr key={row.loanPrdtDesc}>
                                    <td className='right-row'>{row.loanPrdtDesc}:</td>
                                    <td className='mid-row'>{row.date}</td>
                                    <td className='mid-row'>{row.amount}</td>
                                    <td className="paid">{row.status}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                </div>
            </ProductDescWrap>



        </>
    )
}


export default TransactionDetails