import React, { FC } from 'react';
import 'primeflex/primeflex.css';
import "primeicons/primeicons.css";
import "primereact/resources/themes/saga-blue/theme.css";  //theme
import {TbCurrencyNaira} from 'react-icons/tb'
import {BsCheckLg} from "react-icons/bs";
import {FcCancel} from "react-icons/fc"
import {GoStop} from "react-icons/go"
import {LoanStatWrap} from "../../../../styles/BorrowerDashboard"

const LoanStats:FC = () => {

    return (
        <LoanStatWrap>
            <div className='grid m-2'>
                <div className="col-12  md:col-6 lg:col-3">
                    <div className=" bg-stat-wrap shadow-2 p-2 border-round-md">
                        <div className="flex justify-content-between p-2">
                            <div>
                                <span className="text-1xl font-bold">Approved Loans</span>
                                <h2>0</h2>
                            </div>
                            <div className="naira-circle m-4">
                                <BsCheckLg className='check-icon'/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 hello md:col-6 lg:col-3">
                    <div className="bg-stat-wrap shadow-2 p-2 border-round-md">
                        <div className="flex justify-content-between p-2">
                            <div>
                                <span className="text-1xl font-bold">Paid Loans</span>
                                <h2>0</h2>
                            </div>
                            <div className="naira-circle m-4">
                                <TbCurrencyNaira className='naira-icon'/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 hello md:col-6 lg:col-3">
                    <div className="bg-stat-wrap shadow-2 p-2 border-round-md">
                        <div className="flex justify-content-between p-2">
                            <div>
                                <span className="font-bold text-red-600 text-1xl">pending Loans</span>
                                <h2>0</h2>
                            </div>
                            <div className="naira-circle m-4">
                                <FcCancel className='cancel-icon'/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 hello md:col-6 lg:col-3">
                    <div className="bg-stat-wrap shadow-2 p-2 border-round-md">
                        <div className="flex justify-content-between p-2">
                            <div>
                                <span className="text-red-600 text-1xl font-bold">Un-Paid Loans</span>
                                <h2>0</h2>
                            </div>
                            <div className="naira-circle m-4">
                                <GoStop className='go-icon'/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </LoanStatWrap>
    );
}

export default LoanStats;