import type { NextPage } from 'next'
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
// import 'primereact/resources/themes/lara-light-indigo/theme.css';
import LoanStats from "../../../components/Borrowers/Dashboard/LoanStats"
import * as React from 'react';
import LoanDataTable from '../../../components/Borrowers/Dashboard/BorrowersTransactionData/BorrowersTransactions';
import {loanTableData, BorrowersTransactionHeaders} from "../../../components/Utility/DataFetch";
import { useRouter } from 'next/router';
import BorrowersWalletStats from "../../../components/Borrowers/Dashboard/BorrowersWalletChart";
import {TabPanel, TabView} from "primereact/tabview";
import ApprovedTransaction from "../../../components/Borrowers/Dashboard/BorrowersTransactionData/ApprovedTransactions";
import PendingTransaction from "../../../components/Borrowers/Dashboard/BorrowersTransactionData/PendingTransactions";

const Dashboard: NextPage = () => {
    const router = useRouter()
    const TransactionDetailsId = router.query.TransactionDetailsId;

    return (
        <>
            <BorrowersWalletStats />
            <LoanStats/>
            <div className="p-grid ">
                <div className="p-col-6 p-col-12 p-md-6">
                    <TabView >
                        <TabPanel header=" All Transactions" className="text-xs">
                            <LoanDataTable
                                TransactionDetailsId={TransactionDetailsId}
                                BorrowersTransactionHeaders={BorrowersTransactionHeaders}
                                loanTableData={loanTableData}
                            />
                        </TabPanel>
                        <TabPanel header="Approved Loan" className="text-xs">
                            <ApprovedTransaction
                                TransactionDetailsId={TransactionDetailsId}
                                BorrowersTransactionHeaders={BorrowersTransactionHeaders}
                                loanTableData={loanTableData}
                            />
                        </TabPanel>
                        <TabPanel header="Pending Loan Application" className="text-xs">
                            <PendingTransaction
                                TransactionDetailsId={TransactionDetailsId}
                                BorrowersTransactionHeaders={BorrowersTransactionHeaders}
                                loanTableData={loanTableData}
                            />
                        </TabPanel>
                    </TabView>
                </div>
            </div>
        </>
    )
}


export default Dashboard
