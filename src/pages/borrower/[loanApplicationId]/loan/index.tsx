import React, {useState} from "react"
import {StepperContext} from "../../../../components/Utility/StepperForm/StepperContext/StepperContext";
import Stepper from "../../../../components/Utility/StepperForm/Stepper";
import StepperControl from "../../../../components/Utility/StepperForm/StepperControl";
import LoanInstructions from "../../../../components/Borrowers/LoanCategory/FormStepper/steps/loanInstructions";
import LoanAmount from "../../../../components/Borrowers/LoanCategory/FormStepper/steps/loanAmount";


const LoanAppilcation:React.FC = () => {
    const [currentStep, setCurrentStep] = useState<any>(1)
    const [userData, setUserData]= useState<any>('')
    const [finalData, setFinalData]= useState<any[]>([])



    const steps = [
        "Loan Amount",
        "Loan Instructions",
        "complete",
    ]

    const displayStep =(step:any)=>{
        switch (step){
            case 1:
                return <LoanAmount/>
            case 2:
                return <LoanInstructions/>
            case 3:
                return <LoanAmount/>
            // default:
        }
    }

    const handleClick =(direction:any)=>{
        let newStep = currentStep;
        direction === "next" ? newStep ++ : newStep --;
        newStep > 0 && newStep << steps.length && setCurrentStep(newStep)

    }

    return (
        <div className="grid p-6 surface-10 text-800 justify-content-end loanSteps ">
            {/*<LoanSteps/>*/}
            <div className="col-12 md:col-6 text-center m-2 md:text-left align-items-center shadow-3 border-round  surface-50  ">
                    {/*    Stepper  */}
                    <div className="mt-3">
                        <Stepper steps={steps} currentStep={currentStep}/>
                    </div>
                    {/*    Display Component*/}
                    <div className="p-6">
                        <StepperContext.Provider
                            value={{userData, setUserData, finalData, setFinalData}}>
                            {displayStep(currentStep)}
                        </StepperContext.Provider>
                    </div>


                    {/*  Navigation Control  */}
                    {/*{currentStep !== steps.length &&*/}
                        <StepperControl
                            handleClick={handleClick}
                            currentStep={currentStep}
                            steps={steps}
                        />
                     {/*}*/}

            </div>
        </div>


    )
}


export default LoanAppilcation