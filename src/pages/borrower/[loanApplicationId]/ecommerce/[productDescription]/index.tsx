import React from "react"
import ProductDescription from "../../../../../components/Borrowers/LoanCategory/Ecommerce/UI/ProductDescription";
import {useRouter} from "next/router";
import MenuIcons from "../../../../../components/Borrowers/LoanCategory/Ecommerce/Header/MenuIcons";


const ProductDescriptions =()=>{
    const router = useRouter()
    const {productDescription} = router.query

    return(
        <>
            <MenuIcons/>
            <ProductDescription />
        </>

    )
}

export default ProductDescriptions