import React,{useState, useEffect} from 'react';
import type { NextPage } from 'next';
import Ecommerce_Hero from "../../../../components/Borrowers/LoanCategory/Ecommerce/Header/HeroSlide";
import Services from "../../../../components/Borrowers/LoanCategory/Ecommerce/Header/Services";
import ProductList from "../../../../components/Borrowers/LoanCategory/Ecommerce/UI/ProductList";
import  products from "../../../../components/Utility/Ecommerce/data/product"
import counterImg from "../../../../components/Utility/Ecommerce/images/counter-timer-img.png"
import Image from "next/image";
import Clock from "../../../../components/Borrowers/LoanCategory/Ecommerce/UI/Clock";
import {ButtonsContainer} from "../../../../../styles/BorrowerDashboard/BuyProduct/Header";
import {useRouter} from "next/router";
import MenuIcons from "../../../../components/Borrowers/LoanCategory/Ecommerce/Header/MenuIcons";

const Ecommerce_: NextPage = ()=>{
    // const id = 5
    const router = useRouter()
    const {loanApplicationId} = router.query
    const {productDescription} = router.query

    const [trendingProducts, setTrendingProducts] = useState<any>([])
    const [bestSalesProducts, setBestSalesProducts] = useState<any>([])
    const [mobileProducts, setMobileProducts] = useState<any>([])
    const [wirelessProducts, setWirelessProducts] = useState<any>([])
    const [popularProducts, setPopularProducts] = useState<any>([])

    useEffect(()=>{
          const filteredTrendingProducts = products.filter((item:any) => item.category === "chair")
          const filteredBestSalesProducts = products.filter((item:any) => item.category === "sofa")
          const filteredMobileProducts = products.filter((item:any) => item.category === "mobile")
          const filteredWirelessProducts = products.filter((item:any) => item.category === "wireless")
          const filteredPopularProducts = products.filter((item:any) => item.category === "watch")


        setTrendingProducts(filteredTrendingProducts )
        setBestSalesProducts(filteredBestSalesProducts)
        setMobileProducts(filteredMobileProducts)
        setWirelessProducts(filteredWirelessProducts)
        setPopularProducts(filteredPopularProducts)

    },[])


    const visitStore  =()=>{
        router.push(`/borrower/${loanApplicationId}/ecommerce/shop`)
    }

    return(
        <>
            <MenuIcons/>
            <Ecommerce_Hero/>
            <Services/>
            <section className="trending__products">
                    <div className="grid">
                        <div  className="col-12 md:col-12 text-center">
                            <h2 className="section__title"> Trending Products</h2>
                        </div>
                        <ProductList data={trendingProducts} loanApplicationId={loanApplicationId} productDescription={productDescription}/>
                    </div>
            </section>

            <section className="trending__products">
                <div className="grid">
                    <div  className="col-12 md:col-12 text-center">
                        <h2 className="section__title">Best Sales</h2>
                    </div>
                    <ProductList data={bestSalesProducts}/>
                </div>
            </section>
            <section className="timer__count">
                <div className="grid ">
                    <div  className="col-12 md:col-6 p-6">
                        <div className="clock__top-content">
                            <h3 className="pb-3">Limited offers Discover the Online Shopping World</h3>
                            <h2  className="pb-2" >Quality ArmChair</h2>
                        </div>
                        <Clock/>
                        <ButtonsContainer >
                            {/*<Link*/}
                            {/*      href={{*/}
                            {/*          pathname: `/borrower/${loanApplicationId}/ecommerce/shop`,*/}
                            {/*      }}>*/}
                            {/*        <a className="Button">Visit Store</a>*/}
                            {/*</Link>*/}
                            <a onClick={visitStore} className="Button">Visit Store</a>
                        </ButtonsContainer>

                    </div>
                    <div  className="col-12 md:col-6 justify-content-end flex p-4">
                        <Image src={counterImg}   alt="counterImg" height={200} width={320}/>
                    </div>
                </div>
            </section>

            <section className="new__arrivals mt-3">
                <div className="grid">
                    <div  className="col-12 md:col-12 text-center">
                        <h2 className="section__title">New Arrivals</h2>
                    </div>
                    <ProductList data={mobileProducts}/>
                    <ProductList data={wirelessProducts}/>
                </div>
            </section>

            <section className="new__arrivals mt-3">
                <div className="grid">
                    <div  className="col-12 md:col-12 text-center">
                        <h2 className="section__title">popular In Category</h2>
                    </div>
                    <ProductList data={popularProducts}/>
                </div>
            </section>



        </>
    )
}


export default Ecommerce_