import React,{useState} from "react"
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import CommonBanner from "../../../../../components/Borrowers/LoanCategory/Ecommerce/Header/CommonBanner";
import {ShopWrapper} from "../../../../../../styles/BorrowerDashboard/BuyProduct/Header";

import products from "../../../../../components/Utility/Ecommerce/data/product";
import ShopProductList from "../../../../../components/Borrowers/LoanCategory/Ecommerce/UI/ShopProductList";
import MenuIcons from "../../../../../components/Borrowers/LoanCategory/Ecommerce/Header/MenuIcons";

const Shop =()=>{
const [productsData, setProductsData] = useState(products)
    const handleFilter =(e:any)=> {
        const filterValue = e.target.value;
        if(filterValue === "sofa"){
            const filterProducts = products.filter(
                (item) => item.category === "sofa"
            );
            setProductsData(filterProducts)
        }
        if(filterValue === "mobile"){
            const filterProducts = products.filter(
                (item) => item.category === "mobile"
            );
            setProductsData(filterProducts)
        }
        if(filterValue === "chair"){
            const filterProducts = products.filter(
                (item) => item.category === "chair"
            );
            setProductsData(filterProducts)
        }
        if(filterValue === "watch"){
            const filterProducts = products.filter(
                (item) => item.category === "watch"
            );
            setProductsData(filterProducts)
        }
        if(filterValue === "wireless"){
            const filterProducts = products.filter(
                (item) => item.category === "wireless"
            );
            setProductsData(filterProducts)
        }
    }

    const handleSearch = (e:any)=>{
    const searchItem = e.target.value
        const searchProducts = products.filter(item => item.productName.toLowerCase().includes(searchItem.toLowerCase()))

        setProductsData(searchProducts)
    }

    return(
        <div style={{width:"100%"}}>
            <MenuIcons/>
         <CommonBanner title="net apps market place"/>
            <ShopWrapper className="grid mt-4 justify-content-center text-center flex ">
                <div className="col-12 md:col-3">
                    <div className="filter__widget">
                        <select onClick={handleFilter}>
                            <option>Filter By Category</option>
                            <option value="sofa">Sofa</option>
                            <option value="mobile">Mobile</option>
                            <option value="chair">Chair</option>
                            <option value="watch">Watch</option>
                            <option value="wireless">Wireless</option>
                        </select>
                    </div>
                </div>
                <div className="col-12 md:col-6">
                    <div className="search__box">
                        <input type="text" placeholder="Search.........." onChange={handleSearch}/>
                        <span> <i className="pi pi-search"> </i></span>
                    </div>
                </div>
                <div className="col-12 md:col-3">
                    <div className="filter__widget">
                        <select>
                            <option>Sort By</option>
                            <option value="ascending">Ascending</option>
                            <option value="descending">Descending</option>
                        </select>
                    </div>
                </div>

            </ShopWrapper>

            <section className="new__arrivals mt-3 justify-content-center flex ">
                <div className="grid p-4">
                    {productsData.length === 0 ? (<h2 className="ml-5">No Product Found !!</h2>) : (<ShopProductList data={productsData}/>)}
                </div>
            </section>
        </div>
    )
}

export default Shop