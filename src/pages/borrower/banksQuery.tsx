import type { NextPage } from 'next'
import * as React from 'react';
import BankQuery from "../../components/BankQuery/BankQuery";

const BankQueries: NextPage = () => {
    return (
        <>
            <BankQuery/>
        </>
    )
}

export default BankQueries
