import type { NextPage } from 'next'
import React from "react"
import BorrowersLoanCategory from "../../components/Borrowers/Home/LoanCategory";
import Section1 from "../../components/Borrowers/Home/Section1";
import Section2 from "../../components/Borrowers/Home/Section2";
import LoanSteps from "../../components/Borrowers/Home/LoanSteps";
import Section3 from "../../components/Borrowers/Home/Section3";


const Borrower_Home_page: NextPage = () => {


    return (
        <>
            <Section2/>
            <LoanSteps/>
           <BorrowersLoanCategory/>
            <Section1/>
              <Section3/>
        </>
    )
}


export default Borrower_Home_page
