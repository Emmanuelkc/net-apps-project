import '../../styles/globals.css'
import 'primeicons/primeicons.css';
import "../../styles/BorrowerDashboard/BuyProduct/EcommerceProduct"
import * as React from 'react'
import type { AppProps } from 'next/app'
import {Provider} from "react-redux";
import store from "../components/Utility/redux/store";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
// import {QueryClient, QueryClientProvider} from "react-query";
// const client = new QueryClient()

function MyApp({ Component, pageProps }: AppProps) {
  return (
    // <QueryClientProvider client={client}>
      <Provider store={store}>
          <ToastContainer
              theme="light"
              position="top-right"
              autoClose={3000}
              closeOnClick
              draggable
              pauseOnHover={false}
              />
          <Component {...pageProps }/>
      </Provider>

    // </QueryClientProvider>
  )
}

export default MyApp


