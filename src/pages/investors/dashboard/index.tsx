import Investor_Stats from "../../../components/Investor/Dashboard/InvestorStats";
import WalletStats from "../../../components/Investor/Dashboard/WalletStat";
import * as React from "react";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import {
  transactionTableData,
  transactionTableHeaders,
} from "../../../components/Utility/DataFetch";
import { useRouter } from "next/router";
import InvestorTransactionData from "../../../components/Investor/Dashboard/InvestorsTransactionData/InvestorsTrasactions";
import { TabPanel, TabView } from "primereact/tabview";
import { NextPage } from "next";
import PendingTransactionData from "../../../components/Investor/Dashboard/InvestorsTransactionData/IncomingLoanTransaction";
import ApprovedTransactionData from "../../../components/Investor/Dashboard/InvestorsTransactionData/ApprovedLoanTransaction";

const Vendors_Home: NextPage = () => {
  const router = useRouter();
  const transactionDetailsId = router.query.transactionDetailsId;

  return (
    <>
      <WalletStats />
      <Investor_Stats />
      <div className="p-grid ">
        <div className="p-col-6 p-col-12 p-md-6">
          <TabView>
            <TabPanel header=" All Transactions" className="text-xs">
              <InvestorTransactionData
                transactionTableHeaders={transactionTableHeaders}
                transactionTableData={transactionTableData}
                transactionDetailsId={transactionDetailsId}
              />
            </TabPanel>
            <TabPanel header="Incoming Loan Request" className="text-xs">
              <PendingTransactionData
                transactionTableHeaders={transactionTableHeaders}
                transactionTableData={transactionTableData}
                transactionDetailsId={transactionDetailsId}
              />
            </TabPanel>
            <TabPanel header="Approved Loan" className="text-xs">
              <ApprovedTransactionData
                transactionTableHeaders={transactionTableHeaders}
                transactionTableData={transactionTableData}
                transactionDetailsId={transactionDetailsId}
              />
            </TabPanel>
            <TabPanel
              header="Un-Retrieved Loans / Debts"
              className="text-xs"
            ></TabPanel>
          </TabView>
        </div>
      </div>
    </>
  );
};

export default Vendors_Home;
