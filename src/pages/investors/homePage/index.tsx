import type { NextPage } from "next";
import React from "react";
import Section1 from "../../../components/Investor/Home/Section1";
import Section2 from "../../../components/Investor/Home/Section2";
import LoanSteps from "../../../components/Investor/Home/LoanSteps";
import Section3 from "../../../components/Investor/Home/Section3";
import Section4 from "../../../components/Investor/Home/Section4";

const Investor_Home_page: NextPage = () => {
  return (
    <>
      <Section2 />
      <Section3 />
      <Section1 />
      <LoanSteps />
      <Section4 />
    </>
  );
};

export default Investor_Home_page;
