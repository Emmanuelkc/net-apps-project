import React from "react";
import CustomerCartItems from "../../../components/Vendor/ProductsAddedToCart/customerCartItems";
import { cartItems } from "../../../components/Vendor/Utility/productDescData";

const CartItems = () => {
  return (
    <div>
      <CustomerCartItems cartItems={cartItems} />
    </div>
  );
};

export default CartItems;
