import { useRouter } from "next/router";
import React from "react";
import {
  approvedRequestTableData,
  pendingRequestTableData,
  productTableData,
  requestTableDataHeader,
} from "../../../components/Vendor/Utility/VendorData";
import CustomerLog from "../../../components/Vendor/VendorDashboard/CustomerLog";
import ProductTable from "../../../components/Vendor/VendorDashboard/ProductTable";
import VendorHero from "../../../components/Vendor/VendorDashboard/VendorHero";
import VendorStat from "../../../components/Vendor/VendorDashboard/VendorStat";
import { TabPanel, TabView } from "primereact/tabview";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import ProductDesc from "../../../components/Vendor/ProductDesc/ProductDesc";
import CustomerCartItems from "../../../components/Vendor/ProductsAddedToCart/customerCartItems";
import { cartItems } from "../../../components/Vendor/Utility/VendorData";
import ApprovedRequest from "../../../components/Vendor/VendorDashboard/ApprovedRequest";
import PendingRequest from "../../../components/Vendor/VendorDashboard/PendingRequest";

const Index = () => {
  const router = useRouter();
  const uploadProduct = router.query.uploadProduct;
  const productOrderId = router.query.productOrderId;
  const productDescriptionId = router.query.productDescriptionId;
  const cartItemsId = router.query.cartItemsId;
  const productRequestDescription = router.query.productRequestDescription;
  const customerHistoryId = router.query.customerHistoryId;

  return (
    <div>
      <VendorHero
        uploadProduct={uploadProduct}
        productOrderId={productOrderId}
      />
      <VendorStat />
      {/* <CustomerLog /> */}

      <div className="p-grid ">
        <div className="p-col-6 p-col-12 p-md-6">
          <TabView>
            <TabPanel header="Product Catalogue" className="text-xs">
              <ProductTable
                productTableData={productTableData}
                productDescriptionId={productDescriptionId}
              />
            </TabPanel>
            <TabPanel header="Customer Product Request" className="text-xs">
              <CustomerCartItems
                cartItems={cartItems}
                cartItemsId={cartItemsId}
                productRequestDescription={productRequestDescription}
              />
            </TabPanel>
            <TabPanel header="Customer Approved Requests" className="text-xs">
              <ApprovedRequest
                cartItems={cartItems}
                cartItemsId={cartItemsId}
              />
            </TabPanel>
            <TabPanel header="Customer Pending Requests" className="text-xs">
              <PendingRequest cartItems={cartItems} cartItemsId={cartItemsId} />
            </TabPanel>
          </TabView>
        </div>
      </div>
    </div>
  );
};

export default Index;
