import React, { useState } from "react";
import { StepperContext } from "../../components/Vendor/Utility/StepperForm/StepperContext/StepperContext";
import Stepper from "../../components/Vendor/Utility/StepperForm/Stepper";
import StepperControl from "../../components/Vendor/Utility/StepperForm/StepperControl";
import BusinessInfo from "../../components/Vendor/KYCForm/FormProgress/steps/BusinessInfo";
import VendorCredentials from "../../components/Vendor/KYCForm/FormProgress/steps/VendorCredentials";
import AccountVerification from "../../components/Vendor/KYCForm/FormProgress/steps/AccountVerification";
import TermsAndConditions from "../../components/Vendor/KYCForm/FormProgress/steps/TermsAndConditions";

const VendorKYCForm: React.FC = () => {
  const [currentStep, setCurrentStep] = useState<any>(1);
  const [userData, setUserData] = useState<any>("");
  const [finalData, setFinalData] = useState<any[]>([]);

  const steps = [
    "Business Information",
    "Business Credentials",
    "Account Verification",
    "Terms and Conditions",
  ];

  const displayStep = (step: any) => {
    switch (step) {
      case 1:
        return <BusinessInfo />;
      case 2:
        return <VendorCredentials />;
      case 3:
        return <AccountVerification />;
      case 4:
        return <TermsAndConditions />;
      // default:
    }
  };

  const handleClick = (direction: any) => {
    let newStep = currentStep;
    direction === "next" ? newStep++ : newStep--;
    newStep > 0 && newStep << steps.length && setCurrentStep(newStep);
  };

  return (
    <div className="grid p-6 surface-10 text-800 justify-content-end kycSteps ">
      {/*<LoanSteps/>*/}
      <div className="col-12 md:col-6 text-center m-2 md:text-left align-items-center shadow-3 border-round  surface-50  ">
        {/*    Stepper  */}
        <div className="mt-3">
          <Stepper steps={steps} currentStep={currentStep} />
        </div>
        {/*    Display Component*/}
        <div className="p-6">
          <StepperContext.Provider
            value={{ userData, setUserData, finalData, setFinalData }}
          >
            {displayStep(currentStep)}
          </StepperContext.Provider>
        </div>

        {/*  Navigation Control  */}
        {/*{currentStep !== steps.length &&*/}
        <StepperControl
          handleClick={handleClick}
          currentStep={currentStep}
          steps={steps}
        />
        {/*}*/}
      </div>
    </div>
  );
};

export default VendorKYCForm;
