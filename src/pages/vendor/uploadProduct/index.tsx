import fileUpload from 'express-fileupload'
import React, { useState } from 'react'
import ProductUpload from "../../../components/Vendor/UploadProduct/ProductUpload"

const uploadPrdt = () => {
    const [files, setFiles] = useState([{ name: 'myFile.pdf'}])

  return (
    <ProductUpload files={files} setFiles={setFiles}/>
  )
}

export default uploadPrdt