import styled from "styled-components";

export const Wrapper = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
`

export const TitleText = styled.h2`
padding-bottom: 1rem;
font-size: 20px;
text-align: center;
`