import styled from "styled-components";

export const UserFormWrap = styled.div`
  display: flex;
  flex-direction: column;

  .flex-top, .flex-bottom {
    display: flex;
    flex-direction: column;
  }

  label {
    text-align: left;
    padding-left: 5px;
    font-size: 1.125rem !important;
    color: #535b61;
    font-family: "Poppins", sans-serif;
    margin-top: 0;
    margin-bottom: 1rem;
  }

  input {
    width: 35vw;
    //height: 1.45rem;
    background-color: #e9ecef;
    padding: 1.10rem .96rem;
    font-size: 1rem;
    border-top: 1px solid #777;
    border-bottom:1px  solid  #777;
    border-left: none;
    border-right: none;
    font-weight: 400;
    appearance: none;
    border-radius: .25rem;
    box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.15) !important;
    margin-bottom: 2.4rem;

    &:focus {
      border-top: 1px solid #0d6efd;
      border-bottom:1px  solid  #0d6efd;
      outline: #0d6efd;
      background-color: #ffffff;
    }
  }
`  

  

