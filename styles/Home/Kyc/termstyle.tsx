import styled from "styled-components";

export const TermWrap = styled.div`
    text-align: justify;
    margin-left: 15rem;
    margin-right: 15rem;
    margin-top: -3rem;
    line-height: 1.2rem;

  .title{
  font-size: 12px;
  margin-top: 4rem;
  }

.terms-uppercase p{
    font-size: 14px;
    margin-bottom: 1.5rem;
    line-height: 1.4rem;
}

p{
    font-size: 11px;
    margin-bottom: 1.5rem;
    line-height: 1.4rem;
}

label{
    padding-bottom: 3rem;
    font-size: 11px;
    font-weight: 900;
}

/* .check input{
    padding-top: 2rem;
} */
`