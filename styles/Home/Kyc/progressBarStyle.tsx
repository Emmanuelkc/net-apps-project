import styled from "styled-components";

export const ProgressBarWrap = styled.div`
margin: 0;
padding: 0;
box-sizing: border-box;
display: flex;
justify-content: center;
align-items: center;
/* height: 25vh; */
width: 100vw;

.content{
    align-items: center;
    width: 90vw;
    
}

.progress-bar{
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: relative;
}

.progress-bar::before{
    content: "";
    position: absolute;
    background-color: #e0e0e0;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    border-radius: 5px;
    width: 100%;
    height: 4px;
    z-index: -1;
    transition: 0.4s ease;
}

.circle{
    background-color: #ffffff;
    color: #999999;
    border-radius: 50%;
    height: 30px;
    width: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 3px solid #e0e0e0;
}
.active{
    /* color: #ffffff;
    background-color: #349834; */
    border: 3px solid #349834;
    font-size: 1.1rem;
    }

.button{
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 20px;
}

.btn{
padding: 5px 20px;
width: 11rem;
height: 3rem;
border-radius: 24px;
border: 1px solid #0071cc;
background-color: #0071cc;
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
color: #ffffff;
font-size: 1.1rem;
font-weight: 900;
text-transform: uppercase;
margin: 5px;
&:hover{
cursor: pointer;
opacity: 0.9;
translate: initial;
border: 1px solid #0071cc;
}
&:focus{
    outline: none;
}
&:disabled{
    background-color: #999999;
    border: 1px solid #999999;
    cursor: not-allowed;
}
}

.active{
    transform: scale(0.98);
}

/* .btn.active{
    transform: scale(0.98);
} */

.progress{
    position: absolute;
    background-color: #349834;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    border-radius: 5px;
    width: 50%;
    height: 4px;
    z-index: -1;
    transition: 0.4s ease;
}
`