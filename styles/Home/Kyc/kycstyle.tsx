import styled from "styled-components";

export const SectionWrap = styled.section`
    display: flex;
    justify-content: center;
    margin: 5rem;
    text-align: center;
  

.progress-bar{
    overflow: hidden;
    border-radius: 1rem;
    padding: 3px;
    position: absolute; 
    top: 15rem;
    right: 43rem;
    align-items: center;
}

.progress-btn button{
    background-color: #084b83;
}

button{
width: 11rem;
height: 3rem;
border-radius: 24px;
border: 1px solid #0071cc;
background-color: #0071cc;
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
color: #ffffff;
font-size: 1.1rem;
font-weight: 900;
text-transform: uppercase;
margin-bottom: 7px;
&:hover{
cursor: pointer;
opacity: 0.9;
translate: initial;
border: 1px solid #0071cc;
}
&:last-child{
    margin-left: 1rem;
}
}
  
`
