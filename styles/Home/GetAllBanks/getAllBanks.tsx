import styled from "styled-components";

export const GetAllBanks = styled.div`
  .card {
    padding: 1rem 2rem;
    height: 12.5rem;
    background-color: rgba(157, 153, 153, 0.11);
    border-radius: 5px;
    box-sizing: border-box;
    border: 1px solid #ffffff;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);

    &:hover {
      cursor: pointer;
    }
  }

  .icon-wrap {
    width: 4.5rem;
    height: 4.5rem;
    margin: 1px;
    border-radius: 2rem;
    box-sizing: border-box;
    border: 1.3px solid #084b83;
    background-color: #ffffff;
    align-items: center;
    justify-content: center;
    display: flex;

    &:hover {
      opacity: 0.8;
      cursor: pointer;
    }
  }

  .card-text-lg {
    margin-top: 1.5rem;
    font-size: 1rem;
    color: #084b83;
    font-weight: 900;
    line-height: 5px;

  }

  .card-text-sm {
    margin-top: -1.5rem;
    font-size: 0.75rem;
  }

`