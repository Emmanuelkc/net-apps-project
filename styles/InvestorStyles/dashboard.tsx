import styled from "styled-components";

export const InvestorsStats = styled.div`
  .wallet-card {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    border-radius: 1.5rem;
  }

  .clipPath {
    border-radius: 1.5rem;
  }

  .wallet-card-btn {
    border: 1px solid #4baaf3;
    border-radius: 5rem;
    font-size: 12px;
    background-color: #4baaf3;
    padding: 0.6rem 1.4rem 0.6rem 1.5rem;
    margin-top: 2rem;
    color: #ffffff;
    font-weight: 900;
    text-align: center;
    cursor: pointer;
    &:hover {
      cursor: pointer;
      background-color: #ffffff;
      color: #0c2154;
      border: 1px solid #0c2154;
    }
  }

  .naira-circle {
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 2rem;
    border: 1px solid #0c2154;
    width: 2.5rem;
    height: 2.5rem;
    &:hover {
      opacity: 0.6;
      cursor: pointer;
      transition: 5ms;
    }
  }

  .check-icon {
    border-radius: 2rem;
    color: #2e681c;
    font-weight: 900;
    font-size: 1rem;
    padding: 2px;
  }

  .naira-icon {
    border-radius: 2rem;
    color: #0c2154;
    font-weight: 900;
    font-size: 2rem;
    padding: 1px;
  }
  .value {
    font-size: 1.2rem;
    font-weight: 600;
  }

  .cancel-icon {
    border-radius: 2rem;
    color: #ff0000;
    font-weight: 900;
    font-size: 1.8rem;
    padding: 2px;
  }

  .go-icon {
    border-radius: 2rem;
    color: #fbbc04;
    font-weight: 900;
    font-size: 1.8rem;
    padding: 2px;
  }
`;

export const InvestorTransactionTable = styled.div`
  background-color: rgba(184, 250, 250, 0.15);
  padding: 1rem;
  //margin: 1rem;
  border-radius: 4px;
  box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);

  .table-title-flex {
    display: grid;
    grid-template-columns: 4fr 1fr;
  }

  .table-head {
    font-size: 14px;
    color: #0c2154;
    margin: 1.5rem;
  }

  .update-icon {
    margin-bottom: -0.2rem;
  }

  .table-head-button {
    border: 1px solid #4baaf3;
    border-radius: 5rem;
    text-transform: capitalize;
    background-color: #4baaf3;
    padding: 0.6rem 1rem 0.6rem 1rem;
    margin: 1.5rem;
    color: #ffffff;
    font-weight: 600;
    text-align: center;
    cursor: pointer;

    &:hover {
      cursor: pointer;
      background-color: #ffffff;
      color: #0c2154;
      border: 1px solid #0c2154;
    }
  }

  .table {
    width: 100%;
    border-collapse: collapse;

    &:hover {
      cursor: pointer;
    }
  }

  .paid {
    color: #2e681c;
    font-weight: 600;
    font-size: 0.7rem;
    text-transform: lowercase;

    &:hover {
      cursor: pointer;
    }
  }

  .repay {
    text-transform: unset;
    font-size: 0.7rem;
    font-weight: 500;
    color: #ff0000;
    background-color: #bcc0c3;
    cursor: pointer;
    border-radius: 1rem;

    &:hover {
      //width:9rem;
    }
  }

  .table td,
  .table th {
    padding: 20px 20px;
    text-align: center;
    font-size: 12px;
    box-sizing: border-box;
  }

  .table th {
    text-transform: uppercase;
    font-size: 10px;
    color: #0c2154;
  }

  .table tr {
    color: #535b61;
    border-top: 1px solid #cdcdcd;
    border-bottom: 1px solid #cdcdcd;
  }

  .payment-icon {
    margin-bottom: -0.2rem;
    margin-left: 0.3rem;
  }

  .pagination {
    display: flex;
    flex-direction: row;
    justify-content: left;
    color: #535b61;
    list-style: none;
    margin-top: 2rem;

    &:hover {
      cursor: pointer;
    }
  }

  .previous-btn,
  .next-btn {
    border: none;
    border-radius: 2rem;
    background-color: #81d7e6;
    padding: 0.6rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;
    margin-right: 2rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .page-number {
    border: none;
    border-radius: 1rem;
    background-color: #81d7e6;
    padding: 0.4rem 0.8rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;
    margin-right: 2rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .active {
    border: 1px solid #4baaf3;
    border-radius: 1rem;
    background-color: #4baaf3;
    padding: 0.4rem 0.8rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  /* MOBILE RESPONSIVE */
  @media (max-width: 500px) {
    .table thead {
      display: none;
    }

    .table-title-flex {
      display: block;
    }

    .table-head-button {
      margin-right: 6rem;
    }

    .table {
      margin-left: 1rem;
    }

    .table,
    .table tbody,
    .table tr,
    .table td {
      display: block;
      width: 97.5%;
      background: none;
      box-shadow: none;
      border: none;
    }

    .table tr {
      box-sizing: border-box;
      border: 1px solid #c5c5c5;
      border-bottom: none;
      padding: 1rem 0 1rem 0;
    }

    .table tr:last-child {
      border-bottom: 1px solid #c5c5c5;
    }

    .table td {
      text-align: right;
      padding-left: 50%;
      position: relative;
      font-size: 13px;
      width: 95%;
      line-height: 5px;
    }

    .table td::before {
      content: attr(data-label);
      position: absolute;
      left: 0;
      width: 50%;
      padding-left: 15px;
      font-size: 13px;
      font-weight: 600;
      text-align: left;
    }

    .pagination {
      margin-top: 3rem;
      margin-left: -1.5rem;
    }

    .previous-btn,
    .next-btn {
      padding: 0.5rem;
      color: #ffffff;
      font-size: 0.7rem;
      margin-right: 2rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }

    .page-number {
      border: 1px solid #535b61;
      border-radius: 1rem;
      background-color: #535b61;
      padding: 0.1rem 0.2rem;
      color: #ffffff;
      font-weight: 900;
      font-size: 0.8rem;
      margin-right: 1.5rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }

    .active {
      border: 1px solid #4baaf3;
      border-radius: 1rem;
      background-color: #4baaf3;
      padding: 0.1rem 0.2rem;
      color: #ffffff;
      font-weight: 900;
      font-size: 0.8rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }
  }
`;

export const TransactionModalWrap = styled.div`
  background-color: rgba(0, 0, 0, 0.19);
  width: 100%;
  height: 100%;
  margin: -0.01rem;
  z-index: 0;
  top: 50%;
  left: 50%;
  overflow-y: hidden;
  overflow-x: hidden;
  transform: translate(-50%, -50%);
  position: fixed;

  .modal {
    position: fixed;
    z-index: 1;
    background-color: #e7e9ed;
    padding: 30px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 6px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .loan-modal-description {
    margin-bottom: -0.5rem;
    margin-top: -0.6rem;
  }

  .loan-type {
    font-weight: 900;
    color: #000000;
    margin-right: 0.5rem;
    //padding-left: 8px;
  }

  label {
    text-align: left;
    font-size: 13px;
    color: #3f3e3e;
    font-weight: 900;
  }

  input {
    height: 3.5rem;
    width: 100%;
    background-color: rgba(255, 255, 255, 0.52);
    padding: 1.15rem 1rem;
    font-size: 1rem;
    border-top: 1px solid rgba(119, 119, 119, 0.45);
    border-bottom: 1px solid rgba(119, 119, 119, 0.45);
    border-left: none;
    border-right: none;
    font-weight: 400;
    appearance: none;
    border-radius: 0.25rem;
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.1) !important;
    margin-bottom: 2.4rem;

    &:focus {
      background-color: #ffffff;
      outline: #0d6efd;
    }
  }

  //input {
  //  width: 100%;
  //  height: 3rem;
  //  //border-radius: 5px;
  //  border: 1px solid #242323;
  //  margin-top: 0.7rem;
  //  margin-bottom: 1.5rem;
  //  padding-left: 0.7rem;
  //  font-size: 16px;
  //
  //  &:focus {
  //    border-top: 1px solid #0d6efd;
  //    border-bottom: 1px solid #0d6efd;
  //    background-color: #ffffff;
  //    outline: #0d6efd;
  //    padding-left: 0.7rem;
  //  }
  //}

  .button {
    margin-top: 0.3rem;
    justify-content: end;
    align-items: flex-end;
    display: flex;
  }

  .cancel {
    border: 1px solid #0071cc;
    border-radius: 1rem;
    background-color: #0071cc;
    padding: 0.5rem 1rem 0.5rem 1rem;
    color: #ffffff;
    font-weight: 600;
    text-align: center;
    margin-right: 0.8rem;
    cursor: pointer;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .submit {
    border: 1px solid #0071cc;
    border-radius: 1rem;
    background-color: #0071cc;
    padding: 0.5rem 1rem 0.5rem 1rem;
    color: #ffffff;
    font-weight: 600;
    text-align: center;
    cursor: pointer;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  /* RESPONSIVE */
  @media (max-width: 500px) {
    .modal {
      width: 90%;
    }
  }
`;
