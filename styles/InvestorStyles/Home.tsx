import styled from "styled-components";

export const LoanCategoriesWrap = styled.div`
  background: no-repeat padding-box #edeffc;
  //background: no-repeat padding-box #dcf9fc;
  justify-content: center;
  text-align: center;

  .card {
    padding: 0.6rem 1rem;
    height: 12.5rem;
    background-color: #ffffff;
    border-radius: 5px;
    box-sizing: border-box;
    border: 1px solid #ffffff;
    box-shadow: 2px 3px 18px #cbcef7cc;
    //box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);
    &:hover {
      cursor: pointer;
      background-color: #d5effb;
      border: none;
    }
  }

  .apply {
    font-size: 11px;
    border-radius: 5rem;
    font-weight: 600;
    background-color: #87d5fa;
    border: none;
    color: #222222;

    &:hover {
      cursor: pointer;
      background-color: #42b1e5;
      border: none;
    }
  }

  .icon-wrap {
    width: 2rem;
    height: 2rem;
    margin: 1px;
    border-radius: 2rem;
    box-sizing: border-box;
    border: 1.5px solid #084b83;
    background-color: #ffffff;
    align-items: center;
    justify-content: center;
    display: flex;

    &:hover {
      opacity: 0.8;
      cursor: pointer;
    }
  }

  .icon {
    width: 1.5rem;
    height: 1.5rem;
    color: #051827;
    padding: 0.1rem;
  }

  .card-text-lg {
    margin-top: 0.6rem;
    font-size: 0.8rem;
    color: #084b83;
    font-weight: 900;
    line-height: 3px;
  }

  .card-text-sm {
    font-size: 0.7rem;
    color: #222222;
  }
`;
export const ButtonsContainer = styled.div`
  display: flex;
  .Button{
    font-weight: bold;
    color: #ffff;
    background-color: rgba(255, 89, 89, 0.96);
    margin-left: 5px;
    cursor: pointer;
    border: none;
    font-size: 11px;
    border-radius: 3rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;

    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
    }

    //for Mobiles
    @media only screen and (max-width: 600px) {
      padding: 10px 25px;
    }
    //for Tablets and Medium Screens
    @media only screen and (min-width: 600px) {
      padding: 10px 20px;
    }
    //for laptops and desktops
    @media only screen and (min-width: 992px) {
      padding: 15px230px;
    
  }
`;

export const Button = styled.button`

`;
export const HeroSection1 = styled.div`
  margin-bottom: -1.2rem;
  box-sizing: border-box;
  display: flex;
  background-color: rgba(255, 255, 255, 0.98);
  //background-color: rgba(255, 255, 255, 0.31);
  justify-content: center;
  align-items: center;
  text-align: center;

  @media only screen and (max-width: 600px) {
    height: 60%;
  }
  @media only screen and (min-width: 600px) {
    height: 50%;
  }
  @media only screen and (min-width: 992px) {
    height: 80%;
  }
`;

export const HeroSection22 = styled.div`
  box-sizing: border-box;
  display: flex;
  //background: no-repeat padding-box #edeffc;
  //background-color: rgba(255, 255, 255, 0.31);
  //justify-content: center;
  //align-items: center;
  //text-align: center;

  @media only screen and (max-width: 600px) {
    height: 60%;
  }
  @media only screen and (min-width: 600px) {
    height: 50%;
  }
  @media only screen and (min-width: 992px) {
    height: 80%;
  }
`;

export const LoanStepsWrapper = styled.div`
  .step {
    font-size: 16px;
    display: inline-block;
    color: #171756;
    font-weight: 700;
  }
  .font-medium {
    color: #171756;
    font-size: 0.9rem;
  }
`;
