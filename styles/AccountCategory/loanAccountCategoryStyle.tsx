import styled from "styled-components";

export const LoanProductAccountWrap = styled.div`
   border-radius: 1.5rem;
  
  margin-bottom: -2rem;
  margin-top: -2rem;
  position: relative;
  box-shadow: 0 0 50px -35px rgba(0, 0, 0, 0.4);
  //box-shadow: 0 0 0 0.5px rgba(0, 0, 0, 0.1), 0 0 0 0 rgba(0, 0, 0, 0.1);
  background-color: #cfebfd;
  --bs-bg-opacity: 1;
  text-align: center;
  box-sizing: border-box;
  padding: 1rem 2rem;
  border: none;
  width:95%;
  margin-left: 2rem;
  //border: 1px solid #ffffff;

  .grid h3 {
    &:hover {
      cursor: pointer;
    }
  }

  .count {
    color: #0899da;
    font-size: calc(1.2rem + .1vw);
    font-weight: 600 !important;
    font-family: "Poppins", sans-serif;
    line-height: 1.2;
  }
`

export const Container = styled.div`
  display: flex;
  padding: 3rem;
  background-color: rgba(255, 255, 255, 0.31);

  .TC{
    font-size: 14px;
  }
 
  @media only screen and (max-width: 600px) {
    height: 60%;
  }
  @media only screen and (min-width: 600px) {
    height: 50%;
  }
  @media only screen and (min-width: 992px) {
    height: 80%;
  }
`;

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 1rem 4rem;
  
  .Button{
    font-weight: bold;
    color: #ffff;
    background-color: rgba(255, 89, 89, 0.96);
    margin-left: 5px;
    cursor: pointer;
    border: none;
    font-size: 11px;
    border-radius: 3rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;

    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
    }

    //for Mobiles
    @media only screen and (max-width: 600px) {
      padding: 10px 25px;
    }
    //for Tablets and Medium Screens
    @media only screen and (min-width: 600px) {
      padding: 10px 30px;
    }
    //for laptops and desktops
    @media only screen and (min-width: 992px) {
      padding: 10px 30px;
    }

  }
`;

export const Button = styled.button`
`;
