import styled from "styled-components";

export const FormProgressBar = styled.div`
  .step {
    color: white;
    width: 40px;
    height: 40px;
    font-size: 12px;
    background-color: #0899da;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .step.completed {
    background-color: indigo;
  }
`


export const ButtonsContainer = styled.div`
  display: flex;
`;
export const Button = styled.button`
  font-weight: bold;
  color: #ffff;
  background-color: rgba(255, 89, 89, 0.96);
  margin-left: 5px;
  cursor: pointer;
  border: none;
  font-size: 11px;
  border-radius: 3rem;
  background-position: right bottom;
  transition: all 0.5s ease-out;

  &:hover {
    border: 1px solid #0899da;
    background-position: left bottom;
    cursor: pointer;
  }

  //for Mobiles
  @media only screen and (max-width: 600px) {
    padding: 10px 10px;
  }
  //for Tablets and Medium Screens
  @media only screen and (min-width: 600px) {
    padding: 10px 10px;
  }
  //for laptops and desktops
  @media only screen and (min-width: 992px) {
    padding: 10px 10px;
  }
`;
