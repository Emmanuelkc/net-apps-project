import styled from "styled-components";

export const ProductDescWrap = styled.div`
  background-color: #ffffff;
  border-radius: 6px;
  padding: 0.1rem 3rem;
  margin: 2rem;
  box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);

  .product-top-flex {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .square-left,
  .square-right {
    background-color: #ffffff;
    border-radius: 6px;
    padding: 0.1rem 3rem;
    margin: 2rem;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .outter-wrap {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .description-title {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    background-color: #c5c5c5;
    padding: 0.1rem 1rem 0.1rem 1rem;
    margin: 1rem;
    border-radius: 4px;
    border: 1px solid #c5c5c5;
    /* box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1); */
  }

  .table {
    margin: 2rem;
    padding: 1rem;
    /* background-color: #ffffff; */
    border-radius: 4px;
    border: 1px solid #777777;
    /* box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1); */
    border-collapse: collapse;
  }

  .table td,
  .table th {
    padding: 20px 20px;
    text-align: left;
    font-size: 13px;
    box-sizing: border-box;
    border-right: 1px solid #777777;
  }

  .table td:last-child,
  .table th:last-child {
    border-right: none;
  }

  .table th {
    text-transform: uppercase;
    font-size: 12px;
    color: #000000;
    /* padding-top: 20px; */
    background-color: #a8a8a8;
  }

  .table tr:nth-child(even) {
    background-color: #c5c5c5;
  }

  .mid-row {
    width: 62vw;
  }

  .right-row {
    font-weight: 900;
    color: #424141;
  }
  .left-sidebar {
    background-color: #ffffff;
    padding: 1rem;
    width: 15rem;
    font-weight: 900;
    margin: 1rem;
    line-height: 6rem;
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .mid-bar {
    background-color: #ffffff;
    padding: 1rem;
    font-weight: 900;
    margin: 1rem;
    line-height: 6rem;
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .mid-bar p,
  .right-sidebar p {
    font-weight: 100;
  }

  .right-sidebar {
    background-color: #ffffff;
    padding: 1rem;
    width: 15rem;
    font-weight: 900;
    margin: 1rem;
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .description-content {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    background-color: #ffffff;
    padding: 1rem;
    margin: 1rem;
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }
  /* MOBILE RESPONSIVE */
  @media (max-width: 500px) {
    .table thead {
      display: none;
    }
    .table {
      margin-left: 0.8rem;
    }
    .table,
    .table tbody,
    .table tr,
    .table td {
      display: block;
      width: 97.5%;
      background: none;
      box-shadow: none;
      border: none;
      padding-bottom: 1rem;
    }

    .table tr {
      box-sizing: border-box;
      border: 1px solid #c5c5c5;
      border-bottom: none;
      padding: 1rem 0 1rem 0;
    }

    .table tr:last-child {
      border-bottom: 1px solid #c5c5c5;
    }
    /* .table td{
        text-align: right;
        padding-left: 50%;
        position: relative;
        font-size: 13px;
        width: 95%;
        line-height: 5px;
        
    } */

    .table td::before {
      content: attr(data-label);
      position: absolute;
      left: 0;
      width: 50%;
      padding-left: 15px;
      font-size: 13px;
      font-weight: 600;
      text-align: left;
    }
  }
`;
