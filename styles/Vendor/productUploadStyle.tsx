import styled from "styled-components";

export const ProductUploadWrap = styled.div`
  .title {
    background-color: #0899da;
    border: 1px solid #0899da;
    border-radius: 6px;
    padding: 1rem 3rem;
    margin: 2rem;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .image-uploaded {
    background-color: #eeedee;
    width: 10rem;
    height: 10rem;
    border-radius: 7rem;
    padding: 0.5rem;
  }

  .product-data-container {
    background-color: #f7fcfd;
    border-radius: 6px;
    padding: 3rem;
    margin: 2rem;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .product-data {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .avatar {
    display: flex;
    flex-direction: column;
  }

  .avatar-flex {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-bottom: 4rem;
  }
  .avatar-flex img {
    width: 10rem;
    height: 10rem;
    border-radius: 2.5rem;
  }

  .img-ctrl {
    font-size: 13px;
  }

  .avatar img {
    width: 10rem;
    height: 10rem;
    border-radius: 5rem;
  }

  label.form-label {
    font-size: 13px;
    font-weight: 500;
    color: #0c2f54;
  }

  input.input-field {
    width: 99%;
    height: 3rem;
    border: 1px solid #888888;
    border-top: none;
    border-right: none;
    border-left: none;
    border-radius: 0.3rem;
    margin-top: 0.1rem;
    margin-bottom: 2rem;
    padding-left: 0.7rem;
    font-size: 13px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.001),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
    &:focus {
      background-color: #ffffff;
      outline: none;
      padding-left: 0.7rem;
    }
  }

  .select-category {
    display: flex;
    font-size: 13px;
    font-weight: 500;
    /* color: #555555; */
    color: #0c2f54;
  }

  .category-options {
    width: 103%;
    height: 3rem;
    background-color: #ffffff;
    border-bottom: 1px solid #888888;
    border-top: none;
    border-right: none;
    border-left: none;
    border-radius: 0.3rem;
    margin-top: 0.1rem;
    margin-bottom: 2rem;
    padding-left: 0.4rem;
    font-size: 13px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.001),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
    &:focus {
      background-color: #ffffff;
      outline: none;
      padding-left: 0.7rem;
    }
  }

  .select-brand {
    font-size: 13px;
  }

  .description-input {
    width: 99%;
    height: 5rem;
    background-color: #f3f1f1;
    border: 1px solid #888888;
    margin-top: 0.1rem;
    margin-bottom: 2rem;
    padding-left: 0.7rem;
    font-size: 13px;
    border-radius: 0.25rem;
    &:focus {
      background-color: #ffffff;
      outline: none;
      padding-left: 0.7rem;
    }
  }

  .btn {
    font-weight: bold;
    color: #ffffff;
    background-color: rgba(255, 89, 89, 0.96);
    border: 1px solid rgba(255, 89, 89, 0.96);
    width: 9.5rem;
    height: 2.5rem;
    border: none;
    font-size: 0.9rem;
    border-radius: 0.5rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
    }
  }

  .upload-btn {
    font-weight: bold;
    color: #ffffff;
    background-color: rgba(255, 89, 89, 0.96);
    border: 1px solid rgba(255, 89, 89, 0.96);
    width: 9.5rem;
    height: 2.5rem;
    margin: 2rem 0.5rem;
    border: none;
    font-size: 0.9rem;
    border-radius: 0.5rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
    }
  }
  .upload-btn-icon {
    margin-bottom: -0.1rem;
    margin-left: 0.5rem;
  }

  .submit-btn {
    display: flex;
    justify-content: right;
    align-items: right;
  }

  @media (max-width: 900px) {
    .product-data {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
    }
  }
`;
