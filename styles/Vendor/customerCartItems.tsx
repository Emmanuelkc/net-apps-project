import styled from "styled-components";

export const CartItemsWrap = styled.div`
  background-color: rgba(184, 250, 250, 0.15);
  padding: 1rem;
  //margin: 1rem;
  border-radius: 4px;
  box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);

  button {
    font-weight: bold;
    color: #ffff;
    background-color: rgba(255, 89, 89, 0.96);
    /* background-color: #4f46e5; */
    width: 6.3rem;
    height: 2rem;
    border: none;
    font-size: 0.8rem;
    text-transform: lowercase;
    border-radius: 2rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      background-color: #ffffff;
      border: 1px solid #0c2154;
      color: #0c2154;
      background-position: left bottom;
      cursor: pointer;
    }
  }

  /* .naira-icon {
    margin-top: -1rem;
    font-size: 0.9rem;
  } */

  .read-icon {
    margin-bottom: -0.2rem;
    margin-left: 0.3rem;
    font-size: 0.9rem;
  }

  .status-icon {
    margin-bottom: -0.1rem;
    margin-left: 0.3rem;
  }

  .table-head {
    font-size: 14px;
    color: #0c2154;
    margin: 1.5rem;
  }

  .update-icon {
    margin-bottom: -0.2rem;
  }

  .table {
    width: 100%;
    background-color: #f7fcfd;
    border-collapse: collapse;
    &:hover {
      cursor: pointer;
    }
  }

  .approved {
    color: #2e681c;
    font-weight: 600;
    font-size: 0.7rem;
    text-transform: lowercase;
    &:hover {
      cursor: pointer;
    }
  }

  .pending {
    text-transform: lowercase;
    font-size: 0.7rem;
    font-weight: 600;
    color: #ff0000;
    background-color: #bcc0c3;
    border: 1px solid #4f46e5;
    height: 2.5rem;
    width: 5.5rem;
    cursor: pointer;
    border-radius: 1rem;
    &:hover {
      background-color: #4f46e5;
      border: 1px solid #4f46e5;
      color: #ffffff;
    }
  }

  .table td,
  .table th {
    padding: 20px 20px;
    text-align: center;
    font-size: 12px;
    box-sizing: border-box;
  }

  .table th {
    text-transform: uppercase;
    font-size: 10px;
    color: #0c2154;
  }

  .table tr {
    color: #535b61;
    border-top: 1px solid #cdcdcd;
    border-bottom: 1px solid #cdcdcd;
  }

  .pagination {
    display: flex;
    flex-direction: row;
    justify-content: left;
    color: #535b61;
    list-style: none;
    margin-top: 2rem;

    &:hover {
      cursor: pointer;
    }
  }

  .previous-btn,
  .next-btn {
    border: none;
    border-radius: 2rem;
    background-color: #81d7e6;
    padding: 0.6rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;
    margin-right: 2rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .page-number {
    border: none;
    border-radius: 1rem;
    background-color: #81d7e6;
    padding: 0.4rem 0.8rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;
    margin-right: 2rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .active {
    border: 1px solid #4baaf3;
    border-radius: 1rem;
    background-color: #4baaf3;
    padding: 0.4rem 0.8rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  /* MOBILE RESPONSIVE */
  @media (max-width: 500px) {
    .table thead {
      display: none;
    }

    .table-title-flex {
      display: block;
    }

    .table-head-button {
      margin-right: 10rem;
    }

    .table {
      margin-left: 1rem;
    }

    .table,
    .table tbody,
    .table tr,
    .table td {
      display: block;
      width: 97.5%;
      background: none;
      box-shadow: none;
      border: none;
    }

    .table tr {
      box-sizing: border-box;
      border: 1px solid #c5c5c5;
      border-bottom: none;
      padding: 1rem 0 1rem 0;
    }

    .table tr:last-child {
      border-bottom: 1px solid #c5c5c5;
    }
    .table td {
      text-align: right;
      padding-left: 50%;
      position: relative;
      font-size: 13px;
      width: 95%;
      line-height: 5px;
    }

    .table td::before {
      content: attr(data-label);
      position: absolute;
      left: 0;
      width: 50%;
      padding-left: 15px;
      font-size: 13px;
      font-weight: 600;
      text-align: left;
    }

    .pagination {
      margin-top: 3rem;
      margin-left: -1.5rem;
    }

    .previous-btn,
    .next-btn {
      padding: 0.5rem;
      color: #ffffff;
      font-size: 0.7rem;
      margin-right: 2rem;
      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }

    .page-number {
      border: 1px solid #535b61;
      border-radius: 1rem;
      background-color: #535b61;
      padding: 0.1rem 0.2rem;
      color: #ffffff;
      font-weight: 900;
      font-size: 0.8rem;
      margin-right: 1.5rem;
      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }

    .active {
      border: 1px solid #4baaf3;
      border-radius: 1rem;
      background-color: #4baaf3;
      padding: 0.1rem 0.2rem;
      color: #ffffff;
      font-weight: 900;
      font-size: 0.8rem;
      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }
  }
`;
