import styled from "styled-components";

export const KYCFormWrap = styled.div`
  .grid-card {
    display: flex;
    flex-wrap: wrap;
  }

  .terms-section h4 {
    color: #0c2154;
  }

  input[type="checkbox"] {
    width: 1.4em;
    height: 1.4em;
    border: 0.15em solid currentColor;
    border-radius: 0.15em;
    transform: translateY(-0.075em);
    font-family: "Poppins", sans-serif;
  }

  .checkbox-label {
    padding-left: 10px;
    font-size: 11px;
    line-height: 2rem;
    text-align: left;
  }

  .right-flex-card {
    margin-top: 1.5rem;
    height: 57vh;
    width: 100%;
    background-color: #ffffff;
    border-radius: 0.5rem;
    border: 1px solid #ffffff;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .right-flex label {
    text-align: left;
    font-size: 13px;
    padding-left: 5px;
  }

  .right-flex input {
    height: 3.5rem;
    width: 100%;
    background-color: rgba(233, 236, 239, 0.37);
    padding: 1.15rem 1rem;
    font-size: 1rem;
    border-top: 1px solid rgba(119, 119, 119, 0.45);
    border-bottom: 1px solid rgba(119, 119, 119, 0.45);
    border-left: none;
    border-right: none;
    font-weight: 400;
    appearance: none;
    border-radius: 0.25rem;
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.1) !important;
    margin-bottom: 2.4rem;

    &:focus {
      background-color: #ffffff;
      outline: #0d6efd;
    }
  }

  Button {
    padding: 0.7rem 3rem 0.6rem 3rem;
  }

  .naira {
    font-size: 1.4rem;
    margin-top: -0.2rem;
  }
`;

export const KYCFileUploadWrap = styled.div`
  /* .image-uploaded {
    display: flex;
    align-items: center;
    background-color: #eeedee;
    width: 20rem;
    height: 12rem;
    border: 1px solid #c5c5c5;
    padding: 0.5rem;
  }
 */
  .product-data-container {
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 6px;
    border: 1px dashed gray;
    background-color: #f3f1f1;
    padding: 2rem;
    margin: 2rem;
  }

  .avatar-flex {
    /* display: flex;
    flex-direction: row;
    justify-content: space-between; */
    margin-bottom: 4rem;
  }
  .avatar-flex img {
    width: 10rem;
    height: 10rem;
  }

  .img-ctrl {
    font-size: 13px;
  }

  .avatar img {
    /* width: 10rem;
    height: 10rem;
    border-radius: 5rem;
    border: 1px solid gray; */
  }

  label.form-label {
    font-size: 13px;
    font-weight: 500;
    color: #0c2f54;
  }

  input.input-field {
    width: 99%;
    height: 3rem;
    border: 1px solid #888888;
    border-top: none;
    border-right: none;
    border-left: none;
    border-radius: 0.3rem;
    margin-top: 0.1rem;
    margin-bottom: 2rem;
    padding-left: 0.7rem;
    font-size: 13px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.001),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
    &:focus {
      background-color: #ffffff;
      outline: none;
      padding-left: 0.7rem;
    }
  }

  .upload-btn {
    font-weight: bold;
    color: #ffffff;
    background-color: #0899da;
    border: 1px solid #0899da;
    width: 9.5rem;
    height: 2.5rem;
    margin: 3rem 0rem 0rem 0rem;
    border: none;
    font-size: 0.9rem;
    border-radius: 1.5rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
      opacity: 0.8;
    }
  }
  .upload-btn-icon {
    margin-bottom: -0.1rem;
    margin-left: 0.5rem;
  }

  .submit-btn {
    display: flex;
    justify-content: right;
    align-items: right;
  }

  @media (max-width: 900px) {
    .product-data {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
    }
  }
`;
