import styled from "styled-components";

export const VendorHeroWrap = styled.div`
  .vendor-items {
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .upload-prdt {
    align-items: center;
    text-align: center;
    justify-content: center;
    font-size: 3rem;
    /* text-transform: uppercase; */
    font-weight: 900;
    color: #0c2154;
    border-left: 3px solid rgba(255, 89, 89, 0.96);
    padding-left: 1rem;
    &:hover {
      cursor: pointer;
    }
  }

  .b-home-menu {
    font-size: 2rem;
    margin-right: 0.5rem;
    color: #084b83;
  }

  .table-head {
    font-family: Arial, Helvetica, sans-serif;
    text-transform: uppercase;
    font-size: 18px;
    color: #000000;
    margin: 1.5rem;
  }

  button {
    font-weight: bold;
    color: #ffff;
    background-color: rgba(255, 89, 89, 0.96);
    /* background-color: #4f46e5; */
    width: 10.5rem;
    height: 2.5rem;
    border: none;
    font-size: 0.9rem;
    border-radius: 0.5rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
    }
  }

  .upload-icon {
    margin-bottom: 0.3rem;
    font-size: 1.2rem;
    font-weight: 900;
    margin-left: 0.5rem;
    &:hover {
      cursor: pointer;
    }
  }

  /* RESPONSIVENESS */
  @media (max-width: 900px) {
    .upload-prdt {
      font-size: 2rem;
      border-left: none;
    }
  }

  @media (max-width: 500px) {
    .upload-prdt {
      font-size: 1.5rem;
    }
  }
`;

export const VendorStatWrap = styled.div`
  .card-title {
    font-size: small;
    opacity: 0.6;
    font-weight: 600;
  }

  .card {
    padding: 1.5rem;
    border-radius: 1rem;
    border-bottom: 2px solid #0899da;
    &:hover {
      opacity: 0.8;
      cursor: pointer;
      transition: 5ms;
    }
  }

  .card-last {
    padding: 1.5rem;
    border-radius: 1rem;
    border-bottom: 2px solid rgba(255, 89, 89, 0.96);
    &:hover {
      opacity: 0.8;
      cursor: pointer;
      transition: 5ms;
    }
  }

  .card-flex {
    display: flex;
  }

  .wallet-card {
    padding: 0;
    margin: 0;

    box-sizing: border-box;
    border-radius: 1.5rem;
  }
  .naira-circle {
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 0.5rem;
    background-color: #d0e1fd;
    border: 1px solid #d0e1fd;
    /* border: 1px solid #0c2154; */
    width: 2.8rem;
    height: 2.8rem;
    &:hover {
      opacity: 0.6;
      cursor: pointer;
      transition: 5ms;
    }
  }

  .check-icon {
    border-radius: 2rem;
    color: #2e681c;
    font-weight: 900;
    font-size: 1.3rem;
    padding: 2px;
  }

  .naira-icon {
    border-radius: 2rem;
    color: #0c2154;
    font-weight: 900;
    font-size: 1.7rem;
    padding: 2px;
  }

  .total-icon {
    border-radius: 2rem;
    color: #ff0000;
    font-weight: 900;
    font-size: 1.3rem;
    padding: 0px;
  }

  .debt-icon {
    border-radius: 2rem;
    color: #fbbc04;
    font-weight: 900;
    font-size: 1.3rem;
    padding: 0px;
  }

  .link-text {
    padding-left: 0.7rem;
    font-size: 0.8rem;
    &:hover {
      opacity: 0.7;
      cursor: pointer;
    }
  }
`;

export const CustomerLogBtn = styled.div`
  margin: 1rem;
  button {
    font-weight: bold;
    color: #ffff;
    background-color: rgba(255, 89, 89, 0.96);
    /* background-color: #4f46e5; */
    width: 9.5rem;
    height: 2.5rem;
    border: none;
    font-size: 0.9rem;
    border-radius: 0.5rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
    }
  }

  .log {
    margin-bottom: -0.2rem;
    margin-right: 0.5rem;
  }
`;

export const ProductTableWrap = styled.div`
  background-color: rgba(184, 250, 250, 0.15);
  padding: 1rem;
  //margin: 1rem;
  border-radius: 4px;
  box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);

  .table-title-flex {
    display: grid;
    grid-template-columns: 4fr 1fr;
  }

  .table-head {
    font-size: 14px;
    color: #0c2154;
    margin: 1.5rem;
  }

  .update-icon {
    margin-bottom: -0.2rem;
  }

  .table-head-button {
    border: 1px solid #4baaf3;
    border-radius: 5rem;
    text-transform: capitalize;
    background-color: #4baaf3;
    padding: 0.6rem 1rem 0.6rem 1rem;
    margin: 1.5rem;
    color: #ffffff;
    font-weight: 600;
    text-align: center;
    cursor: pointer;

    &:hover {
      cursor: pointer;
      background-color: #ffffff;
      color: #0c2154;
      border: 1px solid #0c2154;
    }
  }

  .table {
    width: 100%;
    border-collapse: collapse;

    &:hover {
      cursor: pointer;
    }
  }

  button {
    font-weight: bold;
    color: #ffff;
    background-color: rgba(255, 89, 89, 0.96);
    /* background-color: #4f46e5; */
    width: 6.3rem;
    height: 2rem;
    border: none;
    font-size: 0.8rem;
    text-transform: lowercase;
    border-radius: 2rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      background-color: #ffffff;
      border: 1px solid #0c2154;
      color: #0c2154;
      background-position: left bottom;
      cursor: pointer;
    }
  }

  .read-icon {
    margin-bottom: -0.2rem;
    margin-left: 0.3rem;
    font-size: 0.9rem;
  }

  button.pending {
    text-transform: lowercase;
    font-size: 0.7rem;
    font-weight: 600;
    color: #ff0000;
    background-color: #bcc0c3;
    border: 1px solid #4f46e5;
    height: 2.5rem;
    width: 5.5rem;
    cursor: pointer;
    border-radius: 1rem;
    &:hover {
      background-color: #4f46e5;
      border: 1px solid #4f46e5;
      color: #ffffff;
    }
  }

  .approved {
    color: #2e681c;
    font-weight: 600;
    font-size: 0.7rem;
    text-transform: lowercase;
    &:hover {
      cursor: pointer;
    }
  }

  .status-icon {
    margin-bottom: -0.1rem;
    margin-left: 0.3rem;
  }
  .repay {
    text-transform: unset;
    font-size: 0.7rem;
    font-weight: 500;
    color: #ff0000;
    background-color: #bcc0c3;
    cursor: pointer;
    border-radius: 1rem;

    &:hover {
      //width:9rem;
    }
  }

  .table td,
  .table th {
    padding: 20px 20px;
    text-align: center;
    font-size: 12px;
    box-sizing: border-box;
  }

  .table th {
    text-transform: uppercase;
    font-size: 10px;
    color: #0c2154;
  }

  .table tr {
    color: #535b61;
    border-top: 1px solid #cdcdcd;
    border-bottom: 1px solid #cdcdcd;
  }

  .payment-icon {
    margin-bottom: -0.2rem;
    margin-left: 0.3rem;
  }

  .pagination {
    display: flex;
    flex-direction: row;
    justify-content: left;
    color: #535b61;
    list-style: none;
    margin-top: 2rem;

    &:hover {
      cursor: pointer;
    }
  }

  .previous-btn,
  .next-btn {
    border: none;
    border-radius: 2rem;
    background-color: #81d7e6;
    padding: 0.6rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;
    margin-right: 2rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .page-number {
    border: none;
    border-radius: 1rem;
    background-color: #81d7e6;
    padding: 0.4rem 0.8rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;
    margin-right: 2rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .active {
    border: 1px solid #4baaf3;
    border-radius: 1rem;
    background-color: #4baaf3;
    padding: 0.4rem 0.8rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  /* MOBILE RESPONSIVE */
  @media (max-width: 500px) {
    .table thead {
      display: none;
    }

    .table-title-flex {
      display: block;
    }

    .table-head-button {
      margin-right: 6rem;
    }

    .table {
      margin-left: 1rem;
    }

    .table,
    .table tbody,
    .table tr,
    .table td {
      display: block;
      width: 97.5%;
      background: none;
      box-shadow: none;
      border: none;
    }

    .table tr {
      box-sizing: border-box;
      border: 1px solid #c5c5c5;
      border-bottom: none;
      padding: 1rem 0 1rem 0;
    }

    .table tr:last-child {
      border-bottom: 1px solid #c5c5c5;
    }

    .table td {
      text-align: right;
      padding-left: 50%;
      position: relative;
      font-size: 13px;
      width: 95%;
      line-height: 5px;
    }

    .table td::before {
      content: attr(data-label);
      position: absolute;
      left: 0;
      width: 50%;
      padding-left: 15px;
      font-size: 13px;
      font-weight: 600;
      text-align: left;
    }

    .pagination {
      margin-top: 3rem;
      margin-left: -1.5rem;
    }

    .previous-btn,
    .next-btn {
      padding: 0.5rem;
      color: #ffffff;
      font-size: 0.7rem;
      margin-right: 2rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }

    .page-number {
      border: 1px solid #535b61;
      border-radius: 1rem;
      background-color: #535b61;
      padding: 0.1rem 0.2rem;
      color: #ffffff;
      font-weight: 900;
      font-size: 0.8rem;
      margin-right: 1.5rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }

    .active {
      border: 1px solid #4baaf3;
      border-radius: 1rem;
      background-color: #4baaf3;
      padding: 0.1rem 0.2rem;
      color: #ffffff;
      font-weight: 900;
      font-size: 0.8rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }
  }
  //   background-color: #ffffff;
  //   padding: 1rem 0 1rem 0;
  //   margin: 1rem;
  //   border-radius: 4px;
  //   box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);

  //   .table-title-flex {
  //     display: grid;
  //     grid-template-columns: 4fr 1fr;
  //   }

  //   .table-head {
  //     font-family: Arial, Helvetica, sans-serif;
  //     text-transform: uppercase;
  //     font-size: 18px;
  //     color: #0c2154;
  //     margin: 1.5rem;
  //   }

  //   .update-icon {
  //     margin-bottom: -0.2rem;
  //   }

  //   thead {
  //     background-color: #4baaf3;
  //   }

  //   .table-head-button {
  //     border: 1px solid #4baaf3;
  //     border-radius: 5rem;
  //     text-transform: capitalize;
  //     background-color: #4baaf3;
  //     padding: 0.6rem 1rem 0.6rem 1rem;
  //     margin: 1.5rem;
  //     color: #ffffff;
  //     font-weight: 600;
  //     text-align: center;
  //     cursor: pointer;
  //     &:hover {
  //       cursor: pointer;
  //       background-color: #ffffff;
  //       color: #0c2154;
  //       border: 1px solid #0c2154;
  //     }
  //   }

  //   .table {
  //     width: 100%;
  //     background-color: #ffffff;
  //     border-collapse: collapse;
  //     &:hover {
  //       cursor: pointer;
  //     }
  //   }

  //   .paid {
  //     color: #2e681c;
  //     font-weight: 600;
  //     font-size: 0.9rem;
  //     text-transform: lowercase;
  //     &:hover {
  //       cursor: pointer;
  //     }
  //   }

  //   .repay {
  //     text-transform: unset;
  //     font-size: 0.8rem;
  //     font-weight: 500;
  //     color: #ff0000;

  //     &:hover {
  //       cursor: pointer;
  //       border: 1px solid #0c2f54;
  //       border-radius: 1rem;
  //       padding: 0.5rem;
  //       width: 9rem;
  //     }
  //   }

  //   .table td,
  //   .table th {
  //     padding: 20px 20px;
  //     text-align: center;
  //     font-size: 13px;
  //     box-sizing: border-box;
  //   }

  //   .table th {
  //     text-transform: uppercase;
  //     font-size: 12px;
  //     color: #ffffff;
  //     /* padding-top: 20px; */
  //   }

  //   .table tr {
  //     color: #535b61;
  //     border-top: 1px solid #cdcdcd;
  //     border-bottom: 1px solid #cdcdcd;
  //   }

  //   .payment-icon {
  //     margin-bottom: -0.2rem;
  //     margin-left: 0.3rem;
  //   }

  //   .pagination {
  //     display: flex;
  //     flex-direction: row;
  //     justify-content: right;
  //     align-items: right;
  //     color: #ffffff;
  //     list-style: none;
  //     margin-top: 2rem;
  //     &:hover {
  //       cursor: pointer;
  //     }
  //   }

  //   .previous-btn,
  //   .next-btn {
  //     border: 1px solid #4baaf3;
  //     border-radius: 0.5rem;
  //     background-color: #4baaf3;
  //     padding: 0.6rem;
  //     color: #ffffff;
  //     font-weight: 900;
  //     font-size: 0.8rem;
  //     margin-right: 2rem;
  //     &:hover {
  //       cursor: pointer;
  //       opacity: 0.9;
  //     }
  //   }

  //   .page-number {
  //     border: 1px solid #9eb09f;
  //     border-radius: 0.3rem;
  //     background-color: #9eb09f;
  //     padding: 0.1rem 0.2rem;
  //     color: #0c2f54;
  //     font-weight: 900;
  //     font-size: 0.8rem;
  //     margin-right: 2rem;
  //     &:hover {
  //       cursor: pointer;
  //       opacity: 0.9;
  //     }
  //   }

  //   .active {
  //     border: 1px solid #4baaf3;
  //     border-radius: 0.3rem;
  //     background-color: #4baaf3;
  //     padding: 0.1rem 0.2rem;
  //     color: #ffffff;
  //     font-weight: 900;
  //     font-size: 0.8rem;
  //     &:hover {
  //       cursor: pointer;
  //     }
  //   }
  //   /* MOBILE RESPONSIVE */
  //   @media (max-width: 500px) {
  //     .table thead {
  //       display: none;
  //     }

  //     .table-title-flex {
  //       display: block;
  //     }

  //     .table-head-button {
  //       margin-right: 10rem;
  //     }

  //     .table {
  //       margin-left: 1rem;
  //     }

  //     .table,
  //     .table tbody,
  //     .table tr,
  //     .table td {
  //       display: block;
  //       width: 97.5%;
  //       background: none;
  //       box-shadow: none;
  //       border: none;
  //     }

  //     .table tr {
  //       box-sizing: border-box;
  //       border: 1px solid #c5c5c5;
  //       border-bottom: none;
  //       padding: 1rem 0 1rem 0;
  //     }

  //     .table tr:last-child {
  //       border-bottom: 1px solid #c5c5c5;
  //     }
  //     .table td {
  //       text-align: right;
  //       padding-left: 50%;
  //       position: relative;
  //       font-size: 13px;
  //       width: 95%;
  //       line-height: 5px;
  //     }

  //     .table td::before {
  //       content: attr(data-label);
  //       position: absolute;
  //       left: 0;
  //       width: 50%;
  //       padding-left: 15px;
  //       font-size: 13px;
  //       font-weight: 600;
  //       text-align: left;
  //     }

  //     .pagination {
  //       margin-top: 3rem;
  //       margin-left: -1.5rem;
  //     }

  //     .previous-btn,
  //     .next-btn {
  //       padding: 0.5rem;
  //       color: #ffffff;
  //       font-size: 0.7rem;
  //       margin-right: 2rem;
  //       &:hover {
  //         cursor: pointer;
  //         opacity: 0.9;
  //       }
  //     }

  //     .page-number {
  //       border: 1px solid #535b61;
  //       border-radius: 1rem;
  //       background-color: #535b61;
  //       padding: 0.1rem 0.2rem;
  //       color: #ffffff;
  //       font-weight: 900;
  //       font-size: 0.8rem;
  //       margin-right: 1.5rem;
  //       &:hover {
  //         cursor: pointer;
  //         opacity: 0.9;
  //       }
  //     }

  //     .active {
  //       border: 1px solid #4baaf3;
  //       border-radius: 1rem;
  //       background-color: #4baaf3;
  //       padding: 0.1rem 0.2rem;
  //       color: #ffffff;
  //       font-weight: 900;
  //       font-size: 0.8rem;
  //       &:hover {
  //         cursor: pointer;
  //         opacity: 0.9;
  //       }
  //     }
  //   }
`;
