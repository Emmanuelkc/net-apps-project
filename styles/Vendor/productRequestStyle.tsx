import styled from "styled-components";

export const ProductRequestWrap = styled.div`
  /* background: linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.8)),
    url(".../public/Assets/productRequest/background.jpg");
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
 */

  .customer-request-flex {
    background-color: #f7fcfd;
    border-radius: 6px;
    /* padding: 0.1rem 5rem; */
    margin: 1rem;
    /* padding-bottom: 0rem; */
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    padding-top: -15rem;
  }

  .flex-right {
    margin-top: 3rem;
    width: 45%;
  }

  .description {
    color: rgba(255, 89, 89, 0.96);
    font-weight: 600;
    font-size: 13px;
  }

  p {
    color: #444444;
    font-size: 12px;
  }

  .price-tag {
    display: flex;
    font-size: 1.5rem;
    margin-bottom: 0.5rem;
    font-weight: bold;
    padding: 0.2rem 0.4rem;
    width: 4.7rem;
    height: 1.6rem;
    border: 1px solid #999999;
    border-radius: 0.5rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      border: 1px solid #0c2f54;
      background-position: left bottom;
      cursor: pointer;
    }
  }

  .name-tag {
    font-size: 1.3rem;
    font-weight: 700;
    color: #0c2f54;
  }

  .naira-icon {
    margin-top: 0.1rem;
  }

  .price-tag p {
    font-size: 1.2rem;
  }

  hr {
    height: 0px;
    border: none;
    border-top: 1px solid #999999;
  }

  .sub-title {
    font-weight: 600;
    margin-bottom: 0.5rem;
  }

  .personal {
    color: #0c2154;
    font-weight: 600;
    font-size: 12px;
  }

  .personal-info-flex {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .name-title {
    font-weight: 600;
    margin-bottom: 0.5rem;
  }

  .email-title {
    font-weight: 600;
    margin-bottom: -0.5rem;
  }

  @media (max-width: 900px) {
    .customer-request-flex {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
    }

    .flex-right {
      margin-top: 1rem;
      margin-left: 2rem;
      width: 80%;
    }

    .personal-info-flex {
      display: block;
      flex-direction: column;
      justify-content: space-between;
    }
  }
`;
