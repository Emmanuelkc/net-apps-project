import styled from "styled-components";

export const CustomerLogWrap = styled.div`
  background-color: #f8f9fa;
  .log-hero {
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    background-color: rgba(184, 250, 250, 0.15);
    padding: 0.1rem 1rem 0.1rem 1rem;
    margin: 1rem;
    border-radius: 4px;
    border: 1px solid rgba(184, 250, 250, 0.15);
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .flex-left {
    width: 45vw;
    margin-top: 8rem;
  }

  .fragmen {
    display: grid;
    grid-template-columns: 4fr 4fr 4fr;
  }

  .text-large {
    color: #0071cc;
    font-weight: 900;
    font-size: 2rem;
    line-height: 2.5rem;
  }

  .text-small {
    width: 40vw;
    color: #777777;
    font-size: 1rem;
    line-height: 1.5rem;
  }

  .categories {
    /* background-color: #ffffff; */
    padding: 1rem;
    margin: 1rem;
    border-radius: 4px;
    /* box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1); */
  }

  .category-title p {
    color: #0c2f54;
    font-weight: 700;
    font-size: 1.3rem;
    line-height: 2.5rem;
    text-align: center;
    margin-bottom: 3rem;
    margin-top: 2rem;
    /* width: 20rem; */
  }

  .category-flex {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    text-align: center;
  }

  .category-flex div {
    &:hover {
      cursor: pointer;
      opacity: 0.6;
    }
  }

  .sub-title {
    /* width: 40vw; */
    color: #0c2f54;
    font-weight: 700;
    font-size: 0.9rem;
    line-height: 1rem;
    margin-top: 1rem;
  }

  .description {
    /* width: 25vw; */
    padding: 0rem 3rem;
    color: #000000;
    font-size: 0.8rem;
    line-height: 1rem;
    margin-top: -0.5rem;
  }

  /* RESPONSIVENESS */
  @media (max-width: 900px) {
    .log-hero {
      flex-direction: column;
    }

    .flex-left {
      width: 85vw;
      margin-top: 2rem;
    }

    .text-small {
      width: 85vw;
      font-size: 0.9rem;
    }

    .category-flex {
      flex-direction: column;
    }

    .description {
      margin-bottom: 3rem;
    }
  }
`;

export const LogFormWrap = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  margin-top: 3rem;
  /* display: flex;
  flex-direction: row;
  justify-content: space-evenly; */

  .outter-wrap {
    /* display: flex;
    flex-direction: row;
    justify-content: space-evenly; */
    background-color: #ffffff;
    padding: 1rem;
    margin: 1rem;
    border-radius: 4px;
    border: 1px solid rgba(184, 250, 250, 0.15);
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .image {
    background-color: #ffffff;
    padding: 1rem;
    margin: 1rem;
    border-radius: 4px;
    border: 1px solid rgba(184, 250, 250, 0.15);
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1),
      0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  p {
    color: #000000;
  }

  .about {
    display: grid;
    grid-template-columns: 9fr 1fr;
  }

  button {
    font-weight: bold;
    color: #000000;
    /* background-color: rgba(255, 89, 89, 0.96); */
    background-color: #0899da;
    /* width: 4rem;
    height: 1.7rem; */
    width: inherit;
    height: inherit;
    border: none;
    font-size: 0.9rem;
    border-radius: 1rem;
    background-position: right bottom;
    transition: all 0.5s ease-out;
    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
    }
  }

  .info-section {
    display: grid;
    grid-template-columns: 2fr 2fr 2fr;
  }

  .payment-section {
    display: grid;
    grid-template-columns: 2fr 2fr 2fr;
    margin-top: -2.5rem;
  }

  .location-section {
    display: grid;
    grid-template-columns: 2fr 2fr 2fr;
    margin-top: -0.5rem;
  }

  .title {
    color: #777777;
    font-size: 0.65rem;
  }

  .data-info {
    font-weight: 500;
  }

  .customer-info {
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    color: #000000;
  }

  .customer-email {
    margin-top: -2.7rem;
  }

  /* .customer-phone {
    margin-top: -2rem;
  } */
  /* 
  .customer-name {
    margin-top: 2rem;
  }

  .customer-email {
    margin-top: -5.5rem;
  }

  .customer-phone {
    margin-top: -10.5rem;
  } */
`;
