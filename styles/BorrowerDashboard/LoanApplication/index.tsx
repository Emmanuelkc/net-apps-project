import styled from "styled-components";

export const LoanTermFormWrap = styled.div`

  .grid-card {
    display: flex;
    flex-wrap: wrap;
  }

  .terms-section h4 {
    color: #0c2154;
  }

  input[type="checkbox"] {
    width: 1.4em;
    height: 1.4em;
    border: 0.15em solid currentColor;
    border-radius: 0.15em;
    transform: translateY(-0.075em);
    font-family: "Poppins", sans-serif;
  }

  .checkbox-label {
    padding-left: 10px;
    font-size: 12px;
    line-height: 2rem;
    text-align: left;
  }

  .right-flex-card {
    margin-top: 1.5rem;
    height: 57vh;
    width: 100%;
    background-color: #ffffff;
    border-radius: 0.5rem;
    border: 1px solid #ffffff;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .right-flex label {
    text-align: left;
    font-size: 13px;
    padding-left: 5px;
  }


  .right-flex input {
    height: 3.5rem;
    width: 100%;
    background-color: rgba(233, 236, 239, 0.37);
    padding: 1.15rem 1rem;
    font-size: 1rem;
    border-top: 1px solid rgba(119, 119, 119, 0.45);
    border-bottom: 1px solid rgba(119, 119, 119, 0.45);
    border-left: none;
    border-right: none;
    font-weight: 400;
    appearance: none;
    border-radius: .25rem;
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.1) !important;
    margin-bottom: 2.4rem;

    &:focus {
      background-color: #ffffff;
      outline: #0d6efd;
    }
  }

  Button {
    padding: 0.7rem 3rem 0.6rem 3rem;
  }

  .naira {
    font-size: 1.4rem;
    margin-top: -0.2rem;
  }
`

