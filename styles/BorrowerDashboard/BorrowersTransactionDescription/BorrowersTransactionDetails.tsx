import styled from "styled-components";

export const ProductDescWrap = styled.div`
  background-color: rgba(96, 239, 239, 0.15);
  padding: 1rem;
  margin: 1rem;
  border-radius: 4px;
  box-shadow: 0 0 50px -35px rgba(0, 0, 0, 0.6);
  //box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);

  .outter-wrap {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .description-title {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 0.1rem 1rem 0.1rem 1rem;
    margin: 1rem;
    border-radius: 4px;
    border: 1px solid #c5c5c5;
  }

  .table {
    margin: 2rem;
    padding: 1rem;
    border-radius: 4px;
    border-collapse: collapse;
  }

  .table td, .table th {
    padding: 20px 10px;
    text-align: left;
    font-size: 12px;
    box-sizing: border-box;
  }

  td {
    border-bottom: 1px solid rgba(216, 216, 216, 0.98);
  }

  .table td:last-child, .table th:last-child {
    border-right: none;
  }


  .table th {
    //text-transform: uppercase;
    font-size: 12px;
    color: #000000;
    /* padding-top: 20px; */
    background-color: rgba(168, 168, 168, 0.3);
  }

  .table tr:nth-child(even) {
    //background-color: #c5c5c5;

  }

  .mid-row {
    width: 27vw;
    color: #222222;
    
  }

  .right-row {
    width: 27vw;
    font-weight: 900;
    color: #424141;
  }

  .paid {
    color: #2e681c;
    font-weight: 600;
    font-size: 0.7rem;
    text-transform: lowercase;

    &:hover {
      cursor: pointer;
    }
  }

  .repay {
    text-transform: unset;
    font-size: 0.7rem;
    font-weight: 500;
    color: #ff0000;

    &:hover {
      cursor: pointer;
      border: 1px solid #0c2f54;
      border-radius: 1rem;
      padding: 0.5rem;
      width: 9rem;
    }
  }

  .left-sidebar {
    background-color: #ffffff;
    padding: 1rem;
    width: 15rem;
    font-weight: 900;
    margin: 1rem;
    line-height: 6rem;
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .mid-bar {
    background-color: #ffffff;
    padding: 1rem;
    font-weight: 900;
    margin: 1rem;
    line-height: 6rem;
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .mid-bar p, .right-sidebar p {
    font-weight: 100;

  }

  .right-sidebar {
    background-color: #ffffff;
    padding: 1rem;
    width: 15rem;
    font-weight: 900;
    margin: 1rem;
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);
  }

  .description-content {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    background-color: #ffffff;
    padding: 1rem;
    margin: 1rem;
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);

  }

  /* MOBILE RESPONSIVE */
  @media (max-width: 500px) {

    .table thead {
      display: none;
    }

    .table {
      margin-left: 0.8rem;
    }

    .table, .table tbody, .table tr, .table td {
      display: block;
      width: 97.5%;
      background: none;
      box-shadow: none;
      border: none;
      padding-bottom: 1rem;
    }

    .table tr {
      box-sizing: border-box;
      border: 1px solid #c5c5c5;
      border-bottom: none;
      padding: 1rem 0 1rem 0;

    }

    .table tr:last-child {
      border-bottom: 1px solid #c5c5c5;
    }

    /* .table td{
        text-align: right;
        padding-left: 50%;
        position: relative;
        font-size: 13px;
        width: 95%;
        line-height: 5px;
        
    } */
    .table td::before {
      content: attr(data-label);
      position: absolute;
      left: 0;
      width: 50%;
      padding-left: 15px;
      font-size: 13px;
      font-weight: 600;
      text-align: left;
    }
  }
`