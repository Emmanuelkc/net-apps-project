import styled from "styled-components";

export const ModalWrap = styled.div`
  background-color: rgba(0, 0, 0, 0.6);
  width: 100%;
  height: 100%;
  margin: -0.01rem;
  z-index: 0;
  top: 50%;
  left: 50%;
  overflow-y: hidden;
  overflow-x: hidden;
  transform: translate(-50%, -50%);
  position: fixed;
.modal{
  position: fixed; 
  z-index: 1;
  background-color: #e7e9ed;
  padding: 30px;
  width: 30%;
  max-width: 100%;
  height: 30%;
  max-height: 100%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
    border-radius: 4px;
    box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1);
}

.loan-modal-description{
  display: flex;
  margin-bottom: -0.5rem;
  margin-top: -0.6rem;
}
.loan-type{
  font-weight: 900;
  color: #000000;
  margin-right: 0.5rem;
  padding-left: 8px;
}

label{
  text-align: left;
  font-size: 13px;
  padding-left: 8px;
  color: #3f3e3e;
  font-weight: 900;
}

input{
width: 99%;
height: 3rem;
border-radius: 5px;
border: 1px solid #242323;
margin-top: 0.7rem;
margin-bottom: 1.5rem;
padding-left: 0.7rem;
font-size: 16px;
&:focus{
      border-top: 1px solid #0d6efd;
      border-bottom:1px  solid  #0d6efd;
      background-color: #ffffff;
      outline: #0d6efd;
      padding-left: 0.7rem;
}
}

.button{
  margin-top: 0.3rem;
  justify-content: end;
  align-items: flex-end;
  display: flex;
}

.cancel{
    border: 1px solid #0071cc;
border-radius: 1rem;
background-color: #0071cc;
padding: 0.5rem 1rem 0.5rem 1rem;
color: #ffffff;
font-weight: 600;
text-align: center;
margin-right: 0.8rem;
  cursor: pointer;
&:hover{
    cursor: pointer;
    opacity: 0.9;
}
}

.submit{
    border: 1px solid #0071cc;
border-radius: 1rem;
background-color: #0071cc;
padding: 0.5rem 1rem 0.5rem 1rem;
color: #ffffff;
font-weight: 600;
text-align: center;
  cursor: pointer;
&:hover{
    cursor: pointer;
    opacity: 0.9;
}
}

/* RESPONSIVE */
@media (max-width: 500px){
  .modal{
  width: 90%;
} 
}
`