import styled from "styled-components";

export const Headers = styled.div`
  padding: 0;
  margin: 0;
  box-sizing: border-box;
  width: 100%;
  height: 60px;
  overflow: hidden;
  //background-color: rgba(0, 112, 243, 0.1);
  

   span i {
    font-size: 1.3rem;
     cursor:pointer;
  }
  p{
    cursor:pointer;
    font-size: 11px;
  }

  .cart__icon,
  .fav__icon {
    position: relative;
    color: #0c2f54;
  }

  .badge {
    position: absolute;
    top: -60%;
    right: -25%;
    height: 15px;
    width: 15px;
    content: '';
    background-color: #0070f3;
    color: white;
    border-radius: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: .7rem;
    font-weight: 600;
    z-index: 10;
  }
  

`

export const Service = styled.div`
  overflow: hidden;
  .service__item {
    //background-color: #EDB0688B;
    padding: 14px;
    display: flex;
    align-items: center;
    column-gap: .7rem;
    border-radius: 5px;
    cursor: pointer;
  }

  .service__item span i {
    font-size: 2rem;
    background-color: #0c2f54;
    padding: 10px;
    border-radius: 50px;
    color: #ffffff;
    font-weight: 400 !important;
  }

  .service__item h5 {
    font-size: 1rem;
    line-height: 2rem;
    font-weight: 600;
  }

  .service__item p{
    font-size: 0.8rem;
    color: #0c2f54;
  }
`

export const ProductCardWrapper = styled.div`
 .product__info h5{
   
 }
  .product__item{
    cursor:pointer;
  }
  .product__info span{
    font-size: 0.9rem;
  }
  .product__card-bottom .price{
    font-size: 0.8rem;
  }
  .product__card-bottom span i{
    font-size: 1.3rem;
    padding: 1px;
    background-color: #0c2f54;
    color: #fafafa;
    border-radius: 60px;
  }
  
  

`

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  //margin: 1rem 2rem;
  
  .Button{
    font-weight: bold;
    color: #ffff;
    background-color: rgba(255, 89, 89, 0.96);
    margin-left: 5px;
    cursor: pointer;
    border: none;
    font-size: 11px;
    border-radius: 8px;
    background-position: right bottom;
    transition: all 0.5s ease-out;

    &:hover {
      border: 1px solid #0899da;
      background-position: left bottom;
      cursor: pointer;
    }

    //for Mobiles
    @media only screen and (max-width: 600px) {
      padding: 10px 25px;
    }
    //for Tablets and Medium Screens
    @media only screen and (min-width: 600px) {
      padding: 10px 30px;
    }
    //for laptops and desktops
    @media only screen and (min-width: 992px) {
      padding: 10px 30px;
    }

  }
`;

export const ShopWrapper = styled.div`
  overflow: hidden;
  width:100%;
  
.filter__widget select{
  padding:  10px 20px;
  border: 1px solid #0070f3;
  cursor: pointer;
  border-radius: 5px;
  background-color: #0c2f54;
  color: #fafafa;
}
  
  .filter__widget select:focus{
    outline:none!important;
  }
  .filter__widget select option{
    font-size: 1rem;
  }
  .search__box{
    width:80%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    border: 1px solid #0071cc;
    border-radius: 5px;
    padding-right: 12px;
    padding-left: 2px;
    cursor: pointer;
  }
  .search__box input{
    width: 100%;
    border: none;
    outline: none;
    padding: 10px 10px;
  }
  .search__box span{
    color: #0071cc;
    
  }
`


export const ProductDescriptionWrapper = styled.div`
  width: 100%;
  overflow: hidden;

  .grid {
    padding: 3rem;
  }

  .product__details {
    margin-top: 20px;
  }

  .product__details h2 {
    margin-bottom: 10px;
    cursor: pointer;
  }

  .product__rating span i {
    color: coral;
  }

  .product__rating p span {
    color: coral;
  }

  .product__price {
    font-size: 1.3rem;
    font-weight: 500;
  }

  .tab__wrapper {
    font-size: 1rem;
    font-weight: 500;
    cursor: pointer;

  }

  .tab__content p {
    color:#33363E7C;
  }

  .active__tab {
    font-weight: 600;
    cursor: pointer;
  }

  .review__wrapper ul li span{
    color:coral;
    font-weight: 600;
  }
  .review__wrapper ul li p{
    margin-top: 13px;
  }
  .review__form{
    width:70%;
    //margin: auto;
    margin-top: 13px;
  }
  .review__form h4{
    font-size: 1.2rem;
    margin-bottom: 30px;
  }
  .form__group input, .form__group textarea{
    width:100%;
    border: 1px solid #33363E7C;
    border-radius:5px;
    padding: 12px 20px;
    
  }
  
  .form__group input:focus, .form__group textarea:focus{
    outline:none
  }
  .form__group{
    margin-bottom: 10px;
  }
  .form__group span{
    display: flex;
    align-items: center;
    column-gap: 5px;
    color: coral;
    font-weight: 600;
  }
  
  .section__title{
    color:#0c2f54;
    font-weight: 600;
    font-size: 1.2rem;
    margin-bottom: -8rem;
  }
`

export const CartWrapper = styled.div`
  padding:0;
  margin: 0;
  box-sizing: border-box;

  .naira-icon{
    color: #0c2154;
    font-weight: 500;
    font-size: 2.3rem;
    padding: 1px;
    margin-bottom: -0.6rem;
  }
  
  .amount{
    font-size: 1.3rem;

  }
  .naira-icon-sm{
    font-size: 1.3rem;
    margin-bottom: -0.3rem;
  }
  .table-title-flex {
    display: grid;
    grid-template-columns: 4fr 1fr;
  }

  .table-head {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 10px;
    color: #0c2154;
    margin: 1.5rem;
  }

  .update-icon {
    margin-bottom: -0.2rem;
  }

  .table-head-button {
    border: 1px solid #4baaf3;
    border-radius: 5rem;
    text-transform: capitalize;
    background-color: #4baaf3;
    padding: 0.6rem 1rem 0.6rem 1rem;
    margin: 1.5rem;
    color: #ffffff;
    font-weight: 600;
    text-align: center;
    cursor: pointer;

    &:hover {
      cursor: pointer;
      background-color: #ffffff;
      color: #0c2154;
      border: 1px solid #0c2154;
    }
  }

  .table {
    width: 100%;
    border-collapse: collapse;

    &:hover {
      cursor: pointer;
    }

  }
  
  .delete, i {
    font-size: 1rem;
    font-weight: 600;
    color: #ff0000;
    cursor: pointer;
    border: none;
    //padding: 0.4rem;
    
  }


  .table td, .table th {
    padding: 15px 20px;
    text-align: center;
    font-size: 11px;
    box-sizing: border-box;
  }

  .table th {
    text-transform: uppercase;
    font-size: 11px;
    color: #0c2154;
    /* padding-top: 20px; */

  }

  .table tr {
    color: #535b61;
    border-top: 1px solid #cdcdcd;
    border-bottom: 1px solid #cdcdcd;
  }

  .payment-icon {
    margin-bottom: -0.2rem;
    margin-left: 0.3rem;

  }

  .pagination {
    display: flex;
    flex-direction: row;
    justify-content: left;
    color: #535b61;
    list-style: none;
    margin-top: 2rem;

    &:hover {
      cursor: pointer;
    }
  }

  .previous-btn, .next-btn {
    border: none;
    border-radius: 1rem;
    background-color: #81d7e6;
    padding: 0.6rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;
    margin-right: 2rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .page-number {
    border: none;
    border-radius: 1rem;
    background-color: #81d7e6;
    padding: 0.4rem 0.8rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.6rem;
    margin-right: 2rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }

  .active {
    border: 1px solid #4baaf3;
    border-radius: 1rem;
    background-color: #4baaf3;
    padding: 0.4rem 0.8rem;
    color: #ffffff;
    font-weight: 900;
    font-size: 0.8rem;

    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
  }


  /* MOBILE RESPONSIVE */
  @media (max-width: 500px) {

    .table thead {
      display: none;
    }

    .table-title-flex {
      display: block;
    }

    .table-head-button {
      margin-right: 10rem;
    }

    .table {
      margin-left: 1rem;
    }

    .table, .table tbody, .table tr, .table td {
      display: block;
      width: 97.5%;
      background: none;
      box-shadow: none;
      border: none;

    }

    .table tr {
      box-sizing: border-box;
      border: 1px solid #c5c5c5;
      border-bottom: none;
      padding: 1rem 0 1rem 0;

    }

    .table tr:last-child {
      border-bottom: 1px solid #c5c5c5;
    }

    .table td {
      text-align: right;
      padding-left: 50%;
      position: relative;
      font-size: 13px;
      width: 95%;
      line-height: 5px;

    }

    .table td::before {
      content: attr(data-label);
      position: absolute;
      left: 0;
      width: 50%;
      padding-left: 15px;
      font-size: 13px;
      font-weight: 600;
      text-align: left;
    }

    .pagination {
      margin-top: 3rem;
      margin-left: -1.5rem;
    }

    .previous-btn, .next-btn {
      padding: 0.5rem;
      color: #ffffff;
      font-size: 0.7rem;
      margin-right: 2rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }

    .page-number {
      border: 1px solid #535b61;
      border-radius: 1rem;
      background-color: #535b61;
      padding: 0.1rem 0.2rem;
      color: #ffffff;
      font-weight: 900;
      font-size: 0.8rem;
      margin-right: 1.5rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }

    .active {
      border: 1px solid #4baaf3;
      border-radius: 1rem;
      background-color: #4baaf3;
      padding: 0.1rem 0.2rem;
      color: #ffffff;
      font-weight: 900;
      font-size: 0.8rem;

      &:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    }
  }
`
