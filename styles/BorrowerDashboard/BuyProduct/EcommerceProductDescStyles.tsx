import styled from "styled-components";

export const EcommerceProductDescWrap = styled.div`

.shipping-card{
margin: 0.5rem 2rem 0.5rem 2rem;
height: 70vh;
background-color: #ffffff;
/* border-radius: 0.5rem; */
border: 1px solid #ffffff;
padding: 1rem;
/* box-shadow: 0 2px 4px 2px rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.1); */

}

.shipping-card p{
    text-transform: uppercase;
    color: #0c2154;
    font-weight: 900;
    font-size: 0.8rem;
    text-align: center;
}
.shipping-form label{
    text-align: left;
    font-size: 12px;
    padding-left: 5px;
}
.shipping-form input{
    height: 1rem;
    padding: 1.10rem .96rem;
    font-size: 0.8rem;
    border: 1px solid #777777;
    font-weight: 400;
    appearance: none;
    border-radius: .25rem;
    margin-bottom: 1rem;
    &:focus {
      border: 1px solid #0d6efd;
      outline: #0d6efd;
    }
}

button.confirm{
border: 1px solid #0071cc;
border-radius: 1rem;
background-color: #0071cc;
padding: 0.5rem 1rem 0.5rem 1rem;
color: #ffffff;
font-weight: 600;
text-align: center;
margin-right: 0.8rem;
width: 100%;
  cursor: pointer;
&:hover{
    cursor: pointer;
    opacity: 0.9;
}
}
`