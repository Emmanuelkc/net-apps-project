import styled from "styled-components";

export const EcommerceHome = styled.section`
  //box-shadow: 0 4px 7px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  border-radius: 5px;
  overflow: hidden;
  width: 100%;

  .slider {
    width: 100%;
    height: 75vh;
    position: relative;
    overflow: hidden;
    background-color: var(--color-dark);
  }

  .slide {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0;
    transform: translateX(-50%);
    transition: all 0.5s ease;
  }

  @media screen and (min-width: 600px) {
    .slide img {
      width: 100%;
      height: 100%;
    }
  }

  .slide img {
    height: 100%;
  }

  .content {
    position: absolute;
    text-align: center;
    top: 23rem;
    left: 50%;
    opacity: 0;
    width: 45%;
    padding: 3rem;
    display: flex;
    justify-self: center;
    align-items: center;
    flex-direction: column;
    transform: translateX(-50%);
    background: rgba(0, 0, 0, 0.4);
    animation: slide-up 1s ease 0.5s;
    animation-fill-mode: forwards;
    //   visibility: hidden;
    h2 {
      font-size: 4.5rem;
    }
  }

  @keyframes slide-up {
    0% {
      visibility: visible;
      top: 30rem;
    }
    100% {
      visibility: visible;
      top: 12rem;
    }
  }

  @media screen and (max-width: 600px) {
    .content {
      width: 80%;
    }
  }

  .content > * {
    color: #fff;
    margin-bottom: 1rem;
  }

  .current {
    opacity: 1;
    transform: translateX(0);
  }

  .current .content {
    opacity: 1;
  }

  .arrow {
    border: 2px solid #0071cc;
    border-radius: 50%;
    background: transparent;
    color: #fff;
    width: 2rem;
    height: 2rem;
    cursor: pointer;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    z-index: 2;
  }

  .arrow:hover {
    background: #fff;
  }

  .next {
    right: 1.5rem;
    color: #8bb5d1;
  }

  .prev {
    left: 1.5rem;
    color: #8bb5d1;
  }

  hr {
    height: 2px;
    background: #fff;
    width: 50%;
  }


`
export const EcommerceProducts = styled.section`
  position: relative;
  .dataview-demo {
    padding: 3rem;
    border-radius: 8px;
  }


  .dataview-demo .product-grid-item {
    border: 1px solid #dee2e6;
    padding: 1rem;
    width: 95%;
    height: 60%;
    cursor:pointer;
    border-radius: 8px;
    background-color: #ffffff;
    margin-bottom: -6rem;
  }
  .container{
    margin-bottom: -10rem;
  }
  .dataview-demo .product-grid-item:hover{
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  }



  .dataview-demo .product-grid-item .product-grid-item-content {
    text-align: center;
    width: 100%;
    height: 50%;
  }



  .product-grid-item img {
    width: 36%;
    height: 80%;
  }

  .dataview-demo .product-grid-item .product-grid-item-top,
  .dataview-demo .product-grid-item .product-grid-item-bottom {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .product-item-bottom{
    cursor:pointer;
    font-size: 1rem;
    padding-left: 1rem;
    padding-bottom: 20rem;
  }
  .dataview-demo, .product-grid-item, .product-item-bottom, .product-name {
    font-size: 0.7rem;
    font-weight: 500;
    padding-top: 0.4rem;

  }
  .dataview-demo .product-item-bottom .product-price {
    font-size: 12px;
    font-weight: 600;
    display: flex;
  
  }
  .product-grid-item-bottom, .product-price, .currency{
    font-size: 1rem;
  }
  

  .cart{
    display: flex;
    justify-content: space-between;
}
  .addToCart{
    border: 1px dotted;
    border-radius: 1rem;
    background-color: beige;
    font-size: 11px;
    font-weight: 600;
      &hover{
        border: none;
        background-color: #f8f9fa;
      }

  }
  
  .cart-icons{
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 2rem;
    border: 1px solid #0c2154;
    width: 2rem;
    height: 2rem;
    &:hover{
      opacity: 0.6;
      cursor: pointer;
      transition: 5ms;
    }
  }
  
  .cart-icon{
    border-radius: 2rem;
    color: #0c2154;
    font-weight: 500;
    font-size: 2rem;
    padding: 3px;
  }


`
export const ShoppingCartWarapper = styled.section`
  width: 380px;
  padding: 15px;
`
export const CartItemsWrapper = styled.section`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid lightblue;
  padding-bottom: 16px;

  div{
    flex: 1;
  }
  h5{
    line-height: 1rem;
  }
  .information, .buttons{
    display: flex;
    justify-content: space-between;
  }
  .btn{
    padding:0.1rem 0.7rem
  }
  
  img{
    max-width: 95px;
    object-fit: cover;
    margin-left: 10px;
    margin-top: 30px;
    //width: 80%;
    //height: 50%;
  }
`
export const Button = styled.button`
  font-weight: bold;
  color: #ffff;
  background-color: rgba(255, 89, 89, 0.96);
  margin-left: 2px;
  cursor: pointer;
  border: none;
  font-size: 11px;
  border-radius: 3rem;
  background-position: right bottom;
  transition: all 0.5s ease-out;

  &:hover {
    border: 1px solid #0899da;
    background-position: left bottom;
    cursor: pointer;
  }

  //for Mobiles
  @media only screen and (max-width: 600px) {
    padding: 9px 9px;
  }
  //for Tablets and Medium Screens
  @media only screen and (min-width: 600px) {
    padding: 10px 10px;
  }
  //for laptops and desktops
  @media only screen and (min-width: 992px) {
    padding: 10px 10px;
  }
`;


