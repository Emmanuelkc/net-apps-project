/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,

  compiler: {
    styledComponents: true,
    // buildActivity: false
  },
  devIndicators: {
    buildActivity: false
  }

}


module.exports = nextConfig
